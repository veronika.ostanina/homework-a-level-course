package nix.alevel.java.nix.practic.arithmeticProgression;

public class ProgressionConfigurationException extends Exception {


    public ProgressionConfigurationException(String message) {
        super(message);
    }
}
