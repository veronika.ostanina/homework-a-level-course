package nix.alevel.java.nix.practic.api;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collection;
import java.util.DoubleSummaryStatistics;
import java.util.GregorianCalendar;

public class APIWork {

    public static void main(String[] args) {

    }

    public DoubleSummaryStatistics sumMinMaxAvg(double... array) {
        return Arrays.stream(array).summaryStatistics();
    }

    public Collection leapSortYear(Collection<LocalDateTime> array) {
        GregorianCalendar cal = (
                GregorianCalendar) GregorianCalendar.getInstance();
        array.stream().filter(p -> cal.isLeapYear(p.getYear())).sorted();
        return array;
    }


}
