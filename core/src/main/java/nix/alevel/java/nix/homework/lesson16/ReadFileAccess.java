package nix.alevel.java.nix.homework.lesson16;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.*;


public class ReadFileAccess {

    private List<List<String>> fileValue = new ArrayList<>();
    private Map<String, Integer> heading = new HashMap<>();


    public void loadData(String path) {
        File file = new File(path);
        if (file.isDirectory() || !file.exists()) {
            throw new RuntimeException("File is directory or not exist!");
        }
        try (InputStream in = new FileInputStream(file);
             BufferedReader bufferedReader =
                     new BufferedReader(new InputStreamReader(in, StandardCharsets.UTF_8))) {
            isUnique(bufferedReader.readLine().split(","));
            bufferedReader.lines().
                    forEach(e -> fileValue.
                            add(Arrays.asList(e.split(","))));

        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }


    public String getCellValue(int row, int col) {
        if (row >= fileValue.size() || col >= heading.size()) {
            throw new RuntimeException("Choose another row or column!");
        }
        return fileValue.get(row).get(col);
    }

    public String getCellValue(int row, String col) {
        if (row >= fileValue.size() || !heading.containsKey(col)) {
            throw new RuntimeException("Choose another row or column!");
        }
        return fileValue.get(row).get(heading.get(col));
    }

    private void isUnique(String[] headings) {
        for (int i = 0; i < headings.length; i++) {
            if (heading.putIfAbsent(headings[i], i) != null) {
                throw new RuntimeException("Heading the same!");
            }
        }

    }

    public Set<String> getHeading() {
        return heading.keySet();
    }
}
