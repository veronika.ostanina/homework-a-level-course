package nix.alevel.java.nix.homework.lesson20;

import java.io.FileWriter;
import java.io.IOException;
import java.io.UncheckedIOException;

public class WriteFileInput {
    private final String path;
    private String input = "";

    public WriteFileInput(String path) {
        Thread r = new Thread(() -> {
            try {
                String in = "";
                while (!input.endsWith("quit")) {
                    if (!input.substring(input.length() / 2).equals(in)) {
                        writeInput(input);
                        in = input;
                    } else {
                        input = in;
                    }
                    Thread.sleep(1000);
                }
                writeInput(in);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        r.start();
        this.path = path;
    }

    public String getInput() {
        return input;
    }

    public void setInput(String temp) {
        input = input.concat(temp);
    }

    private void writeInput(String input) {
        try (FileWriter writer = new FileWriter(path)) {
            writer.write(input);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }


}
