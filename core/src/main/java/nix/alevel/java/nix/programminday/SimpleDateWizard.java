package nix.alevel.java.nix.programminday;

import java.time.LocalDate;

public class SimpleDateWizard implements DateWizard {

    @Override
    public LocalDate getDayOfYear(int year, int day) {
        return LocalDate.ofYearDay(year, day);
    }

}
