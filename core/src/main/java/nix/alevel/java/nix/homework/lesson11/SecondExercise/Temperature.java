package nix.alevel.java.nix.homework.lesson11.SecondExercise;

public class Temperature {
    private double temperature = 20;

    public double getTemperature() {
        return temperature;
    }

    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }


}
