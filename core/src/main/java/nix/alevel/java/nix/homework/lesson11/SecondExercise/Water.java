package nix.alevel.java.nix.homework.lesson11.SecondExercise;

public class Water extends DetermState implements Substance {
    private State stateWater = State.LIQUID;
    private Temperature temperature = new Temperature();

    @Override
    public State heatUp(double t) {
        temperature.setTemperature(t);
        stateWater = super.makeState(99.99, 0, t);
        return stateWater;
    }

    @Override
    public double getTemperature() {
        return temperature.getTemperature();
    }


}

