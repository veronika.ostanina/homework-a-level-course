package nix.alevel.java.nix.homework.lesson8;

import java.util.ArrayList;

public class Gamer {
    private char gamerSymbol;
    private String status;
    private ArrayList<Integer> verticalMove = new ArrayList<>();
    private ArrayList<Integer> horizontalMove = new ArrayList<>();


    public Gamer(char gamerSymbol, String status) {
        this.gamerSymbol = gamerSymbol;
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public char getGamerSymbol() {
        return gamerSymbol;
    }

    public void stepTaken(int posH, int posV) {
        verticalMove.add(posV);
        horizontalMove.add(posH);
    }

    public boolean isWin() {
        int len = verticalMove.size();
        if (len < 3) {
            return false;
        }
        int findMaxWinnerSize = 3;
        int countFirstDiagonal = 0;
        int countSecondDiagonal = 0;
        int[] fidnMaxV = new int[findMaxWinnerSize];
        int[] fidnMaxH = new int[findMaxWinnerSize];
        for (int i = 0; i < len; i++) {
            fidnMaxH[horizontalMove.get(i)]++;
            fidnMaxV[verticalMove.get(i)]++;

            if (horizontalMove.get(i) == verticalMove.get(i)) {
                countFirstDiagonal++;
            }
            if (verticalMove.get(i) == findMaxWinnerSize - 1 - horizontalMove.get(i)) {
                countSecondDiagonal++;
            }

        }
        if (countFirstDiagonal == findMaxWinnerSize || countSecondDiagonal == findMaxWinnerSize) {
            return true;
        }
        for (int i = 0; i < findMaxWinnerSize; i++) {
            if (fidnMaxH[i] == findMaxWinnerSize || fidnMaxV[i] == findMaxWinnerSize) {
                return true;
            }
        }


        return false;
    }


}
