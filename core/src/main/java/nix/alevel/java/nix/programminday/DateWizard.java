package nix.alevel.java.nix.programminday;

import java.time.LocalDate;

public interface DateWizard {

    LocalDate getDayOfYear(int year, int day);

}
