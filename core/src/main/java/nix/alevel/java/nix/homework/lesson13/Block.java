package nix.alevel.java.nix.homework.lesson13;

public interface Block<R> {
    R run() throws Exception;
}
