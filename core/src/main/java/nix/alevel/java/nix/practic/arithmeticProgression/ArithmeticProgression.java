package nix.alevel.java.nix.practic.arithmeticProgression;

public class ArithmeticProgression {
    private final int initial;
    private final int step;

    ArithmeticProgression(int initial, int step) throws ProgressionConfigurationException {
        if (step == 0) {
            throw new ProgressionConfigurationException("step can`t be equals 0!");
        }
        this.initial = initial;
        this.step = step;
    }

    public static void main(String[] args) {
        ArithmeticProgression arithmeticProgression;
        try {
            arithmeticProgression = new ArithmeticProgression(1, 1);
            arithmeticProgression.calculate(5);
        } catch (ProgressionConfigurationException e) {
            System.err.println(e.toString());
        }
        try {
            arithmeticProgression = new ArithmeticProgression(1, 1);
            arithmeticProgression.calculate(-13);
        } catch (ProgressionConfigurationException e) {
            System.err.println(e.toString());
        }
        try {
            arithmeticProgression = new ArithmeticProgression(1, 0);
            arithmeticProgression.calculate(5);
        } catch (ProgressionConfigurationException e) {
            System.err.println(e.toString());
        }
        try {
            arithmeticProgression = new ArithmeticProgression(1, 0);
            arithmeticProgression.calculate(-13);
        } catch (ProgressionConfigurationException e) {
            System.err.println(e.toString());
        }

    }

    int calculate(int n) throws ProgressionConfigurationException// n - number of element progressison
    {
        if (n <= 0) {
            throw new ProgressionConfigurationException("n can`t be less or equals 0!");
        }
        if (n == 1) {
            return initial;
        }
        int nNumber = initial + step;
        if (n == 2) {
            return nNumber;
        }

        for (int i = 2; i < n; i++) {
            nNumber += step;
        }
        return nNumber;
    }
}
