package nix.alevel.java.nix.homework.lesson21;


import java.util.Map;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;

public class Hippodrome {
    private final int userRate;
    private final int quantityHourse;
    private final ConcurrentHashMap<String, Long> places = new ConcurrentHashMap<String, Long>();
    private final CountDownLatch countDownLatch;


    public Hippodrome(int userRate, int quantityHourse) {
        this.userRate = userRate;
        this.quantityHourse = quantityHourse;
        countDownLatch = new CountDownLatch(quantityHourse);

    }

    public Map<String, Long> getPlaces() {
        return places;
    }

    public Long startGame() {
        for (int i = 1; i <= quantityHourse; i++) {
            new Thread(() -> {
                int goDistanse = 1000;
                Random rand = new Random();
                while (goDistanse > 0) {

                    goDistanse -= (rand.nextInt(100) + 100);
                    try {
                        Thread.sleep((rand.nextInt(100) + 400));
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }


                places.put(Thread.currentThread().getName(), countDownLatch.getCount());
                countDownLatch.countDown();

            }, String.valueOf(i)).start();
        }
        try {
            countDownLatch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return quantityHourse + 1 - places.get(String.valueOf(userRate));
    }


}
