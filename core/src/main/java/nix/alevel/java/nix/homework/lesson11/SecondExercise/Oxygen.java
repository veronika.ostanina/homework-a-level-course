package nix.alevel.java.nix.homework.lesson11.SecondExercise;

public class Oxygen extends DetermState implements Substance {
    private State stateOxygen = State.GAS;
    private Temperature temperature = new Temperature();

    @Override
    public State heatUp(double t) {
        temperature.setTemperature(t);
        stateOxygen = super.makeState(-182.96, -219, t);
        return stateOxygen;
    }

    @Override
    public double getTemperature() {
        return temperature.getTemperature();
    }

}
