package nix.alevel.java.nix.homework.lesson19;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(value = ElementType.FIELD)//what exactly we can mark field method or other
@Retention(value = RetentionPolicy.RUNTIME)//life cycle
public @interface PropertyKey {
    String value() default "none";
}
