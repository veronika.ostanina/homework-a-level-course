package nix.alevel.java.nix.practic.arrayStringWork;

public class StringUnit {
    public static void main(String[] args) {
        long d = System.nanoTime();
        System.out.println(enter("abcd"));
        long d2 = System.nanoTime();
        System.out.println(d2 - d);
        long d1 = System.nanoTime();
        System.out.println(enter("abcddfghjkl;''/.,mjnhgfrdfghjkiolp;'/.,mnhgfde4r5t6y7u8iop[];.l,kmjnhgfdddfvghjk"));
        long d4 = System.nanoTime();
        System.out.println(d4 - d1);


    }

    public static CharSequence enter(CharSequence enterString) {
        StringBuilder charSequence = new StringBuilder();
        for (int i = enterString.length() - 1; i > 0; i--) {
            charSequence.append(enterString.charAt(i));
        }
        return charSequence;

    }
}
