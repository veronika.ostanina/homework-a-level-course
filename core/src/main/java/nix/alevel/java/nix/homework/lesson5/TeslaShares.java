package nix.alevel.java.nix.homework.lesson5;

public class TeslaShares {
    private double[] teslaShares;

    public TeslaShares(double... teslaShares) {
        if (teslaShares.length == 0) {
            throw new IllegalArgumentException("Array is empty");
        }
        this.teslaShares = teslaShares;
    }

    public void setTeslaShares(double... teslaShares) {
        if (teslaShares.length == 0) {
            throw new IllegalArgumentException("Array is empty");
        }
        this.teslaShares = teslaShares;
    }

    public double calculateMaxProfit() {

        if (teslaShares[0] <= 0) {
            throw new IllegalArgumentException("Less than 0!!!");
        }

        double minBuy, maxSell, addMin;
        minBuy = maxSell = addMin = teslaShares[0];
        double profit = 0;
        for (int i = 1, length = teslaShares.length; i < length; i++) {
            if (teslaShares[i] <= 0) {
                throw new IllegalArgumentException("Less than 0!!!");
            }
            if (teslaShares[i] < minBuy) {
                if (addMin > teslaShares[i]) {
                    addMin = teslaShares[i];
                }
                if (teslaShares[i] - teslaShares[i - 1] > profit) {
                    minBuy = teslaShares[i - 1];
                    maxSell = teslaShares[i];
                }
            }
            if (teslaShares[i] - addMin > profit) {
                minBuy = addMin;
                maxSell = teslaShares[i];
            }
            profit = maxSell - minBuy;
        }
        System.out.println("Max sell: " + maxSell + " Min buy: " + minBuy);
        return profit;
    }
}
