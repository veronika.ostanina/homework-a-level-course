package nix.alevel.java.nix.homework.lesson3;

public class FindMultiplicityK extends CreateArray {

    public FindMultiplicityK(int[] array) {
        super(array);
    }


    public int[] findMultiplicityK(int k) {
        if (k == 0) {
            return null;
        }
        int countTempArraySize = 0, countFinalArray = 0;
        int[] finalArray;
        for (int i : array) {
            if (i % k == 0) {
                countTempArraySize++;
            }
        }

        finalArray = new int[countTempArraySize];
        for (int i : array) {
            if (i % k == 0) {
                finalArray[countFinalArray] = i;
                countFinalArray++;
            }
        }

        return finalArray;
    }
}
