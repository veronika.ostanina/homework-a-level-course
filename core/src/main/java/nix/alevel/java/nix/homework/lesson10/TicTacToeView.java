package nix.alevel.java.nix.homework.lesson10;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class TicTacToeView {
    private final int GAMESIZE;


    public TicTacToeView(int gameSize) {
        GAMESIZE = gameSize;
    }

    public void printGameTable(char[][] tableGame) {
        String roof = " _ _ ";
        System.out.println(roof.repeat(GAMESIZE));
        for (int i = 0; i < GAMESIZE; i++) {
            for (int j = 0; j < GAMESIZE; j++) {
                System.out.print("|_" + tableGame[i][j] + "_|");
            }
            System.out.println();
        }
    }

    public Position enterPosition(char[][] tableGame, char gamerSymbol) {
        int posHorizontal = 0;
        int posVertical = 0;
        printGameTable(tableGame);
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            System.out.print("Please enter free horizontal (row) position  (0-" + (GAMESIZE - 1) + ")[" + gamerSymbol + "] : ");
            posHorizontal = Integer.parseInt(reader.readLine());
            System.out.print("Please enter free  vertical (column) position  (0-" + (GAMESIZE - 1) + ") [" + gamerSymbol + "] : ");
            posVertical = Integer.parseInt(reader.readLine());
        } catch (NumberFormatException e) {
            System.out.println("Invalid data!");
            return enterPosition(tableGame, gamerSymbol);
        } catch (
                IOException e) {
            e.printStackTrace();
        }
        return new Position(posHorizontal, posVertical);
    }

    public String printResult(char[][] tableGame, GamerModel gamer1, GamerModel gamer2) {
        printGameTable(tableGame);
        String result = "Result game: " + System.lineSeparator() +
                "Gamer 1 [" + gamer1.getGamerSymbol() + "] is " + gamer1.getStatus() + System.lineSeparator() +
                "Gamer 2 [" + gamer2.getGamerSymbol() + "] is " + gamer2.getStatus();
        System.out.println(result);
        return result;

    }
}
