package nix.alevel.java.nix.homework.lesson19;

public class AppProperties {
    public int age = 18;
    @PropertyKey("name")
    public String name;
    public String surname;
    @PropertyKey("mark")
    public int mark;
    @PropertyKey("phone")
    public long phone;

    public AppProperties() {
    }

    public AppProperties(String name, int mark, long phone) {
        this.name = name;
        this.mark = mark;
        this.phone = phone;
    }

    public long getMark() {
        return mark;
    }
}
