package nix.alevel.java.nix.practic.reflection;

public interface SetValueField<E, T, K> {
    void setValueField(E e, T t, K k) throws Exception;
}
