package nix.alevel.java.nix.homework.lesson17.Gallows;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Word {
    private List<HashMap<Character, Boolean>> word = new ArrayList<>();

    public Word(String word) {
        for (int i = 0, len = word.length(); i < len; i++)
            this.word.add(new HashMap<>(Map.of(word.charAt(i), false)));
    }

    public List<HashMap<Character, Boolean>> getWord() {
        return word;
    }

    public int getSizeWord() {
        return word.size();
    }

    public boolean isPresent(char character) {
        Character characterTemp = character;
        if (!word.contains(new HashMap<>(Map.of(characterTemp, false)))) {
            return false;
        }
        for (HashMap<Character, Boolean> i : word) {
            if (i.containsKey(characterTemp)) {
                if (!i.get(characterTemp)) {
                    i.replace(characterTemp, false, true);
                } else return false;
            }
        }
        return true;
    }

    public boolean isWin() {
        for (HashMap<Character, Boolean> i : word) {
            if (i.containsValue(false)) {
                return false;
            }
        }
        return true;
    }
}
