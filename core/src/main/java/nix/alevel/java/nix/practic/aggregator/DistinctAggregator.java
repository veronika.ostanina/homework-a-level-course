package nix.alevel.java.nix.practic.aggregator;

import java.util.HashSet;

public class DistinctAggregator<T> implements Aggregator<Integer, T> {
    @Override
    public Integer aggregate(T... items) {
        int len = items.length;
        if (len == 0) {
            return 0;
        }
        if (len == 1) {
            return 1;
        }
        Integer countUniqueNumber = 1;
        HashSet<T> uniqueValue = new HashSet<>();
        uniqueValue.add(items[0]);
        for (int i = 1; i < len; i++) {
            if (!uniqueValue.contains(items[i])) {
                countUniqueNumber++;
                uniqueValue.add(items[i]);
            }
        }
        return countUniqueNumber;
    }
}
