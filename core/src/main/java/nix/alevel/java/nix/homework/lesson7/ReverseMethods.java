package nix.alevel.java.nix.homework.lesson7;

public class ReverseMethods {
    private String stringReverse;

    public ReverseMethods(String stringReverse) {
        this.stringReverse = stringReverse;
    }

    public void setStringReverse(String stringReverse) {
        this.stringReverse = stringReverse;
    }

    public CharSequence reversedLoop() {
        StringBuilder charSequence = new StringBuilder();
        for (int i = stringReverse.length() - 1; i >= 0; i--) {
            charSequence.append(stringReverse.charAt(i));
        }
        return charSequence.toString();

    }

    public CharSequence reversedStandartLib() {
        StringBuilder charSequence = new StringBuilder(stringReverse);
        return charSequence.reverse().toString();
    }

    public CharSequence reversedConstantTime() {
        return new ReverseString(stringReverse).toString();

    }


}
