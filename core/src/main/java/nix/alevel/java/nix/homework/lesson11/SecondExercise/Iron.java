package nix.alevel.java.nix.homework.lesson11.SecondExercise;

public class Iron extends DetermState implements Substance {
    private State stateIron = State.SOLID_MATTER;
    private Temperature temperature = new Temperature();

    @Override
    public State heatUp(double t) {
        temperature.setTemperature(t);
        this.stateIron = super.makeState(2749.85, 1539.0, t);
        return stateIron;
    }

    @Override
    public double getTemperature() {
        return temperature.getTemperature();
    }


}
