package nix.alevel.java.nix.practic.aggregator;

import java.util.Optional;

public class MaxAggregator<T extends Comparable<T>> implements Aggregator<Optional<T>, T> {

    @Override
    public Optional<T> aggregate(T... items) {
        int len = items.length;
        if (len == 0) {
            Optional.empty();
        }
        T tempMax = items[0];
        for (int i = 1; i < len; i++) {
            if (tempMax.compareTo(items[i]) < 0) {
                tempMax = items[i];
            }
        }

        return Optional.of(tempMax);
    }


}
