package nix.alevel.java.nix.homework.lesson21;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class HippodromeGame {
    public static void main(String[] args) {
        HippodromeGame g = new HippodromeGame();
        g.horseRacing();
    }

    public void horseRacing() {
        Hippodrome racing;
        int userRate = getUserValue("nuber of a horse");
        int quantityHorse = getUserValue("quantity of horse");

        if (userRate <= quantityHorse) {
            racing = new Hippodrome(userRate, quantityHorse);
            long place = racing.startGame();
            System.out.println("Your horse is  " + place);
            if (place == 1) {
                System.out.println("She is winner");
            }
            racing.getPlaces().forEach((horNum, plc) -> {
                System.out.println("Horse " + horNum + " place " + (quantityHorse + 1 - plc));
            });

        } else {
            horseRacing();
        }


    }

    private int getUserValue(String value) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        try {
            System.out.print("Please enter a " + value + "  : ");
            return Integer.parseInt(reader.readLine().trim());

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

    }
}
