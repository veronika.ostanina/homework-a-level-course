package nix.alevel.java.nix.homework.lesson3;


import java.util.Arrays;

public class CreateArray {
    protected int[] array;

    public CreateArray(int[] array) {

        this.array = Arrays.copyOf(array, array.length);
    }

    public int[] getArray() {
        return array;
    }

    public void setArray(int[] array) {
        this.array = array;
    }
}
