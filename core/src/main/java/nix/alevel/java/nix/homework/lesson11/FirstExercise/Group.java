package nix.alevel.java.nix.homework.lesson11.FirstExercise;

public class Group {

    private Student[] arrayStudent;

    public Group(Student[] arrayStudent) {
        if (arrayStudent == null || arrayStudent.length == 0) {
            throw new IllegalArgumentException("Array is Empty!");
        }
        for (Student i : arrayStudent) {
            if (i == null) {
                throw new IllegalArgumentException("Array has element Null!");
            }
        }
        this.arrayStudent = arrayStudent;
    }

    public void showContractStudents() {

        for (Student i : arrayStudent) {
            if (i instanceof ContractStudent) {
                System.out.println(i.dataStudent());
            }
        }
    }

}
