package nix.alevel.java.nix.homework.lesson6;

public class ContainerWater {
    private int finalArea;

    public int getFinalArea() {
        return finalArea;
    }

    public void maxArea(int... height) {
        int len = height.length - 1, end = height[len], finalArea = 0, indexEnd = len;

        for (int i = 0; i < indexEnd; i++) {
            int temp = Math.min(end, height[i]);
            if ((temp * (indexEnd - i)) > finalArea) {
                finalArea = temp * (indexEnd - i);
            }
            if (height[i] > height[indexEnd]) {
                indexEnd--;
                i--;
                end = height[indexEnd];
            }
        }
        this.finalArea = finalArea;


    }


}
