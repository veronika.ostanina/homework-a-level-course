package nix.alevel.java.nix.homework.lesson11.SecondExercise;

public interface Substance {
    State heatUp(double t);

    double getTemperature();

    enum State {LIQUID, SOLID_MATTER, GAS}
}
