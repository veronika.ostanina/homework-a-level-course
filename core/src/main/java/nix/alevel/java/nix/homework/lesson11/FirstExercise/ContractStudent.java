package nix.alevel.java.nix.homework.lesson11.FirstExercise;

public class ContractStudent extends Student {
    private double costContract;

    public ContractStudent(String name, int age, double costContract) {
        super(name, age);
        this.costContract = costContract;
    }

    @Override
    public String dataStudent() {
        return super.dataStudent() + " cost of contract is " + costContract;
    }


}
