package nix.alevel.java.nix.homework.lesson11.FirstExercise;

public class Student {
    private String name;
    private int age;

    public Student(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String dataStudent() {
        return "Name is " + name;
    }


}
