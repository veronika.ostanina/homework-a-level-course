package nix.alevel.java.nix.practic.ioWork;

import java.io.*;
import java.nio.charset.StandardCharsets;

public class ReadFromFileEquals {

    public void readFromFileEquals(String path, String substring) {
        File file = new File(path);
        try (InputStream in = new FileInputStream(file);
             BufferedReader bufferedReader =
                     new BufferedReader(new InputStreamReader(in, StandardCharsets.UTF_8))) {

            bufferedReader.lines()
                    .filter(p -> p.contains(substring))
                    .forEach(System.out::println);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }
}
