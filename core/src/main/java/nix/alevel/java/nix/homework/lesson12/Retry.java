package nix.alevel.java.nix.homework.lesson12;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Retry {
    private static final Logger LOG = LoggerFactory.getLogger(Retry.class);
    private final int miliSecTime = 10;

    public void reTry(int numberTry, Block block) {
        int waiting = 10;
        for (int i = 1; i <= numberTry; i++) {
            try {
                Thread.sleep(waiting);
                block.run();
                return;
            } catch (InterruptedException e1) {
                e1.printStackTrace();
            } catch (Exception e) {
                LOG.error("Exeption : {}, waiting : {}", e.getMessage(), waiting);
                waiting = miliSecTime * (i + 1);
                if (i == numberTry) {
                    LOG.error(e.toString());
                    throw new RuntimeException(e);
                }
            }
        }

    }

}
