package nix.alevel.java.nix.homework.lesson9;

public class LongestCommonPrefix {
    int lenStr;
    private String[] strs;

    public LongestCommonPrefix(String[] strs) {
        lenStr = strs.length;
        this.strs = strs;
    }

    public void setStrs(String[] strs) {
        lenStr = strs.length;
        this.strs = strs;


    }

    public String longestCommonPrefix() {
        boolean i = true;
        int k = 0;
        if (lenStr == 0) {
            return "";
        }
        if (lenStr == 1) {
            return strs[0];
        }
        while (i) {
            for (int j = 0; j < lenStr - 1; j++) {
                if (k >= strs[j + 1].length() || k >= strs[j].length()) {
                    return strs[j].substring(0, k);
                }
                if (strs[j].charAt(k) != strs[j + 1].charAt(k)) {
                    i = false;
                    break;
                }
            }
            k++;
        }


        return strs[0].substring(0, k - 1);
    }
}
