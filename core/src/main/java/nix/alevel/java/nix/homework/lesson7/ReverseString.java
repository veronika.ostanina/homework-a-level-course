package nix.alevel.java.nix.homework.lesson7;


public class ReverseString implements CharSequence {
    private char[] val;
    private int shift;

    public ReverseString(CharSequence val) {
        this.val = val.toString().toCharArray();
        shift = length() - 1;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder(this);
        return stringBuilder.toString();
    }

    @Override
    public int length() {
        return val.length;
    }

    @Override
    public char charAt(int index) {
        return val[shift - index];
    }

    @Override
    public CharSequence subSequence(int start, int end) {
        StringBuilder subString = new StringBuilder();
        for (int i = shift - start, len = shift - end; i > len; i--) {
            subString.append(val[i]);
        }
        return subString.toString();
    }
}