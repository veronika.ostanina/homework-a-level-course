package nix.alevel.java.nix.homework.lesson6;

public class ZigZag {
    private String finalString;

    public String getFinalString() {
        return finalString;
    }

    public void makeZigZag(String enterString, int size) {
        if (size == 1) {
            finalString = enterString;
            return;
        }
        char[] arrayEnterString = enterString.toCharArray();
        int len = enterString.length(), step = 2 * size - 2, temp, tempStep, minus = size - 1;
        StringBuilder finalStringBuilder = new StringBuilder();
        for (int i = 0; i < size; i++) {
            temp = i;
            if (i == 0 || i == minus) {
                while (temp < len) {
                    finalStringBuilder.append(arrayEnterString[temp]);
                    temp += step;
                }
            } else {
                boolean fizz = true;
                tempStep = step - i;
                while (temp < len || tempStep < len) {
                    if (fizz) {
                        finalStringBuilder.append(arrayEnterString[temp]);
                        temp += step;
                        fizz = false;
                    } else {
                        finalStringBuilder.append(arrayEnterString[tempStep]);
                        tempStep += step;
                        fizz = true;
                    }

                }
            }

        }

        this.finalString = finalStringBuilder.toString();
    }


}
