package nix.alevel.java.nix.practic.reflection;

import nix.alevel.java.nix.homework.lesson19.PropertyKey;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CreateObjectFromCSV {


    public <E> List<E> fillField(Class<E> classObject, String path) {
        List<E> instances = new ArrayList<>();
        LoadDataFile loadDataCSV = new LoadDataFile();
        loadDataCSV.setPath(path);
        loadDataCSV.loadFieldsNames();
        var names = loadDataCSV.getNames();
        var listsValues = loadDataCSV.getFieldsValues();
        try {
            for (List<String> listOfValues : listsValues) {
                E instanceE = (E) classObject.getConstructor().newInstance();
                for (Field field : classObject.getFields()) {
                    PropertyKey annotation = field.getAnnotation(PropertyKey.class);
                    field.setAccessible(true);
                    if (annotation != null) {
                        try {
                            setValueField(field, instanceE, field.getType(), listOfValues.get(names.indexOf(field.getName())));

                        } catch (IllegalAccessException e) {
                            throw new RuntimeException(e);
                        }
                    }
                }
                instances.add(instanceE);
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return instances;
    }

    private void setValueField(Field field, Object instanceE, Class classType, String value) throws Exception {
        Map<Class, SetValueField<Field, Object, String>> typesOfField = new HashMap();

        typesOfField.put(int.class, (f, inst, val) -> f.set(inst, Integer.parseInt(val)));
        typesOfField.put(double.class, (f, inst, val) -> f.set(inst, Double.parseDouble(val)));
        typesOfField.put(float.class, (f, inst, val) -> f.set(inst, Float.parseFloat(val)));
        typesOfField.put(char.class, (f, inst, val) -> f.set(inst, val.charAt(0)));
        typesOfField.put(long.class, (f, inst, val) -> f.set(inst, Long.parseLong(val)));
        typesOfField.put(byte.class, (f, inst, val) -> f.set(inst, Byte.parseByte(val)));
        typesOfField.put(short.class, (f, inst, val) -> f.set(inst, Short.parseShort(val)));
        typesOfField.put(boolean.class, (f, inst, val) -> f.set(inst, Boolean.parseBoolean(val)));
        typesOfField.put(String.class, (f, inst, val) -> f.set(inst, val));
        typesOfField.get(classType).setValueField(field, instanceE, value);
    }


}
