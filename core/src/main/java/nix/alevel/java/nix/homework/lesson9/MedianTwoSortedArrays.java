package nix.alevel.java.nix.homework.lesson9;

public class MedianTwoSortedArrays {
    private int[] firstArray;
    private int[] secondArray;

    public MedianTwoSortedArrays(int[] firstArray, int[] secondArray) {
        this.firstArray = firstArray;
        this.secondArray = secondArray;
    }

    public void setFirstSecondArrays(int[] firstArray, int[] secondArray) {

        this.firstArray = firstArray;
        this.secondArray = secondArray;
    }


    public double findMedianSortedArrays() {
        if (firstArray.length == 0 && secondArray.length == 0) {
            throw new IllegalArgumentException("Array is empty");
        }
        int lenNums1 = firstArray.length;
        int lenNums2 = secondArray.length;

        int finalMediana = lenNums1 + lenNums2;
        int i = 0;
        int j = 0;
        int k = 0;
        double temp = 0;
        double temp2 = 0;
        while (i <= (finalMediana / 2)) {
            i++;
            temp2 = temp;
            if (j < firstArray.length && k < secondArray.length && firstArray[j] < secondArray[k]) {
                temp = firstArray[j];
                j++;

            } else {
                if (k == secondArray.length) {
                    temp = firstArray[j];
                    j++;
                } else {
                    temp = secondArray[k];
                    k++;
                }
            }

        }
        if (finalMediana % 2 == 0) {
            return (temp + temp2) / 2.0;
        }
        return temp;
    }
}