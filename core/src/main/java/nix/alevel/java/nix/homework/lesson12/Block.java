package nix.alevel.java.nix.homework.lesson12;

public interface Block {
    void run() throws Exception;
}
