package nix.alevel.java.nix.homework.lesson3;

public class BubbleSort extends CreateArray {


    public BubbleSort(int[] array) {
        super(array);
    }

    public void bubbleSort() {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length - i - 1; j++) {
                if (array[j] > array[j + 1]) {
                    int temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                }
            }
        }
    }
}
