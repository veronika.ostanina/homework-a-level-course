package nix.alevel.java.nix.practic.ioWork;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

public class CountDigit {
    static Map<Character, Integer> countDigit = new HashMap<>();

    public static void main(String[] args) {
        CountDigit.countDigit("D:\\aleveljavanix\\core\\src\\main\\java\\com\\alevel\\java\\nix\\practic\\ioWork\\file1");
        System.out.println(countDigit);
    }

    public static void countDigit(String path) {
        File file = new File(path);
        if (!file.exists()) {
            throw new RuntimeException("File is  not exist!");
        }
        try (InputStream in = new FileInputStream(file);
             BufferedReader bufferedReader =
                     new BufferedReader(new InputStreamReader(in, StandardCharsets.UTF_8))) {

            bufferedReader.lines().
                    forEach(e -> e.codePoints().forEach(i -> countDigit.merge(Character.toChars(i)[0], 1, Integer::sum)));

        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }
}
