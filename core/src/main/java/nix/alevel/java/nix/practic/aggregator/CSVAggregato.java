package nix.alevel.java.nix.practic.aggregator;

import java.security.InvalidParameterException;

public class CSVAggregato<T> implements Aggregator<String, T> {
    @Override
    public String aggregate(T... items) {
        int len = items.length;
        if (len == 0) {
            throw new InvalidParameterException("Array is empty!");
        }
        StringBuilder finalString = new StringBuilder();
        for (int i = 0; i < len - 1; i++) {
            finalString.append(items[i].toString() + ", ");
        }
        return finalString.append(items[len - 1]).toString();
    }
}
