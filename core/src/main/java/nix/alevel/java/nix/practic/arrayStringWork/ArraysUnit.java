package nix.alevel.java.nix.practic.arrayStringWork;


import java.util.Arrays;

public class ArraysUnit {
    public static void main(String[] args) {
        String st = "asc";
        System.out.println(st.indexOf('r'));
        String g = "hj";
        System.out.println(g.charAt(0));

    }

    public int[] first(int... array) {
        int[] arraysZero = new int[0];
        int temp = 0;
        for (int i = 0, lenght = array.length; i < lenght; i++) {
            if (array[i] == 0) {
                array[temp] = i;
                temp++;
            }
        }
        arraysZero = Arrays.copyOf(array, temp);
        return arraysZero;

    }

    public void second(int... array) {
        for (int temp : array) {
            if (temp == 0) {
                continue;
            } else if (temp > 0) {
                System.out.print("Positive ");
            } else {
                System.out.print("Negative ");
            }

        }

    }

    public void third(int... array) {
        for (int i = 0, lenght = array.length; i < lenght; i++) {
            if (!(array[i] >= array[i++])) {
                System.out.println("Unicreased");
                break;
            }

        }
    }

    public void fourth(int... array) {
        int[] arraysFizz = new int[0];
        int temp = 0;
        for (int i = 0, lenght = array.length; i < lenght; i++) {
            if (array[i] % 2 == 0) {
                array[temp] = array[i];
                temp++;
            }
        }
        Arrays.copyOf(arraysFizz, temp);
    }

    public void fiveth(int... array) {
        int tempMin = array[0];
        int tempMax = array[0];
        for (int i : array) {
            if (tempMin > i) {
                tempMin = i;
            } else {
                tempMax = i;
            }
        }

        int axis = tempMax - tempMin;
        System.out.println(axis);
    }

    public void sixth(int z, int... array) {
        int quantity = 0;
        for (int i = 0, length = array.length; i < length; i++) {
            if (array[i] > z) {
                array[i] = z;
                quantity++;
            }
        }
        System.out.println(array.toString() + System.lineSeparator() + quantity);
    }

    public void seventh(double... array) {
        int temp = 0;
        for (int i = 0, lenght = array.length; array[i] != 0 && i < lenght; i++) {
            temp++;
        }
        System.out.println(temp);
    }

    public void eigth(int n, double... array) {
        int positive = 0;
        int negative = 0;
        int zero = 0;
        for (double temp : array) {
            if (temp > 0) {
                positive++;
            } else
                negative++;

            zero = n - (positive + negative);
            System.out.println("Positive: " + positive + " Negative: " + negative + " Zero: " + zero);

        }
    }

    public void nineth(double... array) {
        double tempMin = array[0];
        double tempMax = array[0];
        int tempElemMin = 0;
        int tempElemMax = 0;
        for (int i = 0, lenght = array.length; i < lenght; i++) {

            {
                if (tempMin > array[i]) {
                    tempMin = array[i];
                    tempElemMin = i;

                } else {
                    tempMax = array[i];
                    tempElemMax = i;
                }
            }

            array[tempElemMax] = tempMin;
            array[tempElemMin] = tempMax;
            System.out.println(Arrays.toString(array));


        }


    }

    public void tenth(int... array) {
        for (int i = 0, lenght = array.length; i < lenght; i++) {
            if (array[i] <= i)
                System.out.println(array[i] + System.lineSeparator());
        }
    }

    public void eleventh(int m, int l, int... array) {
        for (int temp : array) {
            if (temp % m == l)
                System.out.println(temp + System.lineSeparator());
        }

    }

    public void twelveth(int... array) {

        for (int i = 1, lenght = array.length; i < lenght; i += 2) {
            int temp = array[i];
            array[i] = array[i - 1];
            array[i - 1] = temp;
        }
        System.out.println(Arrays.toString(array));

    }

    public void thirteenth(int... array) {
        int count = 0;
        for (int temp : array) {
            if (temp == 2) {
                count++;
            }
        }
        System.out.println(count);
    }

    public void fourteenth(double... array) {
        StringBuilder temp = new StringBuilder();
        for (int i = 0, lenght = array.length; array[i] != 0 && i < lenght; i++) {
            temp.append(array[i] + " ");
        }
        System.out.println(temp);
    }

}
