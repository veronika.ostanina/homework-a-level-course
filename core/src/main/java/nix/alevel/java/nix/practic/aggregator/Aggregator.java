package nix.alevel.java.nix.practic.aggregator;

public interface Aggregator<A, T> {

    A aggregate(T[] items);

}

