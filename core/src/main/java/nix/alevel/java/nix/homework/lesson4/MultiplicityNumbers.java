package nix.alevel.java.nix.homework.lesson4;

public class MultiplicityNumbers {

    private int numberMultiplicity;
    private int[] arrayInvertedNumber = {0};
    private int[] arrayDirectNumber = {0};


    public MultiplicityNumbers(int numberMultiplicity) {
        this.numberMultiplicity = Math.abs(numberMultiplicity);
        if (numberMultiplicity != 0) {
            arrayNumber();
        }
    }

    public void setNumberMultiplicity(int numberMultiplicity) {
        this.numberMultiplicity = Math.abs(numberMultiplicity);
        if (numberMultiplicity != 0) {
            arrayNumber();
        } else {
            arrayInvertedNumber = new int[]{0};
            arrayDirectNumber = new int[]{0};
        }

    }

    private void arrayNumber() {
        int length = 0;
        long temp = 1;
        while (temp <= numberMultiplicity) {
            length++;
            temp *= 10;
        }
        arrayInvertedNumber = new int[length];
        arrayDirectNumber = new int[length];
        int tempNumberMultiplicity = numberMultiplicity;
        int DetermDigit = (int) Math.pow(10, (length - 1));
        for (int i = length - 1; i >= 0; i--) {
            int division = tempNumberMultiplicity / DetermDigit;
            arrayInvertedNumber[i] = division;
            arrayDirectNumber[length - 1 - i] = division;
            tempNumberMultiplicity -= division * DetermDigit;
            DetermDigit /= 10;
        }

    }

    public void outLeftRight() {
        outTitle(arrayDirectNumber);
    }

    public void outRightLeft() {
        outTitle(arrayInvertedNumber);
    }

    private void outTitle(int[] array) {
        StringBuilder stringOut = new StringBuilder();
        for (int i : array) {
            if (i % 3 != 0 && i % 2 == 0) {
                stringOut.append("fizz ");
            } else if (i % 2 != 0 && i % 3 == 0) {
                stringOut.append("buzz ");
            } else if (i % 2 == 0 && i % 3 == 0) {
                stringOut.append("fizzbuzz ");
            } else {
                stringOut.append(i + " ");
            }
        }
        System.out.print(stringOut.toString());
    }
}
