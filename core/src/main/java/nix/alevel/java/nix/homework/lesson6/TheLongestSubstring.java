package nix.alevel.java.nix.homework.lesson6;

public class TheLongestSubstring {
    int longestSub;

    public int getLongestSub() {
        return longestSub;
    }

    public void findLongestSubstring(String enterString) {

        int len = enterString.length();
        if (len == 0) {
            longestSub = 0;
        }
        if (len == 1) {
            longestSub = 1;
        }
        char[] enterStringArray = enterString.toCharArray();
        StringBuilder substring = new StringBuilder();
        int oldSubstring = 0, lenS = 0;
        for (int i = 0; i < len; i++) {

            if (substring.indexOf(String.valueOf(enterStringArray[i])) < 0) {
                substring.append(enterStringArray[i]);
                lenS = substring.length();
            } else {
                if (oldSubstring < lenS) {
                    oldSubstring = lenS;
                }
                substring.delete(0, substring.indexOf(String.valueOf(enterStringArray[i])) + 1);
                substring.append(enterStringArray[i]);
            }
        }

        if (oldSubstring > lenS) {
            longestSub = oldSubstring;
        } else
            longestSub = lenS;
    }


}



