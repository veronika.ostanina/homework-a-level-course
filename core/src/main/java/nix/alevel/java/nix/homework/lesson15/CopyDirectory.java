package nix.alevel.java.nix.homework.lesson15;

import java.io.File;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;


public class CopyDirectory {

    public void copyDirectory(String from, String to) {
        try {
            if (to.contains(from)) {
                throw new RuntimeException("Please, choose another directory from copying! It is parent!");
            }
            copyFiles(from, to);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    private void copyFiles(String from, String to) throws IOException {
        File file = new File(from);
        if (file.isFile()) {
            Files.copy(Paths.get(from), Paths.get(to + "\\" + file.getName()), StandardCopyOption.REPLACE_EXISTING);
            return;
        }
        to += "\\" + file.getName();
        Files.createDirectory(Paths.get(to));
        File[] files = file.listFiles();
        for (File f : files) {
            if (f.isDirectory()) {
                copyFiles(f.getPath(), to);
            } else {
                Files.copy(Paths.get(f.getPath()), Paths.get(to + "\\" + f.getName()), StandardCopyOption.REPLACE_EXISTING);
            }
        }

    }
}
