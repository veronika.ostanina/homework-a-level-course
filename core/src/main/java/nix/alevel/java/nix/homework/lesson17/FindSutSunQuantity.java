package nix.alevel.java.nix.homework.lesson17;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class FindSutSunQuantity {
    private ArrayList<Calendar> weekend = new ArrayList<>();

    public ArrayList<Calendar> getWeekend() {
        return weekend;
    }

    public void findSutSunQuantity(GregorianCalendar dateStart, GregorianCalendar dateEnd) {
        if (dateEnd.after(dateStart)) {
            countDate(dateStart, dateEnd);
        } else if (dateStart.after(dateEnd)) {
            countDate(dateEnd, dateStart);
        } else {
            throw new RuntimeException("Date is equals");
        }

    }

    private void countDate(GregorianCalendar dateStart, GregorianCalendar dateEnd) {
        weekend.clear();
        while (!dateStart.equals(dateEnd)) {
            if (dateStart.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY || dateStart.get(Calendar.DAY_OF_WEEK) == (Calendar.SUNDAY)) {
                weekend.add((Calendar) dateStart.clone());
            }
            dateStart.add(Calendar.DAY_OF_WEEK, 1);
        }
    }

}
