package nix.alevel.java.nix.programminday;

import java.time.LocalDate;

public class ProgrammersDayService {
    public static final int PROGRAMMERS_DAY = 256;
    private final DateWizard dateWizard;

    public ProgrammersDayService(DateWizard dateWizard) {
        this.dateWizard = dateWizard;
    }

    public LocalDate getProgrammingDay(int year) {
        return dateWizard.getDayOfYear(year, PROGRAMMERS_DAY);
    }


}
