package nix.alevel.java.nix.practic.dateWork;

import java.time.LocalDateTime;

public class StartEndDuration {
    private LocalDateTime start = LocalDateTime.of(3000, 12, 28, 23, 59);
    private LocalDateTime end = LocalDateTime.of(0, 1, 1, 0, 1);

    public LocalDateTime getStart() {
        return start;
    }

    public LocalDateTime getEnd() {
        return end;
    }

    public void setEqualsStartEnd(LocalDateTime temp) {
        if (start.isAfter(temp)) {
            start = temp;
        }
        if (end.isBefore(temp)) {
            end = temp;
        }
    }

}
