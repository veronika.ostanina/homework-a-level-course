package nix.alevel.java.nix.practic.aggregator;

import java.security.InvalidParameterException;

public class AvgAggregator<T extends Number> implements Aggregator<Double, T> {
    @Override
    public Double aggregate(T... items) {
        if (items.length == 0) {
            throw new InvalidParameterException("Array is empty!");
        }
        int len = items.length;
        Double arithMiddle = 0.0;
        for (T t : items) {
            arithMiddle += t.doubleValue();
        }
        return arithMiddle / len;
    }
}
