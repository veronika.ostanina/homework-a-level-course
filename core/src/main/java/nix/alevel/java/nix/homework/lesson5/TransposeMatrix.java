package nix.alevel.java.nix.homework.lesson5;

public class TransposeMatrix {
    private int[][] matrixToTranspose;

    public TransposeMatrix(int[][] matrixToTranspose) {
        if (matrixToTranspose.length == 0) {
            throw new IllegalArgumentException("Array is empty");
        }
        this.matrixToTranspose = matrixToTranspose;
    }

    public void setTransposeMatrix(int[][] matrixToTranspose) {
        if (matrixToTranspose.length == 0) {
            throw new IllegalArgumentException("Array is empty");
        }
        this.matrixToTranspose = matrixToTranspose;
    }


    public int[][] transposeMatrix() {
        int lengthCol = matrixToTranspose[0].length;
        int lengthRow = matrixToTranspose.length;
        int[][] transposedMatrix = new int[lengthCol][lengthRow];
        for (int i = 0; i < lengthCol; i++) {
            for (int j = 0; j < lengthRow; j++) {
                transposedMatrix[i][j] = matrixToTranspose[j][i];
            }
        }
        return transposedMatrix;
    }
}
