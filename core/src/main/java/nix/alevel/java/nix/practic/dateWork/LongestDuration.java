package nix.alevel.java.nix.practic.dateWork;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Collection;


public class LongestDuration {


    public Long longestDuration(Collection<LocalDateTime> collections) {
        if (collections.isEmpty()) {
            throw new RuntimeException();
        }
        StartEndDuration startEndDuration = new StartEndDuration();
        collections.stream().forEach(startEndDuration::setEqualsStartEnd);
        return Duration.between(startEndDuration.getStart(), startEndDuration.getEnd()).toDays();
    }
}
