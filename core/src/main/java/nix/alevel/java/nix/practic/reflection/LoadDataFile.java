package nix.alevel.java.nix.practic.reflection;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class LoadDataFile {
    private String path;
    private List<String> names = new ArrayList<>();
    private List<List<String>> fieldsValues = new ArrayList<>();

    public void setPath(String path) {
        this.path = path;
    }

    public List getNames() {
        return names;
    }

    public List<List<String>> getFieldsValues() {
        return fieldsValues;
    }

    public void loadFieldsNames() {
        ClassLoader classLoader = ClassLoader.getSystemClassLoader();
        File file = new File(classLoader.getResource(path).getFile());

        if (!file.exists() || !file.isFile() || file.length() == 0) {
            throw new RuntimeException("File is directory or not exist!");
        }
        try (
                InputStream in = new FileInputStream(file);
                BufferedReader bufferedReader =
                        new BufferedReader(new InputStreamReader(in, StandardCharsets.UTF_8))) {
            names = Arrays.asList(bufferedReader.readLine().split(","));
            bufferedReader.lines().forEach(e ->
            {
                if (!e.isEmpty()) {
                    fieldsValues.add(Arrays.asList(e.split(",")));
                }
            });
        } catch (FileNotFoundException e) {
            throw new UncheckedIOException(e);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }

    }
}

