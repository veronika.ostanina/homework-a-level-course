package nix.alevel.java.nix.practic.dateWork;


import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.mapping;
import static java.util.stream.Collectors.toSet;

public class SortData {


    public Map createSortedGroupedMap(Collection<LocalDateTime> collections) {
        if (collections.isEmpty()) {
            throw new RuntimeException();
        }
        return
                collections.stream().collect(Collectors.groupingBy(
                        (p) -> p.toLocalDate(),
                        TreeMap::new,
                        mapping((p) -> p.toLocalTime(), toSet())
                ));


    }


}
