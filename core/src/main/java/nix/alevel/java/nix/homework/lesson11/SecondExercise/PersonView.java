package nix.alevel.java.nix.homework.lesson11.SecondExercise;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class PersonView {

    public static void main(String[] args) {
        PersonView personView = new PersonView();
        personView.determTemperatueState();
    }

    public void determTemperatueState() {
        boolean i = true;

        while (i) {
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            System.out.print("Please one item : " + System.lineSeparator() +
                    "1 - Water " + System.lineSeparator() +
                    "2 - Iron " + System.lineSeparator() +
                    "3 - Oxygen " + System.lineSeparator() +
                    "0 - Exit " + System.lineSeparator());
            try {
                int item = Integer.parseInt(reader.readLine());
                switch (item) {
                    case 1:
                        outValuesTempState(new Water());
                        break;
                    case 2:
                        outValuesTempState(new Iron());
                        break;
                    case 3:
                        outValuesTempState(new Oxygen());
                        break;
                    case 0:
                        i = false;
                        break;
                    default:
                        System.out.println("Invalid value!!");
                        determTemperatueState();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    private void outValuesTempState(Substance substance) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Please, enter new temperature: ");
        double t = 0;
        try {
            t = Double.parseDouble(reader.readLine());
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("New  state is " + substance.heatUp(t) +
                " new temperature is " + substance.getTemperature());
    }
}
