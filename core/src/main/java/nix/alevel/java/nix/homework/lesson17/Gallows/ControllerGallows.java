package nix.alevel.java.nix.homework.lesson17.Gallows;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ControllerGallows {
    private static List<String> listWords = new ArrayList<>();
    private Word word;
    private int counterTry;
    private OutputView outputView = new OutputView();


    public static boolean startGame(String fileName) {
        ClassLoader classLoader = ClassLoader.getSystemClassLoader();
        File file = new File(classLoader.getResource(fileName).getFile());

        if (!file.exists() || !file.isFile() || file.length() == 0) {
            throw new RuntimeException("File is directory or not exist!");
        }
        try (InputStream in = new FileInputStream(file);
             BufferedReader bufferedReader =
                     new BufferedReader(new InputStreamReader(in, StandardCharsets.UTF_8))) {
            bufferedReader.lines().forEach(e ->
            {
                if (!e.isEmpty())
                    listWords.add(e);
            });
        } catch (FileNotFoundException e) {
            throw new UncheckedIOException(e);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
        return true;
    }

    public void play() {
        if (listWords.isEmpty()) {
            throw new RuntimeException("Please start game to load data");
        }
        word = new Word(getRandomWord());
        counterTry = word.getSizeWord();
        playingLoopTry();
    }


    private String getRandomWord() {
        Random random = new Random();
        return listWords.get(random.nextInt(listWords.size()));
    }

    private void playingLoopTry() {
        int i = counterTry;
        while (i != 0) {
            outputView.printGameWord(word);
            if (!word.isPresent(outputView.takePosition())) {
                i--;
                outputView.print("Your character is wrong or is has been opened! Minus try! Now you have ", i);
            }
            if (word.isWin()) {
                outputView.print("You are winner on try : ", i);
                outputView.printGameWord(word);
                break;
            }
        }

    }

}