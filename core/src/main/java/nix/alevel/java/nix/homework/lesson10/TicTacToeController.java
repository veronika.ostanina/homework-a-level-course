package nix.alevel.java.nix.homework.lesson10;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;

public class TicTacToeController {
    private static final Logger LOG = LoggerFactory.getLogger(TicTacToeController.class);
    private final String[] STATUS = {"Playing", "Winner", "Loser", "Draw"};
    private int gameSize;
    private char[][] tableGame;
    private GamerModel gamer1;
    private GamerModel gamer2;
    private TicTacToeView ticTacToeView;


    public TicTacToeController(char symbolGamer1, char symbolGamer2, int size) {
        gameSize = size;
        ticTacToeView = new TicTacToeView(size);
        gamer1 = new GamerModel(symbolGamer1, STATUS[0], size);
        gamer2 = new GamerModel(symbolGamer2, STATUS[0], size);
        tableGame = new char[gameSize][gameSize];
        for (char[] i : tableGame) {
            Arrays.fill(i, ' ');
        }
    }

    public static void main(String[] args) {
        TicTacToeController ticTacToe = new TicTacToeController('Х', '0', 3);
        ticTacToe.startGame();
    }

    public void setTicTacToe(char symbolGamer1, char symbolGamer2, int size) {
        gameSize = size;
        ticTacToeView = new TicTacToeView(size);
        gamer1 = new GamerModel(symbolGamer1, STATUS[0], size);
        gamer2 = new GamerModel(symbolGamer2, STATUS[0], size);
        tableGame = new char[gameSize][gameSize];
        for (char[] i : tableGame) {
            Arrays.fill(i, ' ');
        }
    }

    public void startGame() {
        LOG.info("Game have been started!");

        for (int i = 0, len = gameSize * gameSize; i < len; i++) {
            if (i % 2 == 0) {
                if (takePosition(gamer1).equals(STATUS[1])) {
                    gamer2.setStatus(STATUS[2]);
                    LOG.info(ticTacToeView.printResult(tableGame, gamer1, gamer2));
                    return;
                }
            } else {
                if (takePosition(gamer2).equals(STATUS[1])) {
                    gamer1.setStatus(STATUS[2]);
                    LOG.info(ticTacToeView.printResult(tableGame, gamer1, gamer2));
                    return;
                }
            }
        }
        gamer1.setStatus(STATUS[3]);
        gamer2.setStatus(STATUS[3]);
        LOG.info(ticTacToeView.printResult(tableGame, gamer1, gamer2));

    }

    private String takePosition(GamerModel gamer) {
        char gamerSymbol = gamer.getGamerSymbol();
        Position position = ticTacToeView.enterPosition(tableGame, gamerSymbol);
        int row = position.getPosHorizontal();
        int col = position.getPosVertical();

        LOG.info("Gamer [" + gamerSymbol + "] has entered horizontal position :" + row + " and vertical position: " + col);

        if (position.posValidation(gameSize, tableGame)) {
            tableGame[row][col] = gamerSymbol;
            gamer.stepTaken(row, col);
            if (gamer.isWin()) {
                gamer.setStatus(STATUS[1]);
                return STATUS[1];
            }
        } else {
            String error = "Invalid data or chosen place isn`t empty, please check your choise!";
            System.out.println(error);
            LOG.info(error);
            return takePosition(gamer);
        }
        return STATUS[0];
    }
}