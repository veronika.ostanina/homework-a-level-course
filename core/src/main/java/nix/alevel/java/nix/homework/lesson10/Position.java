package nix.alevel.java.nix.homework.lesson10;

public class Position {
    private int posHorizontal;
    private int posVertical;

    public Position(int posHorizontal, int posVertical) {
        this.posHorizontal = posHorizontal;
        this.posVertical = posVertical;
    }

    public int getPosHorizontal() {
        return posHorizontal;
    }

    public int getPosVertical() {
        return posVertical;
    }

    protected boolean posValidation(int gameSize, char[][] tableGame) {
        if (posHorizontal < 0 || posHorizontal > gameSize - 1 || posVertical < 0 || posVertical > gameSize - 1) {
            return false;
        }
        return tableGame[posHorizontal][posVertical] == ' ';

    }
}
