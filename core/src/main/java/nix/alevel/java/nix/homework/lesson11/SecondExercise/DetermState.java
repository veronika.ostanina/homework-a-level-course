package nix.alevel.java.nix.homework.lesson11.SecondExercise;

public abstract class DetermState {
    Substance.State state;

    protected Substance.State makeState(double maxValue, double middleValue, double temperature) {
        if (temperature >= maxValue) {
            state = Substance.State.GAS;
        } else if (temperature >= middleValue) {
            state = Substance.State.LIQUID;
        } else {
            state = Substance.State.SOLID_MATTER;
        }
        return state;

    }

}
