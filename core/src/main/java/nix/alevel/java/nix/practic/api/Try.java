package nix.alevel.java.nix.practic.api;

import java.util.Collection;
import java.util.function.UnaryOperator;

public class Try {

    public <E> UnaryOperator<E> manyToOne(Collection<UnaryOperator<E>> unaryOperator) {
        return unaryOperator.stream().reduce((p, b) -> e -> b.apply(p.apply(e))).orElse(UnaryOperator.identity());
    }
}
