package nix.alevel.java.nix.homework.lesson4;

public class СountUnits {
    long numberCountUnits;

    public СountUnits(long numberCountUnits) {
        this.numberCountUnits = numberCountUnits;
    }

    public void setNumberCountUnits(long numberCountUnits) {
        this.numberCountUnits = numberCountUnits;
    }

    public int numberUnits() {
        long tempCountUnits = numberCountUnits;
        int numberUnits = 0;
        while (tempCountUnits != 0) {
            numberUnits += tempCountUnits & 1;
            tempCountUnits >>>= 1;
        }
        return numberUnits;
    }
}
