package nix.alevel.java.nix.homework.lesson20;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ConsoleInputFile {
    public static void main(String[] args) {
        System.out.println(Runtime.getRuntime().availableProcessors());
        ConsoleInputFile d = new ConsoleInputFile();
        d.startWrite("thread/output.txt");
    }

    public void startWrite(String path) {
        WriteFileInput writeFileInput = new WriteFileInput(path);
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String in = "";
        while (!in.equals("quit")) {
            try {
                System.out.print("Please enter a character or \"quit\" to end  : ");
                in = reader.readLine().trim();
                writeFileInput.setInput(in);

            } catch (IOException e) {
                throw new IllegalArgumentException(e);
            }
        }
    }
}
