package nix.alevel.java.nix.homework.lesson3;

public class MinMax extends CreateArray {


    public MinMax(int[] array) {
        super(array);
    }

    public int findMin() throws Exception {
        if (array.length != 0) {
            int tempMin = array[0];
            for (int i = 1; i < array.length; i++) {
                if (tempMin > array[i]) {
                    tempMin = array[i];
                }
            }
            return tempMin;
        } else {
            throw new Exception("Array is empty!");
        }
    }

    public int findMax() throws Exception {
        if (array.length != 0) {
            int tempMax = array[0];
            for (int i = 1; i < array.length; i++) {
                if (tempMax < array[i]) {
                    tempMax = array[i];
                }
            }
            return tempMax;
        } else {
            throw new Exception("Array is empty!");
        }
    }
}