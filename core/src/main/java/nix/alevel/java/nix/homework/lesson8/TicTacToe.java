package nix.alevel.java.nix.homework.lesson8;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class TicTacToe {
    static final Logger LOG = LoggerFactory.getLogger(TicTacToe.class);
    private final int GAMESIZE = 3;
    private final String STATUSPLAYING = "Playing";
    private final String STATUSWINNER = "Winner";
    private final String STATUSLOSER = "Loser";
    private final String STATUSDRAW = "Draw";
    private char[][] tableGame = new char[GAMESIZE][GAMESIZE];
    private Gamer gamer1;
    private Gamer gamer2;

    public TicTacToe() {
    }


    public TicTacToe(char symbolGamer1, char symbolGamer2) {
        for (int i = 0; i < GAMESIZE; i++) {
            Arrays.fill(tableGame[i], ' ');
        }
        gamer1 = new Gamer(symbolGamer1, STATUSPLAYING);
        gamer2 = new Gamer(symbolGamer2, STATUSPLAYING);

    }

    public static void main(String[] args) {
        TicTacToe ticTacToe = new TicTacToe('X', '0');
        ticTacToe.startGame();
    }

    public void setTicTacToe(char symbolGamer1, char symbolGamer2) {
        for (int i = 0; i < GAMESIZE; i++) {
            Arrays.fill(tableGame[i], ' ');
        }
        gamer1 = new Gamer(symbolGamer1, STATUSPLAYING);
        gamer2 = new Gamer(symbolGamer2, STATUSPLAYING);
    }

    public void startGame() {
        LOG.info("Game have been started!");

        for (int i = 0, len = GAMESIZE * GAMESIZE; i < len; i++) {
            if (i % 2 == 0) {
                if (takePosition(gamer1).equals(STATUSWINNER)) {
                    gamer2.setStatus(STATUSLOSER);
                    printResult();
                    return;
                }
            } else {
                if (takePosition(gamer2).equals(STATUSWINNER)) {
                    gamer1.setStatus(STATUSLOSER);
                    printResult();
                    return;
                }
            }
        }
        gamer1.setStatus(STATUSDRAW);
        gamer2.setStatus(STATUSDRAW);
        printResult();

    }

    private void printResult() {
        printGameTable();
        String result = "Result game: " + "\n" +
                "Gamer 1 [" + gamer1.getGamerSymbol() + "] is " + gamer1.getStatus() + "\n" +
                "Gamer 2 [" + gamer2.getGamerSymbol() + "] is " + gamer2.getStatus();
        LOG.info(result);
        System.out.println(result);

    }

    private void printGameTable() {
        System.out.println(" _ _  _ _  _ _  ");
        for (int i = 0; i < GAMESIZE; i++) {
            for (int j = 0; j < GAMESIZE; j++) {
                System.out.print("|_" + tableGame[i][j] + "_|");
            }
            System.out.println();
        }
    }

    private String takePosition(Gamer gamer) {
        int posHorizontal;
        int posVertical;
        char gamerSymbol = gamer.getGamerSymbol();
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try {
            printGameTable();
            System.out.print("Please enter free horizontal position (0-2) [" + gamerSymbol + "] : ");
            posVertical = Integer.parseInt(reader.readLine());
            System.out.print("Please enter free  vertical position (0-2) [" + gamerSymbol + "] : ");
            posHorizontal = Integer.parseInt(reader.readLine());
            LOG.info("Gamer [" + gamerSymbol + "] has entered horizontal position :" + posHorizontal + " and vertical position: " + posVertical);
            if (posValidation(posHorizontal, posVertical)) {
                tableGame[posVertical][posHorizontal] = gamerSymbol;
                gamer.stepTaken(posHorizontal, posVertical);
                if (gamer.isWin()) {
                    gamer.setStatus(STATUSWINNER);
                    return STATUSWINNER;
                }
            } else {
                System.out.println("Invalid data or chosen place isn`t empty, please check your choise!");
                takePosition(gamer);
            }

        } catch (java.lang.NumberFormatException e) {
            System.out.println("Invalid data !");
            takePosition(gamer);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return STATUSPLAYING;
    }

    protected boolean posValidation(int posH, int posV) {
        if (posH < 0 || posH > 2 || posV < 0 || posV > 2) {
            return false;
        }
        return tableGame[posV][posH] == ' ';

    }
}
