package nix.alevel.java.nix.homework.lesson19;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.util.Properties;

public class FillPublicFields<E> {

    public E fillField(Class<E> classObject, String path) {
        E instanceE = null;
        try {
            instanceE = (E) classObject.getConstructor().newInstance();
            for (Field field : classObject.getFields()) {
                PropertyKey annotation = field.getAnnotation(PropertyKey.class);
                if (annotation != null) {
                    try {
                        setValueField(field, instanceE, field.getType().getTypeName(), loadDataInitializeVariable(annotation.value(), path));
                    } catch (IllegalAccessException e) {
                        throw new RuntimeException(e);
                    }
                }
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return instanceE;
    }

    private String loadDataInitializeVariable(String name, String path) throws IOException {
        Properties prop = new Properties();
        try (InputStream input = FillPublicFields.class.getClassLoader().getResourceAsStream(path)) {
            if (input == null) {
                throw new IllegalArgumentException("Unable to find app.properties");
            }
            prop.load(input);
        }
        return prop.getProperty(name);
    }

    private void setValueField(Field field, E instanceE, String type, String value) throws Exception {
        switch (type) {
            case "int":
                field.set(instanceE, Integer.parseInt(value));
                break;
            case "double":
                field.set(instanceE, Double.parseDouble(value));
                break;
            case "float":
                field.set(instanceE, Float.parseFloat(value));
                break;
            case "boolean":
                field.set(instanceE, Boolean.parseBoolean(value));
                break;
            case "short":
                field.set(instanceE, Short.parseShort(value));
                break;
            case "long":
                field.set(instanceE, Long.parseLong(value));
                break;
            case "char":
                field.set(instanceE, value.toCharArray()[0]);
                break;
            case "byte":
                field.set(instanceE, Byte.parseByte(value));
                break;
            default:
                field.set(instanceE, value);

        }
    }
}
