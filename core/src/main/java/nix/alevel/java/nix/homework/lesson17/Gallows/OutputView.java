package nix.alevel.java.nix.homework.lesson17.Gallows;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.List;

public class OutputView {


    public void printGameWord(Word word) {
        List<HashMap<Character, Boolean>> arrayCharacters = word.getWord();
        System.out.print("Game word : \n\" ");
        for (int i = 0, len = word.getSizeWord(); i < len; i++) {
            if (arrayCharacters.get(i).containsValue(true)) {
                System.out.print(arrayCharacters.get(i).keySet().toArray()[0] + " ");
            } else {
                System.out.print("_ ");
            }
        }
        System.out.print("\" ");
    }

    public void print(String s, int countTry) {
        System.out.println(s + countTry);
    }


    public char takePosition() {
        String charValue = "";
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            System.out.print(System.lineSeparator() + "Please enter a character or \"exit\" to end game : ");
            charValue = String.valueOf(reader.readLine()).trim();

        } catch (NumberFormatException e) {
            System.out.println("Invalid data!");
            return takePosition();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (charValue.length() != 1) {
            if (charValue.toLowerCase().equals("exit")) {
                System.exit(0);
            } else {
                System.out.println("Invalid data!");
                return takePosition();
            }
        }
        return charValue.charAt(0);
    }
}
