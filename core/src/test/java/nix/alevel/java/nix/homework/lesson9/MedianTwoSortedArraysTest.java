package nix.alevel.java.nix.homework.lesson9;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class MedianTwoSortedArraysTest {
    MedianTwoSortedArrays medianTwoSortedArrays = new MedianTwoSortedArrays(new int[]{}, new int[]{});

    @Test
    void findMedianSortedArrays() {
        assertThrows(IllegalArgumentException.class, () -> medianTwoSortedArrays.findMedianSortedArrays());
        makeShureMedian(2, new int[]{}, 2);
        makeShureMedian(1.5, new int[]{}, 1, 2);
        makeShureMedian(1.5, new int[]{1}, 2);
        makeShureMedian(2, new int[]{1, 3}, 2);
        makeShureMedian(2.5, new int[]{1, 2}, 3, 4);
        makeShureMedian(2.5, new int[]{3, 4}, 1, 2);
        makeShureMedian(4, new int[]{3, 4}, 1, 2, 5, 67, 322);
    }

    public void makeShureMedian(double median, int[] firstArray, int... secondArray) {
        medianTwoSortedArrays.setFirstSecondArrays(firstArray, secondArray);
        assertEquals(median, medianTwoSortedArrays.findMedianSortedArrays(), 0.001);
    }

}