package nix.alevel.java.nix.homework.lesson12;

public class ImplementationInterfaceBlock implements Block {
    private int checkNumberCreateExeption = 0;
    private int[] arrayEmpty;

    @Override
    public void run() throws Exception {
        if (checkNumberCreateExeption == 3) {
            arrayEmpty = new int[3];
            System.out.print(arrayEmpty[2]);
        } else {
            checkNumberCreateExeption += 1;
            System.out.print(arrayEmpty[2]);

        }
    }
}
