package nix.alevel.java.nix.homework.lesson20;

import org.junit.jupiter.api.Test;

import java.io.*;
import java.nio.charset.StandardCharsets;

import static org.junit.jupiter.api.Assertions.assertEquals;

class WriteFileInputTest {
    private final String path = "C:\\Users\\Nica_\\IdeaProjects\\homework-a-level-course\\thread\\output.txt";

    @Test
    void setInput() throws InterruptedException {
        WriteFileInput writeFileInput = new WriteFileInput(path);
        writeFileInput.setInput("ex");
        Thread.sleep(1000);
        assertEquals("ex", writeFileInput.getInput());
        writeFileInput.setInput("am");
        Thread.sleep(1000);
        assertEquals("exam", writeFileInput.getInput());
        writeFileInput.setInput("exam");
        Thread.sleep(1000);
        assertEquals("exam", writeFileInput.getInput());
        writeFileInput.setInput("ple");
        Thread.sleep(1000);
        writeFileInput.setInput("example");
        Thread.sleep(1000);
        assertEquals("example", writeFileInput.getInput());
        Thread.sleep(1000);
        writeFileInput.setInput("quit");
        assertEquals("example", getWord());


    }

    private String getWord() {
        String str = "";
        File file = new File(path);
        try (InputStream in = new FileInputStream(file);
             BufferedReader bufferedReader =
                     new BufferedReader(new InputStreamReader(in, StandardCharsets.UTF_8))) {
            return bufferedReader.readLine();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return str;
    }
}