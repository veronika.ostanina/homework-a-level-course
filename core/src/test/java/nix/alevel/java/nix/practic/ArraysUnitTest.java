package nix.alevel.java.nix.practic;

import nix.alevel.java.nix.practic.arrayStringWork.ArraysUnit;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

class ArraysUnitTest {
    ArraysUnit arraysUnit = new ArraysUnit();

    @Test
    void first() {
        int[] temp = {2, 5, 7};
        assertArrayEquals(temp, arraysUnit.first(9, 4, 0, 5, 3, 0, 44, 0));
    }

    @Test
    void second() {
        int[] temp = {2, 5, 7};
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(baos);
        System.setOut(ps);
        arraysUnit.second(3, -1, 3, 3, -4, 0);
        assertEquals("Positive Negative Positive Positive Negative ", baos.toString());
    }

    @Test
    void third() {
    }

    @Test
    void fourth() {
    }

    @Test
    void fiveth() {
    }

    @Test
    void sixth() {
    }

    @Test
    void seventh() {
    }

    @Test
    void eigth() {
    }

    @Test
    void nineth() {
    }

    @Test
    void tenth() {
    }

    @Test
    void eleventh() {
    }

    @Test
    void twelveth() {
    }

    @Test
    void thirteenth() {
    }

    @Test
    void fourteenth() {
    }

}