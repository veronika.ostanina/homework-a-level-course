package nix.alevel.java.nix.homework.lesson5;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class TeslaSharesTest {
    TeslaShares teslaShares = new TeslaShares(7.29, 2.8, 6, 9, 1.3, 8.3, 0.3);

    @Test
    void calculateMaxProfit() {
        assertEquals(7.0, teslaShares.calculateMaxProfit(), 0.001);
        teslaShares.setTeslaShares(1.6, 2.6, 4.4, 5.7, 6.9, 7.9, 8.2);
        assertEquals(6.6, teslaShares.calculateMaxProfit(), 0.001);
        teslaShares.setTeslaShares(1.6, 1.6, 1.6, 1.6, 1.6, 1.6, 1.6);
        assertEquals(0, teslaShares.calculateMaxProfit(), 0.001);
        teslaShares.setTeslaShares(8.2, 7.9, 6.9, 5.7, 4.4, 2.6, 1.6);
        assertEquals(0, teslaShares.calculateMaxProfit(), 0.001);
        teslaShares.setTeslaShares(7.29, 6, 9, 1.3, 2.8, 0.3, 8.3);
        assertEquals(8.0, teslaShares.calculateMaxProfit(), 0.001);
        teslaShares.setTeslaShares(-7.29, 6, 9, 1.3, 2.8, 0.3, 8.3);
        assertThrows(IllegalArgumentException.class, () -> teslaShares.setTeslaShares());
        teslaShares.setTeslaShares(7.29, 6, 9, 1.3, -2.8, 0.3, 8.3);
        assertThrows(IllegalArgumentException.class, () -> teslaShares.setTeslaShares());
        teslaShares.setTeslaShares(7.29, 6, 9, 1.3, 2.8, 0.3, 0);
        assertThrows(IllegalArgumentException.class, () -> teslaShares.setTeslaShares());

    }

    @Test
    void setTeslaShares() {
        assertThrows(IllegalArgumentException.class, () -> teslaShares.setTeslaShares());
    }
}