package nix.alevel.java.nix.homework.lesson3;


import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MinMaxTest {
    private int[] testArray = {13, 5, 6, 22, 7, 12, 13};
    private int[] testArrayOne = {};
    private MinMax minMax = new MinMax(testArray);


    @Test
    void findMin() throws Exception {
        assertEquals(5, minMax.findMin());

        minMax.setArray(testArrayOne);
        Exception exception = assertThrows(Exception.class, () -> {
            minMax.findMin();
        });
        String expectedMessage = "Array is empty!";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
    }


    @Test
    void findMax() throws Exception {
        assertEquals(22, minMax.findMax());
        minMax.setArray(testArrayOne);
        Exception exception = assertThrows(Exception.class, () -> {
            minMax.findMax();
        });
        String expectedMessage = "Array is empty!";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
    }

}