package nix.alevel.java.nix.homework.lesson8;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class TicTacToeTest extends TicTacToe {
    TicTacToe ticTacToeTest = new TicTacToe('0', 'X');

    @Test
    void posValidation() {
        assertTrue(ticTacToeTest.posValidation(2, 0));
        assertFalse(ticTacToeTest.posValidation(-10, 3));
    }
}