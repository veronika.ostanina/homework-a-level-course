package nix.alevel.java.nix.homework.lesson11.secondExercise;

import nix.alevel.java.nix.homework.lesson11.SecondExercise.Substance;
import nix.alevel.java.nix.homework.lesson11.SecondExercise.Water;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class WaterTest {

    Water water = new Water();

    @Test
    void heatUp() {
        Assertions.assertEquals(Substance.State.LIQUID, water.heatUp(20));
        assertEquals(Substance.State.GAS, water.heatUp(220));
        assertEquals(Substance.State.SOLID_MATTER, water.heatUp(-20));

    }
}