package nix.alevel.java.nix.practic.aggregator;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class MaxAggregatorTest {
    MaxAggregator maxAggregator = new MaxAggregator();

    @Test
    void aggregate() {
        assertThrows(ClassCastException.class, () -> maxAggregator.aggregate(1, "abc", 'c'));
        assertThrows(ArrayIndexOutOfBoundsException.class, () -> maxAggregator.aggregate());

        assertEquals(1, maxAggregator.aggregate(1).get());
        assertEquals(6, maxAggregator.aggregate(1, 6, 2, 3, 4).get());
        assertEquals(634.5, maxAggregator.aggregate(1.1, 634.5, 2.23, 4.7).get());
        assertEquals('x', maxAggregator.aggregate('a', 'b', 'c', 'x').get());
        assertEquals("xwy", maxAggregator.aggregate("aaf", "dsf", "hello", "xwy").get());


    }
}