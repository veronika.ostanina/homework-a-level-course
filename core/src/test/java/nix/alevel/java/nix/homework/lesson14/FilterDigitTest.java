package nix.alevel.java.nix.homework.lesson14;

import nix.alevel.java.nix.homework.lesson15.tryCopy2.lesson14.FilterDigit;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class FilterDigitTest {
    nix.alevel.java.nix.homework.lesson15.tryCopy2.lesson14.FilterDigit filterDigit = new FilterDigit();

    @Test
    void filterDigit() {
        assertThrows(RuntimeException.class, () -> filterDigit.filterDigit());
        assertThrows(NumberFormatException.class, () -> filterDigit.filterDigit("f", "df"));
        assertEquals(1, filterDigit.filterDigit("01F"));
        assertEquals(12, filterDigit.filterDigit("12"));
        assertEquals(12324874, filterDigit.filterDigit("1wwASF", "SS23", "24EF87", "WWF4"));
        assertEquals(14, filterDigit.filterDigit("1wwASF", "SS", "EF", "WWF4"));
        assertEquals(12324874, filterDigit.filterDigit("1wwASF", "SS23", "24EF87", "WWF4"));//
        assertEquals(12345, filterDigit.filterDigit("string 1 text", "2 string 3 text", "45"));

    }
}