package nix.alevel.java.nix.homework.lesson7;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ReverseMethodsTest {

    ReverseMethods testString = new ReverseMethods("123456789");

    @Test
    void reversedLoop() {
        assertEquals("987654321", testString.reversedLoop());
        testString.setStringReverse("");
        assertEquals("", testString.reversedLoop());
        testString.setStringReverse("a");
        assertEquals("a", testString.reversedLoop());
        testString.setStringReverse("abcd");
        assertEquals("dcba", testString.reversedLoop());
        testString.setStringReverse("abcde");
        assertEquals("edcba", testString.reversedLoop());
        String stringToRepead = "abc";
        testString.setStringReverse(stringToRepead.repeat(100));
        String stringToRepeadCheck = "cba";
        assertEquals(stringToRepeadCheck.repeat(100), testString.reversedLoop());
    }

    @Test
    void reversedStandartLib() {
        assertEquals("987654321", testString.reversedStandartLib());
        testString.setStringReverse("");
        assertEquals("", testString.reversedStandartLib());
        testString.setStringReverse("a");
        assertEquals("a", testString.reversedStandartLib());
        testString.setStringReverse("abcd");
        assertEquals("dcba", testString.reversedStandartLib());
        testString.setStringReverse("abcde");
        assertEquals("edcba", testString.reversedStandartLib());
        String stringToRepead = "abc";
        testString.setStringReverse(stringToRepead.repeat(100));
        String stringToRepeadCheck = "cba";
        assertEquals(stringToRepeadCheck.repeat(100), testString.reversedStandartLib());
    }

    @Test
    void reversedConstantTime() {
        assertEquals("987654321", testString.reversedConstantTime());
        testString.setStringReverse("");
        assertEquals("", testString.reversedConstantTime());
        testString.setStringReverse("a");
        assertEquals("a", testString.reversedConstantTime());
        testString.setStringReverse("abcd");
        assertEquals("dcba", testString.reversedConstantTime());
        testString.setStringReverse("abcde");
        assertEquals("edcba", testString.reversedConstantTime());
        String stringToRepead = "abc";
        testString.setStringReverse(stringToRepead.repeat(100));
        String stringToRepeadCheck = "cba";
        assertEquals(stringToRepeadCheck.repeat(100), testString.reversedConstantTime());


    }


}