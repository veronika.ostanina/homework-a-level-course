package nix.alevel.java.nix.homework.lesson4;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class СountUnitsTest {

    @Test
    void numberUnits() {
        СountUnits countUnits = new СountUnits(1);
        assertEquals(1, countUnits.numberUnits());
        countUnits.setNumberCountUnits(-56);
        assertEquals(59, countUnits.numberUnits());
        countUnits.setNumberCountUnits(0);
        assertEquals(0, countUnits.numberUnits());
        countUnits.setNumberCountUnits(7654656);
        assertEquals(9, countUnits.numberUnits());

    }
}