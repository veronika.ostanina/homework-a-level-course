package nix.alevel.java.nix.homework.lesson15.tryCopy2.lesson14;

import java.util.Arrays;

public class FilterDigit {

    public Long filterDigit(String... collection) {
        if (collection.length == 0) {
            throw new RuntimeException("Array is Empty");
        }
        StringBuilder stringBuilder = new StringBuilder();
        Arrays.stream(collection)
                .map(stringValue -> stringValue
                        .codePoints()
                        .filter(Character::isDigit)
                        .map(s -> Character.digit(s, 10))
                        .reduce((var1, var2) -> var1 * 10 + var2))
                .forEach((s) -> stringBuilder.append(s.isPresent() ? s.getAsInt() : ""));
        return Long.valueOf(stringBuilder.toString());

    }
}
