package nix.alevel.java.nix.practic.arithmeticProgression;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class ArithmeticProgressionTest {

    @Test
    void calculate() {

        assertThrows(ProgressionConfigurationException.class,
                () -> new ArithmeticProgression(1, 0));

        Exception exceptionOne = assertThrows(ProgressionConfigurationException.class,
                () -> new ArithmeticProgression(1, 0));
        assertEquals("step can`t be equals 0!", exceptionOne.getMessage());

        assertThrows(ProgressionConfigurationException.class,
                () -> {
                    ArithmeticProgression arithmeticProgression = new ArithmeticProgression(1, 1);
                    arithmeticProgression.calculate(0);
                });

        Exception exceptionTwo = assertThrows(ProgressionConfigurationException.class,
                () -> {
                    ArithmeticProgression arithmeticProgression = new ArithmeticProgression(1, 1);
                    arithmeticProgression.calculate(-13);
                });
        assertEquals("n can`t be less or equals 0!", exceptionTwo.getMessage());


        try {
            ArithmeticProgression arithmeticProgression = new ArithmeticProgression(0, 3);
            assertEquals(6, arithmeticProgression.calculate(3));
        } catch (ProgressionConfigurationException e) {
            System.err.println(e.toString());
        }

    }
}