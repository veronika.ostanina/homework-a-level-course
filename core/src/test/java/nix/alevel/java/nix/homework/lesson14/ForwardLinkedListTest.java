package nix.alevel.java.nix.homework.lesson14;

import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.*;

class ForwardLinkedListTest {
    private ForwardLinkedList<Integer> list = new ForwardLinkedList<>();
    private ForwardLinkedList<Object> listOne = new ForwardLinkedList<>();

    @Test
    void addItem() {
        assertEquals(0, list.size());
        list.addItem(0);
        assertEquals(1, list.size());
        list.addItem(0);
        list.addItem(1);
        list.addItem(2);
        assertEquals(4, list.size());
        list.addItem(13);
        list.addItem(13);
        assertEquals(6, list.size());
        listOne.addItem("Hello");
        assertEquals(1, listOne.size());
        listOne.addItem("My");
        listOne.addItem(1.54);
        listOne.addItem(2);
        assertEquals(4, listOne.size());
    }

    @Test
    void addElementItem() {
        list.addItem(0);
        list.addItem(1);
        list.addItem(2);
        assertFalse(list.addElementItem(23, 32));
        list.addElementItem(1, 13);
        assertEquals(4, list.size());
        assertEquals(13, list.get(1));
        list.addElementItem(0, 23);
        assertEquals(5, list.size());
        assertEquals(23, list.get(0));
        list.addElementItem(list.size() - 1, 33);
        assertEquals(6, list.size());
        assertEquals(33, list.get(list.size() - 1));


    }

    @Test
    void deleteElement() {
        assertThrows(RuntimeException.class, () -> list.deleteElement(13));
        list.addItem(0);
        assertEquals(1, list.size());
        list.deleteElement(0);
        assertEquals(0, list.size());
        list.addItem(0);
        list.addItem(1);
        list.addItem(2);
        assertEquals(3, list.size());
        list.deleteElement(0);
        list.deleteElement(1);
        assertEquals(1, list.size());
        assertEquals(2, list.get(0));

    }

    @Test
    void deleteFirst() {
        assertThrows(RuntimeException.class, () -> list.deleteFirst());
        list.addItem(0);
        list.addItem(1);
        list.addItem(2);
        list.deleteFirst();
        assertEquals(2, list.size());
        assertEquals(1, list.get(0));

    }

    @Test
    void deleteLast() {
        assertThrows(RuntimeException.class, () -> list.deleteFirst());
        list.addItem(0);
        list.addItem(1);
        list.addItem(2);
        list.deleteLast();
        assertEquals(2, list.size());
        assertEquals(1, list.get(1));
        list.deleteLast();
        assertEquals(1, list.size());
        assertEquals(0, list.getLast());
    }

    @Test
    void deleteElementItem() {
        assertThrows(RuntimeException.class, () -> list.deleteElementItem(1));
        list.addItem(0);
        assertFalse(list.deleteElementItem(23));
        list.addItem(1);
        list.addItem(2);
        list.deleteElementItem(1);
        assertEquals(2, list.size());
        list.deleteElementItem(1);
        assertEquals(1, list.size());
        assertEquals(0, list.get(0));
    }

    @Test
    void get() {
        assertThrows(RuntimeException.class, () -> list.get(1));
        list.addItem(0);
        list.addItem(1);
        list.addItem(2);
        assertThrows(RuntimeException.class, () -> list.get(11));
        assertEquals(2, list.get(2));
        assertEquals(0, list.get(0));
        listOne.addItem("Hello");
        listOne.addItem("My");
        listOne.addItem(1.54);
        listOne.addItem(2);
        assertEquals("Hello", listOne.get(0));
        assertEquals(1.54, listOne.get(2));

    }

    @Test
    void getLast() {
        assertThrows(RuntimeException.class, () -> list.deleteFirst());
        list.addItem(0);
        list.addItem(1);
        list.addItem(2);
        assertEquals(2, list.getLast());
        listOne.addItem("hello");
        assertEquals("hello", listOne.getLast());
        listOne.addItem("hello1");
        listOne.addItem("hello2");
        assertEquals("hello2", listOne.getLast());

    }

    @Test
    void getFirst() {
        assertThrows(RuntimeException.class, () -> list.deleteFirst());
        list.addItem(0);
        list.addItem(1);
        list.addItem(2);
        assertEquals(0, list.getFirst());
        listOne.addItem("hello");
        assertEquals("hello", listOne.getFirst());
        listOne.addItem("hello1");
        listOne.addItem("hello2");
        assertEquals("hello", listOne.getFirst());
    }

    @Test
    void isZero() {
        assertThrows(RuntimeException.class, () -> list.deleteFirst());
        list.addItem(0);
        assertDoesNotThrow(() -> list.isZero());
    }

    @Test
    void print() {

        assertThrows(RuntimeException.class, () -> list.print());
        list.addItem(0);
        list.addItem(1);
        list.addItem(2);
        assertEquals("0" + System.lineSeparator() + "1" + System.lineSeparator() + "2" + System.lineSeparator(), outStreamReader(list));
        listOne.addItem("Hello");
        listOne.addItem("My");
        listOne.addItem(1.54);
        listOne.addItem(2);
        assertEquals("Hello" + System.lineSeparator() + "My" + System.lineSeparator() + "1.54" + System.lineSeparator() + "2" + System.lineSeparator(), outStreamReader(listOne));


    }

    String outStreamReader(ForwardLinkedList list) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(baos);
        System.setOut(ps);
        list.print();
        return baos.toString();
    }
}