package nix.alevel.java.nix.homework.lesson19;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class FillPublicFieldsTest {

    @Test
    void fillTempTest() {
        FillPublicFields<AppProperties> fillTemp = new FillPublicFields();
        AppProperties objectToFillFields = fillTemp.fillField(AppProperties.class, "app.properties");
        assertEquals("Ann", objectToFillFields.name);
        assertEquals(52345, objectToFillFields.phone);


        objectToFillFields = fillTemp.fillField(AppProperties.class, "app1.properties");
        assertEquals("Sam", objectToFillFields.name);
        assertEquals(00000, objectToFillFields.phone);

        assertThrows(RuntimeException.class, () -> fillTemp.fillField(AppProperties.class, "ap.properties"));
        assertThrows(RuntimeException.class, () -> fillTemp.fillField(AppProperties.class, "app2.properties"));
    }
}