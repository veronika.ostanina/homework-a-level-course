package nix.alevel.java.nix.homework.lesson10;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class GamerModelTest {
    GamerModel diagonalOne = new GamerModel('X', "Playing", 3);
    GamerModel diagonalTwo = new GamerModel('0', "Playing", 3);
    GamerModel horizontal = new GamerModel('X', "Playing", 3);
    GamerModel vertical = new GamerModel('0', "Playing", 3);

    @Test
    void isWin() {
        diagonalOne.stepTaken(0, 0);
        assertFalse(diagonalOne.isWin());
        diagonalOne.stepTaken(1, 1);
        assertFalse(diagonalOne.isWin());
        diagonalOne.stepTaken(2, 1);
        assertFalse(diagonalOne.isWin());
        diagonalOne.stepTaken(2, 2);
        assertTrue(diagonalOne.isWin());

        diagonalTwo.stepTaken(0, 2);
        assertFalse(diagonalTwo.isWin());
        diagonalTwo.stepTaken(1, 1);
        assertFalse(diagonalTwo.isWin());
        diagonalTwo.stepTaken(2, 0);
        assertTrue(diagonalTwo.isWin());

        horizontal.stepTaken(0, 0);
        assertFalse(horizontal.isWin());
        horizontal.stepTaken(0, 1);
        assertFalse(horizontal.isWin());
        horizontal.stepTaken(0, 2);
        assertTrue(horizontal.isWin());


        vertical.stepTaken(0, 2);
        assertFalse(vertical.isWin());
        vertical.stepTaken(1, 2);
        assertFalse(vertical.isWin());
        vertical.stepTaken(2, 2);
        assertTrue(vertical.isWin());

    }
}