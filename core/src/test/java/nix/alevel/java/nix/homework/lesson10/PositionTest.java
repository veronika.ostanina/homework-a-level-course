package nix.alevel.java.nix.homework.lesson10;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class PositionTest {
    Position position = new Position(2, 0);
    char[][] testArray = new char[3][3];

    @Test
    void posValidation() {
        for (char[] i : testArray) {
            Arrays.fill(i, ' ');
        }
        assertTrue(position.posValidation(3, testArray));
        Position positionOne = new Position(-2, 8);
        assertFalse(positionOne.posValidation(3, testArray));
        testArray[0][0] = 'X';
        Position positionTwo = new Position(0, 0);
        assertFalse(positionTwo.posValidation(3, testArray));
        Position positionThree = new Position(0, 4);
        testArray = new char[5][5];
        for (char[] i : testArray) {
            Arrays.fill(i, ' ');
        }
        assertTrue(positionThree.posValidation(5, testArray));

    }
}