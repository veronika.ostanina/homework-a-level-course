package nix.alevel.java.nix.homework.lesson3;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

class FindMultiplicityKTest {
    private int[] testArray = {13, 5, 6, 22, 7, 12, 13};
    FindMultiplicityK findMultiplicityKTest = new FindMultiplicityK(testArray);
    private int[] expectedArray = {6, 12};
    private int[] testArrayOne = {};
    private int[] expectedArrayOne = {};

    @Test
    void findMultiplicityK() {
        assertArrayEquals(expectedArray, findMultiplicityKTest.findMultiplicityK(3));
        assertArrayEquals(expectedArrayOne, findMultiplicityKTest.findMultiplicityK(15));
        findMultiplicityKTest.setArray(testArrayOne);
        assertArrayEquals(expectedArrayOne, findMultiplicityKTest.findMultiplicityK(15));
        assertNull(findMultiplicityKTest.findMultiplicityK(0));


    }
}