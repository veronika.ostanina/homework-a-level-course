package nix.alevel.java.nix.homework.lesson17.Gallows;

import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.assertEquals;


class OutputViewTest {
    OutputView outputView = new OutputView();

    @Test
    void printGameWord() {
        Word word = new Word("hello");
        assertEquals("Game word : \n\" _ _ _ _ _ \" ", outStreamReader(word));
        word.isPresent('l');
        assertEquals("Game word : \n\" _ _ l l _ \" ", outStreamReader(word));
        word.isPresent('y');
        assertEquals("Game word : \n\" _ _ l l _ \" ", outStreamReader(word));
        word.isPresent('h');
        assertEquals("Game word : \n\" h _ l l _ \" ", outStreamReader(word));
        word.isPresent('e');
        assertEquals("Game word : \n\" h e l l _ \" ", outStreamReader(word));
        word.isPresent('o');
        assertEquals("Game word : \n\" h e l l o \" ", outStreamReader(word));


    }


    String outStreamReader(Word word) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(baos);
        System.setOut(ps);
        outputView.printGameWord(word);
        return baos.toString();
    }
}