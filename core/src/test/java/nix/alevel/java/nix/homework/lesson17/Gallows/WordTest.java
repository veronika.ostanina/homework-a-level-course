package nix.alevel.java.nix.homework.lesson17.Gallows;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class WordTest {
    Word word;

    @Test
    void getWord() {
        word = new Word("hello");
        List<HashMap<Character, Boolean>> words = new ArrayList<>();
        words.add(new HashMap<>(Map.of('h', false)));
        words.add(new HashMap<>(Map.of('e', false)));
        words.add(new HashMap<>(Map.of('l', false)));
        words.add(new HashMap<>(Map.of('l', false)));
        words.add(new HashMap<>(Map.of('o', false)));
        assertEquals(words, word.getWord());

        word = new Word("h");
        List<HashMap<Character, Boolean>> words2 = new ArrayList<>();
        words2.add(new HashMap<>(Map.of('h', false)));
        assertEquals(words2, word.getWord());

        word = new Word("abracadabra");
        List<HashMap<Character, Boolean>> words3 = new ArrayList<>();
        words3.add(new HashMap<>(Map.of('a', false)));
        words3.add(new HashMap<>(Map.of('b', false)));
        words3.add(new HashMap<>(Map.of('r', false)));
        words3.add(new HashMap<>(Map.of('a', false)));
        words3.add(new HashMap<>(Map.of('c', false)));
        words3.add(new HashMap<>(Map.of('a', false)));
        words3.add(new HashMap<>(Map.of('d', false)));
        words3.add(new HashMap<>(Map.of('a', false)));
        words3.add(new HashMap<>(Map.of('b', false)));
        words3.add(new HashMap<>(Map.of('r', false)));
        words3.add(new HashMap<>(Map.of('a', false)));
        assertEquals(words3, word.getWord());

    }

    @Test
    void getSizeWord() {
        word = new Word("hello");
        assertEquals(5, word.getSizeWord());
        word = new Word("abracadabra");
        assertEquals(11, word.getSizeWord());
        word = new Word("a");
        assertEquals(1, word.getSizeWord());

    }

    @Test
    void isPresent() {
        word = new Word("hello");
        assertTrue(word.isPresent('e'));
        assertTrue(word.isPresent('h'));
        assertTrue(word.isPresent('l'));
        assertFalse(word.isPresent('l'));
        assertFalse(word.isPresent('q'));
        assertFalse(word.isPresent('p'));
        assertFalse(word.isPresent('e'));

    }

    @Test
    void isWin() {
        word = new Word("hello");
        word.isPresent('e');
        assertFalse(word.isWin());
        word.isPresent('h');
        assertFalse(word.isWin());
        word.isPresent('o');
        word.isPresent('l');
        assertTrue(word.isWin());
    }
}