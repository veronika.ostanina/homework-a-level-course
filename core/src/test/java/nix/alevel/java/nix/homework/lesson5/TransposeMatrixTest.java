package nix.alevel.java.nix.homework.lesson5;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class TransposeMatrixTest {
    int[][] matrixZero = {{0, 1, 2},
            {3, 4, 5},
            {6, 7, 8}};
    TransposeMatrix transposeMatrix = new TransposeMatrix(matrixZero);

    @Test
    void transposeMatrix() {
        int[][] transposedMatrix = {{0, 3, 6},
                {1, 4, 7},
                {2, 5, 8}};
        assertArrayEquals(transposedMatrix, transposeMatrix.transposeMatrix());
        int[][] matrixOne = {{2, 2, 2},
                {2, 2, 2},
                {2, 2, 2}};
        transposeMatrix.setTransposeMatrix(matrixOne);
        assertArrayEquals(matrixOne, transposeMatrix.transposeMatrix());
        int[][] matrixTwo = {{0, 1, 2}};
        int[][] transposedMatrixTwo = {{0}, {1}, {2}};
        transposeMatrix.setTransposeMatrix(matrixTwo);
        assertArrayEquals(transposedMatrixTwo, transposeMatrix.transposeMatrix());
        int[][] matrixThree = {{0, 1},
                {2, 3},
                {4, 5}};
        int[][] transposedMatrixThree = {{0, 2, 4},
                {1, 3, 5}};
        transposeMatrix.setTransposeMatrix(matrixThree);
        assertArrayEquals(transposedMatrixThree, transposeMatrix.transposeMatrix());
        int[][] matrixFour = {{0, 1, 1},
                {1, 0, 1},
                {1, 1, 0}};
        transposeMatrix.setTransposeMatrix(matrixFour);
        assertArrayEquals(matrixFour, transposeMatrix.transposeMatrix());

    }

    @Test
    void setTransposeMatrix() {
        int[][] transposedMatrix = {};
        assertThrows(IllegalArgumentException.class, () -> transposeMatrix.setTransposeMatrix(transposedMatrix));
    }
}