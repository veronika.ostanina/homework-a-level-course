package nix.alevel.java.nix.homework.lesson12;


import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.*;

class RetryTest {
    Retry retry = new Retry();

    @Test
    void reTry() {
        assertThrows(Exception.class, () -> {
            retry.reTry(5, () -> {
                int a = 12 / 0;
            });
        });
        assertThrows(Exception.class, () -> {
            retry.reTry(5, () -> {
                throw new Exception("Every turn is exeption!!!");
            });
        });
        assertThrows(Exception.class, () -> {
            retry.reTry(1, new ImplementationInterfaceBlock());
        });
        assertDoesNotThrow(() -> retry.reTry(3, () -> {
            int[] array = new int[3];
            System.out.println(array[1]);
        }));

        assertEquals("0", outStreamReader(4));


    }

    String outStreamReader(int quantityRepead) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(baos);
        System.setOut(ps);
        retry.reTry(quantityRepead, new ImplementationInterfaceBlock());
        return baos.toString();
    }

}