package nix.alevel.java.nix.practic.aggregator;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class AvgAggregatorTest {

    AvgAggregator avgAggregator = new AvgAggregator();

    @Test
    void aggregate() {
        assertThrows(Exception.class, () -> avgAggregator.aggregate());
        assertEquals(2.0, avgAggregator.aggregate(2), 0.01);
        assertEquals(0.0, avgAggregator.aggregate(0), 0.01);
        assertEquals(3.6, avgAggregator.aggregate(2, 1, 3, 5, 7), 0.01);
        assertEquals(4.06, avgAggregator.aggregate(2.4, 1.8, 3.9, 5.2, 7), 0.01);
        assertEquals(4.25, avgAggregator.aggregate(1, 3, 6, 7), 0.01);

    }
}