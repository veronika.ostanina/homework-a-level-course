package nix.alevel.java.nix.homework.lesson6;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ZigZagTest {
    ZigZag zigZag = new ZigZag();

    @Test
    void makeZigZag() {


        zigZag.makeZigZag("PAYPALISHIRING", 1);
        assertEquals("PAYPALISHIRING", zigZag.getFinalString());

        zigZag.makeZigZag("PAYPALISHIRING", 2);
        assertEquals("PYAIHRNAPLSIIG", zigZag.getFinalString());

        zigZag.makeZigZag("PAYPALISHIRING", 3);
        assertEquals("PAHNAPLSIIGYIR", zigZag.getFinalString());

        zigZag.makeZigZag("PAYPALISHIRING", 4);
        assertEquals("PINALSIGYAHRPI", zigZag.getFinalString());

        zigZag.makeZigZag("PAYPALISHIRING", 5);
        assertEquals("PHASIYIRPLIGAN", zigZag.getFinalString());

    }
}