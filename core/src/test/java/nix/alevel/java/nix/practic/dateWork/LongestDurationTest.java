package nix.alevel.java.nix.practic.dateWork;

import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.time.Month;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;


class LongestDurationTest {
    ArrayList<LocalDateTime> arrayFindDuration = new ArrayList<>();
    LongestDuration longestDuration = new LongestDuration();

    @Test
    void longestDuration() {
        assertThrows(RuntimeException.class, () -> longestDuration.longestDuration(arrayFindDuration));
        arrayFindDuration.add(LocalDateTime.of(2019, Month.JUNE, 10, 2, 30));
        assertEquals(0, longestDuration.longestDuration(arrayFindDuration));
        arrayFindDuration.add(LocalDateTime.of(2034, Month.JUNE, 30, 10, 30));
        assertEquals(5499, longestDuration.longestDuration(arrayFindDuration));

        arrayFindDuration = new ArrayList<>();
        arrayFindDuration.add(LocalDateTime.of(2034, Month.JUNE, 30, 10, 30));
        arrayFindDuration.add(LocalDateTime.of(2034, Month.JUNE, 30, 10, 30));
        assertEquals(0, longestDuration.longestDuration(arrayFindDuration));

        arrayFindDuration.add(LocalDateTime.of(2034, Month.JUNE, 29, 10, 30));
        assertEquals(1, longestDuration.longestDuration(arrayFindDuration));

        arrayFindDuration.add(LocalDateTime.of(2034, Month.JUNE, 12, 22, 30));
        arrayFindDuration.add(LocalDateTime.of(2034, Month.JUNE, 30, 16, 30));
        arrayFindDuration.add(LocalDateTime.of(2004, Month.JUNE, 30, 10, 30));
        arrayFindDuration.add(LocalDateTime.of(2014, Month.JUNE, 1, 10, 30));
        assertEquals(10957, longestDuration.longestDuration(arrayFindDuration));
    }
}