package nix.alevel.java.nix.homework.lesson16;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class ReadFileAccessTest {
    ReadFileAccess readFileAccess = new ReadFileAccess();

    @Test
    void loadData() {
        assertThrows(RuntimeException.class, () -> readFileAccess.loadData("File.csv"));
        readFileAccess = new ReadFileAccess();
        assertThrows(RuntimeException.class, () -> readFileAccess.loadData("D:\\aleveljavanix\\core\\src\\test\\java\\com\\alevel\\java\\nix\\homework\\lesson16"));
        readFileAccess = new ReadFileAccess();
        assertThrows(RuntimeException.class, () -> readFileAccess.loadData("D:\\aleveljavanix\\core\\src\\main\\java\\com\\alevel\\java\\nix\\practic\\api\\repeidHeading.csv"));
        readFileAccess = new ReadFileAccess();
        assertDoesNotThrow(() -> readFileAccess.loadData("D:\\aleveljavanix\\core\\src\\main\\java\\com\\alevel\\java\\nix\\practic\\api\\normalData.csv"));
    }

    @Test
    void getHeading() {
        readFileAccess.loadData("D:\\aleveljavanix\\core\\src\\main\\java\\com\\alevel\\java\\nix\\practic\\api\\normalData.csv");
        Set head = new HashSet<String>();
        head.add("Surname");
        head.add("Name");
        head.add("Age");
        assertEquals(head, readFileAccess.getHeading());
        readFileAccess = new ReadFileAccess();
        readFileAccess.loadData("D:\\aleveljavanix\\core\\src\\main\\java\\com\\alevel\\java\\nix\\practic\\api\\empty.csv");
        assertEquals("[]", Arrays.toString(readFileAccess.getHeading().toArray()));
    }


    @Test
    void getCellValueColInt() {
        readFileAccess.loadData("D:\\aleveljavanix\\core\\src\\main\\java\\com\\alevel\\java\\nix\\practic\\api\\empty.csv");
        assertThrows(RuntimeException.class, () -> readFileAccess.getCellValue(0, 1));

        readFileAccess = new ReadFileAccess();
        readFileAccess.loadData("D:\\aleveljavanix\\core\\src\\main\\java\\com\\alevel\\java\\nix\\practic\\api\\brokenData.csv");
        assertThrows(RuntimeException.class, () -> readFileAccess.getCellValue(3, 2));
        assertEquals("", readFileAccess.getCellValue(2, 1).trim());

        readFileAccess = new ReadFileAccess();
        readFileAccess.loadData("D:\\aleveljavanix\\core\\src\\main\\java\\com\\alevel\\java\\nix\\practic\\api\\normalData.csv");
        assertThrows(RuntimeException.class, () -> readFileAccess.getCellValue(8, 1));
        assertThrows(RuntimeException.class, () -> readFileAccess.getCellValue(2, 5));

        assertEquals("Wonder", readFileAccess.getCellValue(3, 1).trim());
        assertEquals("Sara", readFileAccess.getCellValue(0, 0).trim());
        assertEquals("18", readFileAccess.getCellValue(5, 2).trim());

    }

    @Test
    void getCellValueColName() {
        readFileAccess.loadData("D:\\aleveljavanix\\core\\src\\main\\java\\com\\alevel\\java\\nix\\practic\\api\\empty.csv");
        assertThrows(RuntimeException.class, () -> readFileAccess.getCellValue(0, "Surname"));

        readFileAccess = new ReadFileAccess();
        readFileAccess.loadData("D:\\aleveljavanix\\core\\src\\main\\java\\com\\alevel\\java\\nix\\practic\\api\\brokenData.csv");
        assertThrows(RuntimeException.class, () -> readFileAccess.getCellValue(3, "Age"));
        assertEquals("", readFileAccess.getCellValue(2, "Surname"));

        readFileAccess = new ReadFileAccess();
        readFileAccess.loadData("D:\\aleveljavanix\\core\\src\\main\\java\\com\\alevel\\java\\nix\\practic\\api\\normalData.csv");
        assertThrows(RuntimeException.class, () -> readFileAccess.getCellValue(8, "Name"));
        assertThrows(RuntimeException.class, () -> readFileAccess.getCellValue(2, "Sex"));

        assertEquals("Wonder", readFileAccess.getCellValue(3, "Surname"));
        assertEquals("Sara", readFileAccess.getCellValue(0, "Name"));
        assertEquals("18", readFileAccess.getCellValue(5, "Age"));
    }
}