package nix.alevel.java.nix.homework.lesson21;

import org.junit.jupiter.api.Test;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

class HippodromeTest {
    Hippodrome hippodrome;

    @Test
    void startGame() {
        int userRate = 5;
        int quantityHourse = 6;
        hippodrome = new Hippodrome(userRate, quantityHourse);
        long place = hippodrome.startGame();
        Map places = hippodrome.getPlaces();
        long testPlace = quantityHourse - (Long) places.get(String.valueOf(userRate)) + 1;
        assertEquals(place, testPlace);

        userRate = 1;
        quantityHourse = 1;
        hippodrome = new Hippodrome(userRate, quantityHourse);
        place = hippodrome.startGame();
        places = hippodrome.getPlaces();
        testPlace = quantityHourse - (Long) places.get(String.valueOf(userRate)) + 1;
        assertEquals(place, testPlace);

        userRate = 10;
        quantityHourse = 16;
        hippodrome = new Hippodrome(userRate, quantityHourse);
        place = hippodrome.startGame();
        places = hippodrome.getPlaces();
        testPlace = quantityHourse - (Long) places.get(String.valueOf(userRate)) + 1;
        assertEquals(place, testPlace);


    }


}