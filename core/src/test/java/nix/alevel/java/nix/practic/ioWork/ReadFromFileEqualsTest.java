package nix.alevel.java.nix.practic.ioWork;

import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ReadFromFileEqualsTest {
    ReadFromFileEquals readFromFileEquals = new ReadFromFileEquals();

    @Test
    void readFromFile() {
        assertEquals("", outStreamReader("D:\\aleveljavanix\\core\\src\\test\\java\\com\\alevel\\java\\nix\\practic\\ioWork\\substr.txt", "Hello"));
        assertEquals("Hello world!!" + System.lineSeparator() +
                "Hello my dear World!!" + System.lineSeparator(), outStreamReader("D:\\aleveljavanix\\core\\src\\test\\java\\com\\alevel\\java\\nix\\practic\\ioWork\\fileSubstr.txt", "Hello"));
        assertEquals("", outStreamReader("D:\\aleveljavanix\\core\\src\\test\\java\\com\\alevel\\java\\nix\\practic\\ioWork\\fileSubstr.txt", "Work"));
        assertEquals("Hello world!!" + System.lineSeparator() +
                "Hello my dear World!!" + System.lineSeparator() +
                "Hey my dear? there no sustr!" + System.lineSeparator(), outStreamReader("D:\\aleveljavanix\\core\\src\\test\\java\\com\\alevel\\java\\nix\\practic\\ioWork\\fileSubstr.txt", "H"));
        assertEquals("Hey my dear? there no sustr!" + System.lineSeparator(),
                outStreamReader("D:\\aleveljavanix\\core\\src\\test\\java\\com\\alevel\\java\\nix\\practic\\ioWork\\fileSubstr.txt", "Hey my dear? there no sustr!"));


    }

    String outStreamReader(String path, String sub) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(baos);
        System.setOut(ps);
        readFromFileEquals.readFromFileEquals(path, sub);
        return baos.toString();
    }
}