package nix.alevel.java.nix.homework.lesson11.secondExercise;

import nix.alevel.java.nix.homework.lesson11.SecondExercise.Iron;
import nix.alevel.java.nix.homework.lesson11.SecondExercise.Substance;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class IronTest {
    Iron iron = new Iron();

    @Test
    void heatUp() {
        Assertions.assertEquals(Substance.State.LIQUID, iron.heatUp(2000));
        assertEquals(Substance.State.GAS, iron.heatUp(3220));
        assertEquals(Substance.State.SOLID_MATTER, iron.heatUp(-259));
    }
}