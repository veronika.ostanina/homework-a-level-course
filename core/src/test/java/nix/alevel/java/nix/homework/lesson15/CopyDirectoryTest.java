package nix.alevel.java.nix.homework.lesson15;

import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class CopyDirectoryTest {

    CopyDirectory copyDirectory = new CopyDirectory();
    File toCopy = new File("D:\\aleveljavanix\\core\\src\\test\\java\\com\\alevel\\java\\nix\\homework\\lesson15\\tryCopy");
    File toCopy2 = new File("D:\\aleveljavanix\\core\\src\\test\\java\\com\\alevel\\java\\nix\\homework\\lesson15\\tryCopy2");

    @Test
    void copyDirectory() {
        assertThrows(RuntimeException.class, () -> copyDirectory.copyDirectory("D:\\aleveljavanix\\core", toCopy.getPath()));
        copyDirectory.copyDirectory("D:\\aleveljavanix\\core\\log\\tic-tac-toe.txt", toCopy.getPath());
        assertEquals(1L, Arrays.stream(toCopy.listFiles()).count());
        copyDirectory.copyDirectory("D:\\aleveljavanix\\core\\src\\main\\java\\com\\alevel\\java\\nix\\homework\\lesson14", toCopy2.getPath());
        assertEquals(3L, Arrays.stream(toCopy2.listFiles()[0].listFiles()).count());

    }
}