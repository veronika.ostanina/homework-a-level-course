package nix.alevel.java.nix.homework.lesson15.tryCopy2.lesson14;

public class ListElement<E> {
    private E data;
    private ListElement<E> next;

    public ListElement(E data) {
        this.data = data;
    }

    public ListElement<E> getNext() {
        return next;
    }

    public void setNext(ListElement<E> next) {
        this.next = next;
    }

    public E getData() {
        return data;
    }

    public void setData(E data) {
        this.data = data;
    }
}
