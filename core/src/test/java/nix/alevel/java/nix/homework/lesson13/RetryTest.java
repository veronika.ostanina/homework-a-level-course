package nix.alevel.java.nix.homework.lesson13;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class RetryTest {
    Retry retry = new Retry();

    @Test
    void reTry() {
        assertThrows(Exception.class, () -> {
            retry.reTry(5, () -> {
                return 12 / 0;
            });
        });
        assertThrows(Exception.class, () -> {
            retry.reTry(5, () -> {
                throw new Exception("Every turn is exeption!!!");
            });
        });

        assertEquals(0, retry.reTry(3, () -> {
            int[] array = new int[3];
            return array[1];
        }));
        assertEquals("Block didn`t be strted", retry.reTry(0, () -> {
            int[] array = new int[3];
            return array[1];
        }));

    }
}
