package nix.alevel.java.nix.homework.lesson17;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class FindSutSunQuantityTest {

    public ArrayList<Calendar> testListOfWeekend = new ArrayList<>();
    FindSutSunQuantity findSutSunQuantity = new FindSutSunQuantity();

    @Test
    void findSutSunQuantity() {

        assertThrows(RuntimeException.class, () -> findSutSunQuantity.findSutSunQuantity(new GregorianCalendar(2020, 3, 13),
                new GregorianCalendar(2020, 3, 13)));

        findSutSunQuantity.findSutSunQuantity(new GregorianCalendar(2020, 3, 1),
                new GregorianCalendar(2020, 3, 13));
        testListOfWeekend.add(new GregorianCalendar(2020, 3, 4));
        testListOfWeekend.add(new GregorianCalendar(2020, 3, 5));
        testListOfWeekend.add(new GregorianCalendar(2020, 3, 11));
        testListOfWeekend.add(new GregorianCalendar(2020, 3, 12));
        assertEquals(testListOfWeekend, findSutSunQuantity.getWeekend());

        findSutSunQuantity.findSutSunQuantity(new GregorianCalendar(2020, 3, 13),
                new GregorianCalendar(2020, 3, 6));
        testListOfWeekend.clear();
        testListOfWeekend.add(new GregorianCalendar(2020, 3, 11));
        testListOfWeekend.add(new GregorianCalendar(2020, 3, 12));
        assertEquals(testListOfWeekend, findSutSunQuantity.getWeekend());

        findSutSunQuantity.findSutSunQuantity(new GregorianCalendar(2020, 3, 13),
                new GregorianCalendar(2020, 3, 12));
        testListOfWeekend.clear();
        testListOfWeekend.add(new GregorianCalendar(2020, 3, 12));
        assertEquals(testListOfWeekend, findSutSunQuantity.getWeekend());

        findSutSunQuantity.findSutSunQuantity(new GregorianCalendar(2020, 3, 6),
                new GregorianCalendar(2020, 3, 10));
        testListOfWeekend.clear();
        assertEquals(testListOfWeekend, findSutSunQuantity.getWeekend());

    }
}