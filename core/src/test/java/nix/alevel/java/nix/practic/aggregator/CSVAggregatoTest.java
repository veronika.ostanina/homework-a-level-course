package nix.alevel.java.nix.practic.aggregator;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class CSVAggregatoTest {
    CSVAggregato csvAggregatoTest = new CSVAggregato();

    @Test
    void aggregate() {
        assertThrows(Exception.class, () -> csvAggregatoTest.aggregate());
        assertEquals("", csvAggregatoTest.aggregate(""));
        assertEquals("1, 2, 3, 4, 123", csvAggregatoTest.aggregate(1, 2, 3, 4, 123));
        assertEquals("123, 2, 3, 4", csvAggregatoTest.aggregate("123", 2, 3, 4));
        assertEquals("a, b, c", csvAggregatoTest.aggregate("a", "b", "c"));
        assertEquals("f, 2, 3.5, hello, 123, true", csvAggregatoTest.aggregate('f', 2, 3.5, "hello", 123, true));
        assertEquals("That is test!, That is test!", csvAggregatoTest.aggregate(new CSVAggregatoTest(), new CSVAggregatoTest()));

    }

    @Override
    public String toString() {
        return "That is test!";
    }
}