package nix.alevel.java.nix.homework.lesson8;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class GamerTest {

    Gamer diagonalOne = new Gamer('X', "Playing");
    Gamer diagonalTwo = new Gamer('0', "Playing");
    Gamer horizontal = new Gamer('X', "Playing");
    Gamer vertical = new Gamer('0', "Playing");

    @Test
    void isWin() {
        diagonalOne.stepTaken(0, 0);
        assertFalse(diagonalOne.isWin());
        diagonalOne.stepTaken(1, 1);
        assertFalse(diagonalOne.isWin());
        diagonalOne.stepTaken(2, 1);
        assertFalse(diagonalOne.isWin());
        diagonalOne.stepTaken(2, 2);
        assertTrue(diagonalOne.isWin());

        diagonalTwo.stepTaken(0, 2);
        assertFalse(diagonalTwo.isWin());
        diagonalTwo.stepTaken(1, 1);
        assertFalse(diagonalTwo.isWin());
        diagonalTwo.stepTaken(2, 0);
        assertTrue(diagonalTwo.isWin());

        horizontal.stepTaken(0, 0);
        assertFalse(horizontal.isWin());
        horizontal.stepTaken(0, 1);
        assertFalse(horizontal.isWin());
        horizontal.stepTaken(0, 2);
        assertTrue(horizontal.isWin());


        vertical.stepTaken(0, 2);
        assertFalse(vertical.isWin());
        vertical.stepTaken(1, 2);
        assertFalse(vertical.isWin());
        vertical.stepTaken(2, 2);
        assertTrue(vertical.isWin());


    }
}