package nix.alevel.java.nix.homework.lesson11.secondExercise;

import nix.alevel.java.nix.homework.lesson11.SecondExercise.Oxygen;
import nix.alevel.java.nix.homework.lesson11.SecondExercise.Substance;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class OxygenTest {

    Oxygen oxygen = new Oxygen();

    @Test
    void heatUp() {

        Assertions.assertEquals(Substance.State.LIQUID, oxygen.heatUp(-200));
        assertEquals(Substance.State.GAS, oxygen.heatUp(20));
        assertEquals(Substance.State.SOLID_MATTER, oxygen.heatUp(-259));

    }
}