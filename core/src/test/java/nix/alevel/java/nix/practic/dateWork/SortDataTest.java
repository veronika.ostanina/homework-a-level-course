package nix.alevel.java.nix.practic.dateWork;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class SortDataTest {
    ArrayList<LocalDateTime> times = new ArrayList<>();
    SortData sortData = new SortData();
    HashMap<LocalDate, HashSet<LocalTime>> hashSetHashMap = new HashMap<>();

    @Test
    void createSortedGroupedMap() {
        assertThrows(RuntimeException.class, () -> sortData.createSortedGroupedMap(times));

        times.add(LocalDateTime.of(2019, Month.JUNE, 10, 2, 30));
        assertEquals("{" + times.get(0).toLocalDate() + "=[" + times.get(0).toLocalTime() + "]}", sortData.createSortedGroupedMap(times).toString());

        times.add(LocalDateTime.of(2019, Month.JUNE, 10, 2, 30));
        assertEquals("{" + times.get(0).toLocalDate() + "=[" + times.get(0).toLocalTime() + "]}", sortData.createSortedGroupedMap(times).toString());

        times.add(LocalDateTime.of(2019, Month.JUNE, 10, 2, 31));
        assertEquals("{" + times.get(0).toLocalDate() + "=[" + times.get(0).toLocalTime() + ", " + times.get(2).toLocalTime() + "]}", sortData.createSortedGroupedMap(times).toString());

        times.add(LocalDateTime.of(2034, Month.AUGUST, 12, 22, 30));
        assertEquals("{" + times.get(0).toLocalDate() + "=[" + times.get(0).toLocalTime() + ", " + times.get(2).toLocalTime() + "], "
                + times.get(3).toLocalDate() + "=[" + times.get(3).toLocalTime() + "]}", sortData.createSortedGroupedMap(times).toString());

        times.add(LocalDateTime.of(2014, Month.JUNE, 1, 10, 30));
        assertEquals("{" + times.get(4).toLocalDate() + "=[" + times.get(4).toLocalTime() + "], "
                + times.get(0).toLocalDate() + "=[" + times.get(0).toLocalTime() + ", " + times.get(2).toLocalTime() + "], "
                + times.get(3).toLocalDate() + "=[" + times.get(3).toLocalTime() + "]}", sortData.createSortedGroupedMap(times).toString());


    }
}