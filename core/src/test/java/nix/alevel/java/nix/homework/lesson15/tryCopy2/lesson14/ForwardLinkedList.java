package nix.alevel.java.nix.homework.lesson15.tryCopy2.lesson14;

import java.util.AbstractList;

public class ForwardLinkedList<E> extends AbstractList<E> {
    private nix.alevel.java.nix.homework.lesson15.tryCopy2.lesson14.ListElement<E> head;
    private nix.alevel.java.nix.homework.lesson15.tryCopy2.lesson14.ListElement<E> tail;
    private int size = 0;

    public boolean hasNext(nix.alevel.java.nix.homework.lesson15.tryCopy2.lesson14.ListElement temp) {

        return temp.getNext() != null;
    }

    public boolean addItem(E object) {
        nix.alevel.java.nix.homework.lesson15.tryCopy2.lesson14.ListElement<E> newElement = new nix.alevel.java.nix.homework.lesson15.tryCopy2.lesson14.ListElement<E>(object);
        if (size == 0) {
            head = newElement;
            tail = newElement;
            size = 1;
        } else {
            tail.setNext(newElement);
            tail = newElement;
            size++;
        }
        return true;
    }

    public boolean addElementItem(int index, E object) {
        isZero();
        if (index >= size) {
            return false;
        }
        nix.alevel.java.nix.homework.lesson15.tryCopy2.lesson14.ListElement<E> temp = head;
        nix.alevel.java.nix.homework.lesson15.tryCopy2.lesson14.ListElement<E> newElement = new nix.alevel.java.nix.homework.lesson15.tryCopy2.lesson14.ListElement<E>(object);
        if (index == 0) {
            newElement.setNext(head);
            head = newElement;
        }
        if (index == size - 1) {
            this.addItem(object);
            return true;
        }
        for (int i = 0, len = index - 1; i < len; i++) {
            temp = temp.getNext();
        }
        newElement.setNext(temp.getNext());
        temp.setNext(newElement);
        size++;
        return true;
    }

    @Override
    public E get(int index) {
        isZero();
        if (index >= size) {
            throw new RuntimeException("Invalid index");
        }
        if (index == 0) {
            return head.getData();
        }
        if (index == size - 1) {
            return tail.getData();
        }
        nix.alevel.java.nix.homework.lesson15.tryCopy2.lesson14.ListElement<E> temp = head;
        for (int i = 0; i < index; i++) {
            temp = temp.getNext();
        }
        return temp.getData();
    }

    public E getLast() {
        isZero();
        return tail.getData();
    }

    public E getFirst() {
        isZero();
        return head.getData();
    }

    @Override
    public int size() {
        return size;
    }

    public boolean deleteFirst() {
        isZero();
        nix.alevel.java.nix.homework.lesson15.tryCopy2.lesson14.ListElement<E> temp;
        temp = head.getNext();
        head.setNext(null);
        head = temp;
        size--;
        return true;
    }

    public boolean deleteLast() {
        isZero();
        nix.alevel.java.nix.homework.lesson15.tryCopy2.lesson14.ListElement<E> temp = head;
        for (int i = 0, len = size - 2; i < len; i++) {
            temp = temp.getNext();
        }
        tail = temp;
        temp.setNext(null);
        size--;
        return true;
    }

    public boolean deleteElement(E data) {
        isZero();

        if (size == 1) {
            head = null;
            tail = null;
            size = 0;
            return true;
        }
        if (head.getData() == data) {
            head = head.getNext();
            size--;
            return true;
        }

        nix.alevel.java.nix.homework.lesson15.tryCopy2.lesson14.ListElement temp = head;
        while (hasNext(temp)) {
            if (temp.getNext().getData() == data) {
                if (tail == temp.getNext()) {
                    tail = temp;
                }
                temp.setNext(temp.getNext().getNext());
                size--;
                return true;
            }
            temp = temp.getNext();
        }
        return false;
    }

    public boolean deleteElementItem(int index) {
        isZero();
        nix.alevel.java.nix.homework.lesson15.tryCopy2.lesson14.ListElement<E> temp = head;
        ListElement<E> temp2;
        if (index >= size) {
            return false;
        }
        if (index == 0) {
            deleteFirst();
            return true;
        }
        if (index == size - 1) {
            deleteLast();
            return true;
        }

        for (int i = 0, len = index - 2; i < len; i++) {
            temp = temp.getNext();
        }
        temp2 = temp.getNext();
        temp.setNext(temp.getNext().getNext());


        temp2.setNext(null);
        size--;
        return true;
    }

    public void isZero() {
        if (size == 0) {
            throw new RuntimeException("List is Empty");
        }
    }

    public void print() {
        isZero();
        for (int i = 0; i < size; i++) {
            System.out.println(this.get(i));
        }
    }
}
