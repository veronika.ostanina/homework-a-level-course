package nix.alevel.java.nix.practic.aggregator;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class DistinctAggregatorTest {
    DistinctAggregator distinctAggregator = new DistinctAggregator();

    @Test
    void aggregate() {
        assertEquals(0, distinctAggregator.aggregate());
        assertEquals(1, distinctAggregator.aggregate('1'));
        assertEquals(1, distinctAggregator.aggregate(distinctAggregator, distinctAggregator));
        assertEquals(2, distinctAggregator.aggregate(new DistinctAggregatorTest(), new DistinctAggregatorTest()));
        assertEquals(5, distinctAggregator.aggregate("hello", 'h', 'e', 'l', 'l', '0'));
    }
}