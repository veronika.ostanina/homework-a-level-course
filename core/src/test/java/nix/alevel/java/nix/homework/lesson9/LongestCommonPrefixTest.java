package nix.alevel.java.nix.homework.lesson9;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class LongestCommonPrefixTest {
    String[] arrayTestPrefix = new String[0];
    LongestCommonPrefix longestCommonPrefix = new LongestCommonPrefix(arrayTestPrefix);

    @Test
    void longestCommonPrefix() {
        assertEquals("", longestCommonPrefix.longestCommonPrefix());
        makeCheck("", "shake", "snake", "");
        makeCheck("", "cat", "dad", "set");
        makeCheck("cat", "cat");
        makeCheck("s", "snake", "shake", "snake");
        makeCheck("co", "column", "col", "coco");
        makeCheck("cat", "cat", "catdad", "catdadset");
        makeCheck("fat", "fatradfly", "fatradlazycat", "fat");
        makeCheck("", "mycat", "yourcat", "thatcat");
        makeCheck("cat", "cat", "cat", "cat");
        makeCheck("s", "snake", "shake", "snake", "salary", "someday", "sometimes", "solo", "snake", "shake", "snake", "salary", "someday", "sometimes", "solo");


    }

    private void makeCheck(String prefix, String... array) {
        longestCommonPrefix.setStrs(array);
        assertEquals(prefix, longestCommonPrefix.longestCommonPrefix());
    }
}