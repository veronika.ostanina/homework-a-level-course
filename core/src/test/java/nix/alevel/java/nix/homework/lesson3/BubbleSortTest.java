package nix.alevel.java.nix.homework.lesson3;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

class BubbleSortTest {
    private int[] testArray = {13, 5, 6, 22, 7, 12, 13};
    BubbleSort bubbleSortTest = new BubbleSort(testArray);
    private int[] expectedArray = {5, 6, 7, 12, 13, 13, 22};
    private int[] testArrayOne = {1, 2, 3, 4, 5, 6};
    private int[] expectedArrayOne = {1, 2, 3, 4, 5, 6};
    private int[] testArrayTwo = {6, 5, 4, 3, 2, 1};

    @Test
    void bubbleSort() {
        bubbleSortTest.bubbleSort();
        assertArrayEquals(expectedArray, bubbleSortTest.getArray());
        bubbleSortTest.setArray(testArrayOne);
        bubbleSortTest.bubbleSort();
        assertArrayEquals(expectedArrayOne, bubbleSortTest.getArray());
        bubbleSortTest.setArray(testArrayTwo);
        bubbleSortTest.bubbleSort();
        assertArrayEquals(expectedArrayOne, bubbleSortTest.getArray());

    }

}