package nix.alevel.java.nix.homework.lesson17.Gallows;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

class ControllerGallowsTest {

    @Test
    void startGame() {
        assertThrows(RuntimeException.class, () -> ControllerGallows.startGame("directoryTry"));
        assertThrows(RuntimeException.class, () -> ControllerGallows.startGame("notExist.txt"));
        assertTrue(ControllerGallows.startGame("word.txt"));
    }

}