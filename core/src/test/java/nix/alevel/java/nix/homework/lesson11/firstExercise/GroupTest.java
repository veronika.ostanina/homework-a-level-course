package nix.alevel.java.nix.homework.lesson11.firstExercise;

import nix.alevel.java.nix.homework.lesson11.FirstExercise.ContractStudent;
import nix.alevel.java.nix.homework.lesson11.FirstExercise.Group;
import nix.alevel.java.nix.homework.lesson11.FirstExercise.Student;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class GroupTest {
    Student[] students;
    Group group;

    @Test
    void checkInit() {
        Exception exceptionOne = assertThrows(IllegalArgumentException.class, () -> {
            group = new Group(students);
        });
        assertEquals("Array is Empty!", exceptionOne.getMessage());
        students = new Student[0];
        Exception exceptionTwo = assertThrows(IllegalArgumentException.class, () -> {
            group = new Group(students);
        });
        assertEquals("Array is Empty!", exceptionTwo.getMessage());

        students = new Student[2];
        students[0] = new ContractStudent("Ann", 18, 27967.8);
        Exception exceptionThree = assertThrows(IllegalArgumentException.class, () -> {
            group = new Group(students);
        });
        assertEquals("Array has element Null!", exceptionThree.getMessage());

    }

    @Test
    void showStudents() {
        students = new Student[1];
        students[0] = new ContractStudent("David", 19, 27000);
        group = new Group(students);
        assertEquals("Name is David cost of contract is 27000.0" + System.lineSeparator(), outStreamReader(group));

        students[0] = new Student("David", 19);
        group = new Group(students);
        assertEquals("", outStreamReader(group));

        students = new Student[2];
        students[0] = new ContractStudent("Ann", 18, 27967.8);
        students[1] = new Student("Tom", 19);
        group = new Group(students);
        assertEquals("Name is Ann cost of contract is 27967.8" + System.lineSeparator(), outStreamReader(group));

        students = new Student[5];
        students[0] = new Student("Ann", 18);
        students[1] = new Student("Tom", 19);
        students[2] = new ContractStudent("David", 19, 28000);
        students[3] = new ContractStudent("Julia", 19, 28000);
        students[4] = new Student("Mary", 18);
        group = new Group(students);
        assertEquals("Name is David cost of contract is 28000.0" + System.lineSeparator() +
                "Name is Julia cost of contract is 28000.0" + System.lineSeparator(), outStreamReader(group));
    }

    String outStreamReader(Group group) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(baos);
        System.setOut(ps);
        group.showContractStudents();
        return baos.toString();
    }
}