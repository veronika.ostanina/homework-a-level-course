package nix.alevel.java.nix.homework.lesson10;

import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;

class TicTacToeViewTest {
    TicTacToeView ticTacToeView = new TicTacToeView(3);

    @Test
    void printGameTable() {
        String outTest = " _ _  _ _  _ _ " + System.lineSeparator() +
                "|_ _||_ _||_ _|" + System.lineSeparator() +
                "|_ _||_ _||_ _|" + System.lineSeparator() +
                "|_ _||_ _||_ _|" + System.lineSeparator();
        char[][] testArray = new char[3][3];
        for (char[] i : testArray) {
            Arrays.fill(i, ' ');
        }
        assertEquals(outTest, outStreamReader(testArray));
        String outTestTwo = " _ _  _ _  _ _ " + System.lineSeparator() +
                "|_ _||_ _||_0_|" + System.lineSeparator() +
                "|_X_||_ _||_ _|" + System.lineSeparator() +
                "|_ _||_0_||_ _|" + System.lineSeparator();
        testArray[0][2] = '0';
        testArray[2][1] = '0';
        testArray[1][0] = 'X';
        assertEquals(outTestTwo, outStreamReader(testArray));
        String outTestThree = " _ _  _ _  _ _ " + System.lineSeparator() +
                "|_0_||_0_||_0_|" + System.lineSeparator() +
                "|_X_||_X_||_0_|" + System.lineSeparator() +
                "|_X_||_0_||_X_|" + System.lineSeparator();
        testArray[0][0] = '0';
        testArray[1][1] = 'X';
        testArray[2][2] = 'X';
        testArray[0][1] = '0';
        testArray[1][2] = '0';
        testArray[2][0] = 'X';
        assertEquals(outTestThree, outStreamReader(testArray));
    }

    String outStreamReader(char[][] testArray) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(baos);
        System.setOut(ps);
        ticTacToeView.printGameTable(testArray);
        return baos.toString();
    }
}