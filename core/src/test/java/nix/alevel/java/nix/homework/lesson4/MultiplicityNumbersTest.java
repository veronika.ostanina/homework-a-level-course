package nix.alevel.java.nix.homework.lesson4;

import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class MultiplicityNumbersTest {


    @Test
    void outLeftRight() {
        MultiplicityNumbers multiplicityNumbers = new MultiplicityNumbers(347_693_485);
        assertEquals("buzz fizz 7 fizzbuzz buzz buzz fizz fizz 5 ", outStreamReaderLR(multiplicityNumbers));
        multiplicityNumbers.setNumberMultiplicity(0);
        assertEquals("fizzbuzz ", outStreamReaderLR(multiplicityNumbers));
        multiplicityNumbers.setNumberMultiplicity(-347_693_485);
        assertEquals("buzz fizz 7 fizzbuzz buzz buzz fizz fizz 5 ", outStreamReaderLR(multiplicityNumbers));
        multiplicityNumbers.setNumberMultiplicity(248_157_39_6);
        assertEquals("fizz fizz fizz 1 5 7 buzz buzz fizzbuzz ", outStreamReaderLR(multiplicityNumbers));
        multiplicityNumbers.setNumberMultiplicity(1);
        assertEquals("1 ", outStreamReaderLR(multiplicityNumbers));

    }

    @Test
    void outRightLeft() {
        MultiplicityNumbers multiplicityNumbers = new MultiplicityNumbers(347_693_485);
        assertEquals("5 fizz fizz buzz buzz fizzbuzz 7 fizz buzz ", outStreamReaderRL(multiplicityNumbers));
        multiplicityNumbers.setNumberMultiplicity(0);
        assertEquals("fizzbuzz ", outStreamReaderRL(multiplicityNumbers));
        multiplicityNumbers.setNumberMultiplicity(-347_693_485);
        assertEquals("5 fizz fizz buzz buzz fizzbuzz 7 fizz buzz ", outStreamReaderRL(multiplicityNumbers));
        multiplicityNumbers.setNumberMultiplicity(248_157_39_6);
        assertEquals("fizzbuzz buzz buzz 7 5 1 fizz fizz fizz ", outStreamReaderRL(multiplicityNumbers));
        multiplicityNumbers.setNumberMultiplicity(1);
        assertEquals("1 ", outStreamReaderRL(multiplicityNumbers));
    }

    String outStreamReaderLR(MultiplicityNumbers multiplicityNumbers) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(baos);
        System.setOut(ps);
        multiplicityNumbers.outLeftRight();
        return baos.toString();
    }

    String outStreamReaderRL(MultiplicityNumbers multiplicityNumbers) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(baos);
        System.setOut(ps);
        multiplicityNumbers.outRightLeft();
        return baos.toString();
    }
}