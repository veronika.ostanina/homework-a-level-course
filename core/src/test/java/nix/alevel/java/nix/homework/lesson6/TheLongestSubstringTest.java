package nix.alevel.java.nix.homework.lesson6;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class TheLongestSubstringTest {
    TheLongestSubstring theLongestSubstring = new TheLongestSubstring();

    @Test
    void findLongestSubstring() {
        theLongestSubstring.findLongestSubstring("");
        assertEquals(0, theLongestSubstring.getLongestSub());
        theLongestSubstring.findLongestSubstring("a");
        assertEquals(1, theLongestSubstring.getLongestSub());
        theLongestSubstring.findLongestSubstring("bbbbbb");
        assertEquals(1, theLongestSubstring.getLongestSub());
        theLongestSubstring.findLongestSubstring("aolkjhg");
        assertEquals(7, theLongestSubstring.getLongestSub());
        theLongestSubstring.findLongestSubstring("au");
        assertEquals(2, theLongestSubstring.getLongestSub());
        theLongestSubstring.findLongestSubstring("abcabcbb");
        assertEquals(3, theLongestSubstring.getLongestSub());
        theLongestSubstring.findLongestSubstring("abracadabra");
        assertEquals(4, theLongestSubstring.getLongestSub());
    }
}