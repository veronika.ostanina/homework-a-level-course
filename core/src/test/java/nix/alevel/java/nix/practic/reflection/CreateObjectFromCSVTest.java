package nix.alevel.java.nix.practic.reflection;

import nix.alevel.java.nix.homework.lesson19.AppProperties;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CreateObjectFromCSVTest {

    @Test
    void fillField() {
        CreateObjectFromCSV fillTemp = new CreateObjectFromCSV();
        List<AppProperties> objectToFillFields = fillTemp.fillField(AppProperties.class, "test.csv");
        List<AppProperties> example = new ArrayList(List.of(
                new AppProperties("Ann", 9, 98990),
                new AppProperties("Tom", 7, 98990),
                new AppProperties("Som", 9, 98990)
        ));
        for (int i = 0; i < example.size(); i++) {
            assertEquals(example.get(i).getMark(), objectToFillFields.get(i).getMark());
            assertEquals(example.get(i).name, objectToFillFields.get(i).name);
            assertEquals(example.get(i).age, objectToFillFields.get(i).age);
        }

    }

}