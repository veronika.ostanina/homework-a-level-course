package nix.alevel.mincost.pathfinder;


import java.util.LinkedHashMap;

public class Town {
    private LinkedHashMap<Integer, Integer> neighbors = new LinkedHashMap<>();
    private int countNeighbors;
    private String nameTown;
    private int numberTown;

    public Town(String nameTown, int countNeighbors, int number) {
        if (countNeighbors == 0 || number == 0) {
            throw new IllegalArgumentException("Number town or count of neighbors is 0");
        }
        this.nameTown = nameTown;
        this.countNeighbors = countNeighbors;
        this.numberTown = number;
    }

    public void setTownNeighbors(LinkedHashMap<Integer, Integer> neighbors) {
        this.neighbors = neighbors;
    }

    public LinkedHashMap<Integer, Integer> getNeighbors() {
        return neighbors;
    }

    public String getNameTown() {
        return nameTown;
    }

    public int getNumberTown() {
        return numberTown;
    }
}
