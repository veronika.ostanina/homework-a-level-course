package nix.alevel.mincost.pathfinder;

public class TownsCost {

    private String townFrom;
    private String townTo;
    private int cost;
    private int problems;

    public TownsCost() {
    }

    public TownsCost(String townFrom, String townTo) {
        if (townFrom.equals(townTo)) {
            throw new IllegalArgumentException("Towns are the same");
        }
        this.townFrom = townFrom;
        this.townTo = townTo;
    }

    public TownsCost(String townFrom, String townTo, int cost) {
        if (townFrom.equals(townTo)) {
            throw new IllegalArgumentException("Towns are the same");
        }
        this.townFrom = townFrom;
        this.townTo = townTo;
        this.cost = cost;
    }

    public int getProblems() {
        return problems;
    }

    public void setProblems(int problems) {
        this.problems = problems;
    }

    public String getTownFrom() {
        return townFrom;
    }

    public void setTownFrom(String townFrom) {
        this.townFrom = townFrom;
    }

    public String getTownTo() {
        return townTo;
    }

    public void setTownTo(String townTo) {
        this.townTo = townTo;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }
}