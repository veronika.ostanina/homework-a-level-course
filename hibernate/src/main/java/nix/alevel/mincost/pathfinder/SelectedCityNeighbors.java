package nix.alevel.mincost.pathfinder;

import java.util.LinkedHashMap;
import java.util.Map;

public class SelectedCityNeighbors {
    private int id;
    private String name;
    private LinkedHashMap<Integer, Integer> neighbors = new LinkedHashMap<>();

    public SelectedCityNeighbors() {
    }

    public SelectedCityNeighbors(int id, String name, Map<Integer, Integer> neighbors) {
        this.id = id;
        this.name = name;
        this.neighbors = (LinkedHashMap<Integer, Integer>) neighbors;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSizeNeighbors() {
        return neighbors.size();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LinkedHashMap<Integer, Integer> getNeighbors() {
        return neighbors;
    }

    public void setNeighbors(int to, int cost) {
        neighbors.put(to, cost);
    }
}
