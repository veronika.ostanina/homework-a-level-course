package nix.alevel.mincost.pathfinder;


import java.util.ArrayList;
import java.util.List;

public class ProfitableWays {
    private final List<Town> towns = new ArrayList<>();
    private List<TownsCost> townCalculateMinCost = new ArrayList<>();
    private int townsSize;

    public ProfitableWays setTownCalculateMinCostSize(int townCalculateMinCostSize) {
        if (townCalculateMinCostSize > 100) {
            throw new IllegalArgumentException("Calculated min cost quantity  is more 100");
        }
        return this;
    }

    public ProfitableWays setTownsSize(int townsSize) {
        if (townsSize > 10_000) {
            throw new IllegalArgumentException(" Town`s quantity  is more 10 000");
        }
        this.townsSize = townsSize;
        return this;
    }

    public ProfitableWays fullTownCalculateMinCost(List<TownsCost> townCalculateMinCost) {
        this.townCalculateMinCost = townCalculateMinCost;
        return this;
    }

    public ProfitableWays fullTowns(List<SelectedCityNeighbors> cities) {

        for (int j = 0; j < townsSize; j++) {
            Town town = new Town(cities.get(j).getName(), cities.get(j).getSizeNeighbors(), cities.get(j).getId());
            town.setTownNeighbors(cities.get(j).getNeighbors());
            towns.add(town);
        }

        return this;
    }

    public void build() {
        if (towns.isEmpty() || townCalculateMinCost.isEmpty()) {
            throw new IllegalArgumentException("There are town`s quantity is 0 or ways to calculate is 0");
        }
    }

    public void calculateSolution() {
        for (TownsCost resultCost : townCalculateMinCost) {
            CalculateMinCost calculateMinCost = new CalculateMinCost();
            resultCost.setCost(calculateMinCost.findMinCost(resultCost.getTownFrom(), resultCost.getTownTo(), towns));
        }

    }

    public List<TownsCost> getTownCalculateMinCost() {
        return townCalculateMinCost;
    }
}




