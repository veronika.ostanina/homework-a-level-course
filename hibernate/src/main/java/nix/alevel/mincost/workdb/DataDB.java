package nix.alevel.mincost.workdb;

import nix.alevel.homework.*;
import nix.alevel.mincost.pathfinder.SelectedCityNeighbors;
import nix.alevel.mincost.pathfinder.TownsCost;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;
import java.util.*;


public class DataDB implements AutoCloseable {

    private static final Logger LOG = LoggerFactory.getLogger(DataDB.class);
    private Configuration configuration = new Configuration().configure();
    private DefaultValue initialData;
    private SessionFactory sessionFactory;
    private Session session;

    public DataDB() {
        sessionFactory = configuration.buildSessionFactory();
        session = sessionFactory.openSession();
        session.beginTransaction();
    }

    public void fillDB(List<String> names, List<TownsCost> neighborsCost, List<TownsCost> townsFindCost) {
        initialData = new DefaultValue(names, neighborsCost, townsFindCost);
        insertValues();
    }

    public void fillDB() {
        initialData = new DefaultValue();
        insertValues();
    }

    private void insertValues() {

        Map<String, City> mapCity = new HashMap<>();

        try {
            for (String city : initialData.getNames()) {
                City newCityDB = new City(city);
                mapCity.put(city, newCityDB);
                session.saveOrUpdate(newCityDB);
            }
            for (TownsCost connectionCost : initialData.getNeighborsCost()) {
                Connection connectionDB = new Connection(connectionCost.getCost(),
                        mapCity.get(connectionCost.getTownFrom()),
                        mapCity.get(connectionCost.getTownTo()));
                session.saveOrUpdate(connectionDB);
            }
            for (TownsCost problem : initialData.getCalculateCost()) {
                Problem problemDB = new Problem(mapCity.get(problem.getTownFrom()),
                        mapCity.get(problem.getTownTo()));
                session.saveOrUpdate(problemDB);
            }
            session.getTransaction().commit();

        } catch (Exception e) {
            session.getTransaction().rollback();
            LOG.error("Error during transaction", e);
        }

    }


    public int getCountCity() {


        try {
            Query<City> cities = session.createQuery("from City", City.class);
            LOG.info(String.valueOf(cities.list().size()));
            return cities.list().size();

        } catch (Exception e) {
            session.getTransaction().rollback();
            LOG.error("Error during transaction", e);
            throw new RuntimeException(e);
        }


    }

    public List<SelectedCityNeighbors> fillSelectedCityNeighbors() {
        List<SelectedCityNeighbors> cities = new ArrayList<>();

        try {
            Query<City> setCity = session.createQuery("from City", City.class);
            Query<Connection> setConnection = session.createQuery("from Connection", Connection.class);
            for (City city : setCity.list()) {

                cities.add(new SelectedCityNeighbors(city.getIdCity(), city.getName(), getListNeighbors(city, setConnection.list())));
            }
            return cities;
        } catch (Exception e) {
            session.getTransaction().rollback();
            LOG.error("Error during transaction", e);
            throw new RuntimeException(e);
        }


    }

    private Map<Integer, Integer> getListNeighbors(City city, List<Connection> connections) {
        Map<Integer, Integer> neighbors = new LinkedHashMap<>();
        for (Connection e : connections) {
            if (e.getFromCity().equals(city)) {
                neighbors.put(e.getToCity().getIdCity(), e.getCost());
            }
        }
        return neighbors;
    }

    public int getCountProblems() {


        try {


            Query<Problem> problems = session.createQuery("from Problem", Problem.class);
            LOG.info(String.valueOf(problems.list().size()));
            return problems.list().size();

        } catch (Exception e) {
            session.getTransaction().rollback();
            LOG.error("Error during transaction", e);
            throw new RuntimeException(e);


        }

    }

    public List<TownsCost> getProblems() {
        List<TownsCost> problems = new ArrayList<>();

        try {
            CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
            CriteriaQuery<Problem> problem = criteriaBuilder.createQuery(Problem.class);
            Root<Problem> root = problem.from(Problem.class);

            problem.select(root)
                    .where(root.join("foundRout", JoinType.LEFT).isNull(),
                            root.join("impossibleRout", JoinType.LEFT).isNull());

            LOG.info(String.valueOf(problem));
            Query<Problem> loadProblems = session.createQuery(problem);
            for (Problem probl : loadProblems.list()) {
                problems.add(new TownsCost(probl.getFromCity().getName(), probl.getToCity().getName()));
            }

            return problems;

        } catch (Exception e) {
            session.getTransaction().rollback();
            LOG.error("Error during transaction", e);
            throw new RuntimeException(e);

        }

    }

    public void fillTableResult(List<TownsCost> results) {

        try {
            for (TownsCost result : results) {

                Query<Problem> loadProblems = session.createQuery("from Problem", Problem.class);

                if (result.getCost() == Integer.MAX_VALUE) {
                    ImpossibleRout impossibleRout = new ImpossibleRout(getProblemResult(loadProblems.list(), result.getTownTo(), result.getTownFrom()));
                    session.saveOrUpdate(impossibleRout);

                } else {
                    FoundRout foundRout = new FoundRout(getProblemResult(loadProblems.list(), result.getTownTo(),
                            result.getTownFrom()), result.getCost());
                    session.saveOrUpdate(foundRout);
                }
            }
            session.getTransaction().commit();
        } catch (Exception e) {
            LOG.error("Error during transaction", e);
            throw new RuntimeException(e);
        }

    }

    private Problem getProblemResult(List<Problem> problems, String to, String from) {
        for (Problem problem : problems) {
            if ((problem.getFromCity().getName().equals(from) && problem.getToCity().getName().equals(to))) {
                return problem;
            }

        }
        return new Problem();

    }

    @Override
    public void close() throws Exception {
        session.close();
        sessionFactory.close();

    }
}
