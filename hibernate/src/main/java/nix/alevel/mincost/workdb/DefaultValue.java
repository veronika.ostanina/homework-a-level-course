package nix.alevel.mincost.workdb;


import nix.alevel.mincost.pathfinder.TownsCost;

import java.util.ArrayList;
import java.util.List;

public class DefaultValue {
    private List<String> names = new ArrayList<>(List.of("gdansk", "bydgoszcz", "vozn", "torun", "warszawa", "kiev"));
    private List<TownsCost> neighborsCost = new ArrayList<>(List.of(
            new TownsCost("gdansk", "bydgoszcz", 1),
            new TownsCost("gdansk", "torun", 3),
            new TownsCost("bydgoszcz", "gdansk", 1),
            new TownsCost("bydgoszcz", "torun", 1),
            new TownsCost("bydgoszcz", "warszawa", 4),
            new TownsCost("torun", "gdansk", 3),
            new TownsCost("torun", "bydgoszcz", 1),
            new TownsCost("torun", "warszawa", 1),
            new TownsCost("kiev", "vozn", 1),
            new TownsCost("vozn", "kiev", 1),
            new TownsCost("warszawa", "bydgoszcz", 4),
            new TownsCost("warszawa", "torun", 1)));
    private List<TownsCost> calculateCost = new ArrayList<>(List.of(
            new TownsCost("gdansk", "warszawa"),
            new TownsCost("kiev", "warszawa"),
            new TownsCost("bydgoszcz", "warszawa")));

    public DefaultValue() {
    }

    public DefaultValue(List<String> names, List<TownsCost> neighborsCost, List<TownsCost> calculateCost) {
        this.names = names;
        this.neighborsCost = neighborsCost;
        this.calculateCost = calculateCost;
    }

    public List<String> getNames() {
        return names;
    }

    public void setNames(List<String> names) {
        this.names = names;
    }

    public List<TownsCost> getNeighborsCost() {
        return neighborsCost;
    }

    public void setNeighborsCost(List<TownsCost> neighborsCost) {
        this.neighborsCost = neighborsCost;
    }

    public List<TownsCost> getCalculateCost() {
        return calculateCost;
    }

    public void setCalculateCost(List<TownsCost> calculateCost) {
        this.calculateCost = calculateCost;
    }
}
