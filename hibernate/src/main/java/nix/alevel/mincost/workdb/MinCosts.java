package nix.alevel.mincost.workdb;


import nix.alevel.mincost.pathfinder.ProfitableWays;
import nix.alevel.mincost.pathfinder.TownsCost;

import java.util.List;

public class MinCosts {

    private ProfitableWays profitableWaysDB;

    public void createFillDB(List<String> names, List<TownsCost> neighborsCost, List<TownsCost> townsFindCost) {

        try (DataDB dataDB = new DataDB()) {
            dataDB.fillDB(names, neighborsCost, townsFindCost);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void createFillDB() {
        try (DataDB dataDB = new DataDB()) {
            dataDB.fillDB();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void loadData() {
        try (DataDB dataDB = new DataDB()) {
            int townSize = dataDB.getCountCity();
            if (townSize == 0) {
                throw new RuntimeException("Table is not exist or empty");
            }
            profitableWaysDB = new ProfitableWays();
            profitableWaysDB.setTownsSize(townSize)
                    .fullTowns(dataDB.fillSelectedCityNeighbors())
                    .setTownCalculateMinCostSize(dataDB.getCountProblems())
                    .fullTownCalculateMinCost(dataDB.getProblems())
                    .build();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }

    public boolean calculateSolution() {
        if (profitableWaysDB == null) {
            return false;
        }
        profitableWaysDB.calculateSolution();
        return true;
    }

    public void writeFileResult() {
        try (DataDB dataDB = new DataDB()) {
            dataDB.fillTableResult(profitableWaysDB.getTownCalculateMinCost());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
