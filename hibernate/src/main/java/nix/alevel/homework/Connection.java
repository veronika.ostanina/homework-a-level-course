package nix.alevel.homework;

import javax.persistence.*;

@Entity
@Table(name = "connection")
public class Connection {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(nullable = false)
    private Integer cost;

    @ManyToOne()
    @JoinColumn(name = "from_city")
    private City fromCity;

    @ManyToOne()
    @JoinColumn(name = "to_city")
    private City toCity;

    public Connection() {
    }

    public Connection(int cost, City fromCity, City toCity) {
        this.cost = cost;
        this.fromCity = fromCity;
        fromCity.addFromCity(this);
        this.toCity = toCity;
        toCity.addToCity(this);

    }

    public Integer getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Integer getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public City getFromCity() {
        return fromCity;
    }

    public void setFromCity(City fromCity) {
        this.fromCity = fromCity;
    }

    public City getToCity() {
        return toCity;
    }

    public void setToCity(City toCity) {
        this.toCity = toCity;
    }
}
