package nix.alevel.homework;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "city")
public class City {

    @OneToMany(mappedBy = "fromCity", cascade = CascadeType.ALL, orphanRemoval = true)
    private final List<Connection> connectionFrom = new ArrayList<>();
    @OneToMany(mappedBy = "toCity", cascade = CascadeType.ALL, orphanRemoval = true)
    private final List<Connection> connectionTo = new ArrayList<>();
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idCity;
    @Column(unique = true, nullable = false)
    private String name;

    public City() {
    }

    public City(String name) {
        this.name = name;
    }

    public Integer getIdCity() {
        return idCity;
    }

    public void setIdCity(Integer idСity) {
        this.idCity = idСity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void addFromCity(Connection connection) {
        connectionFrom.add(connection);
    }

    public void addToCity(Connection connection) {
        connectionTo.add(connection);
    }

    public List<Connection> getConnectionFrom() {
        return connectionFrom;
    }

    public List<Connection> getConnectionTo() {
        return connectionTo;
    }
}
