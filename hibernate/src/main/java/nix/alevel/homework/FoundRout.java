package nix.alevel.homework;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "found_rout")
public class FoundRout implements Serializable {
    @Id
    @OneToOne
    @JoinColumn(name = "id_problem")
    @MapsId
    private Problem idProblem;
    @Column(nullable = false)
    private Integer cost;

    public FoundRout() {
    }

    public FoundRout(Problem idProblem, Integer cost) {
        this.idProblem = idProblem;
        idProblem.setFoundRout(this);
        this.cost = cost;
    }

    public Integer getCost() {
        return cost;
    }

    public void setCost(Integer cost) {
        this.cost = cost;
    }

    public Problem getIdProblem() {
        return idProblem;
    }

    public void setIdProblem(Problem idProblem) {
        this.idProblem = idProblem;
    }

}
