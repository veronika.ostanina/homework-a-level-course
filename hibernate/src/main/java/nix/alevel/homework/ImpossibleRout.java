package nix.alevel.homework;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "impossible_rout")
public class ImpossibleRout implements Serializable {
    @Id
    @OneToOne
    @JoinColumn(name = "id_problem")
    @MapsId
    private Problem idProblem;

    public ImpossibleRout(Problem idProblem) {
        this.idProblem = idProblem;
        idProblem.setImpossibleRout(this);
    }

    public ImpossibleRout() {
    }

    public Problem getIdProblem() {
        return idProblem;
    }

    public void setIdProblem(Problem idProblem) {
        this.idProblem = idProblem;
    }
}

