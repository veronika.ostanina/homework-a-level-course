package nix.alevel.homework;

import javax.persistence.*;

@Entity
@Table(name = "problem")
public class Problem {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idProblem;

    @ManyToOne
    @JoinColumn(name = "from_city")
    private City fromCity;

    @ManyToOne
    @JoinColumn(name = "to_city")
    private City toCity;

    @OneToOne(mappedBy = "idProblem", cascade = CascadeType.ALL, orphanRemoval = true)
    private FoundRout foundRout;

    @OneToOne(mappedBy = "idProblem", cascade = CascadeType.ALL, orphanRemoval = true)
    private ImpossibleRout impossibleRout;


    public Problem() {
    }

    public Problem(City fromCity, City toCity) {
        this.fromCity = fromCity;
        this.toCity = toCity;
    }

    public FoundRout getFoundRout() {
        return foundRout;
    }

    public void setFoundRout(FoundRout foundRout) {
        this.foundRout = foundRout;
    }

    public ImpossibleRout getImpossibleRout() {
        return impossibleRout;
    }

    public void setImpossibleRout(ImpossibleRout impossibleRout) {
        this.impossibleRout = impossibleRout;
    }

    public Integer getIdProblem() {
        return idProblem;
    }

    public void setIdProblem(Integer idProblem) {
        this.idProblem = idProblem;
    }

    public City getFromCity() {
        return fromCity;
    }

    public void setFromCity(City fromCity) {
        this.fromCity = fromCity;
    }

    public City getToCity() {
        return toCity;
    }

    public void setToCity(City toCity) {
        this.toCity = toCity;
    }
}
