package nix.alevel.itl;

import nix.alevel.homework.City;
import nix.alevel.homework.Connection;
import nix.alevel.homework.Problem;
import nix.alevel.mincost.workdb.DataDB;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

public class UploadDataFileDB implements AutoCloseable {


    private static final Logger LOG = LoggerFactory.getLogger(DataDB.class);
    private Configuration configuration = new Configuration().configure();
    private SessionFactory sessionFactory;
    private Session session;

    public UploadDataFileDB() {
        sessionFactory = configuration.buildSessionFactory();
        session = sessionFactory.openSession();
    }

    public void uploadCity(String name) {
        session.beginTransaction();
        City newCityDB = new City(name);
        session.saveOrUpdate(newCityDB);
        session.getTransaction().commit();
    }

    public void uploadConnection(String from, String to, int cost) {
        session.beginTransaction();

        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
        CriteriaQuery<City> cityFrom = criteriaBuilder.createQuery(City.class);
        Root<City> root = cityFrom.from(City.class);

        cityFrom.select(root).where(criteriaBuilder.like(root.get("name"), from));
        Query<City> query = session.createQuery(cityFrom);
        City fromCity = query.getSingleResult();

        cityFrom.select(root).where(criteriaBuilder.like(root.get("name"), to));
        Query<City> query2 = session.createQuery(cityFrom);
        City toCity = query2.getSingleResult();

        Connection connection = new Connection(cost, fromCity, toCity);
        session.saveOrUpdate(connection);
        session.getTransaction().commit();

    }

    public void uploadProblem(String firstCity, String secondCity) {
        session.beginTransaction();
        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
        CriteriaQuery<City> city = criteriaBuilder.createQuery(City.class);
        Root<City> root = city.from(City.class);

        city.select(root.get("name")).distinct(true);
        Query<City> query = session.createQuery(city);
        City from = query.getSingleResult();

        city.select(root).where(criteriaBuilder.like(root.get("name"), secondCity));
        Query<City> query2 = session.createQuery(city);
        City to = query2.getSingleResult();

        Problem problem = new Problem(from, to);
        session.saveOrUpdate(problem);
        session.getTransaction().commit();

    }


    @Override
    public void close() throws Exception {
        session.close();
        sessionFactory.close();
    }
}
