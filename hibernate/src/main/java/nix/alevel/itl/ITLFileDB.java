package nix.alevel.itl;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class ITLFileDB {
    private int quantityCities;
    private int quantityProblems;
    private Map<Integer, TownSigns> townSigns;

    public void loadDataFileBD(String path) {
        File file = new File(path);
        if (file.isDirectory() || !file.exists()) {
            throw new RuntimeException("File is directory or not exist!");
        }
        try (InputStream in = new FileInputStream(file);
             BufferedReader bufferedReader =
                     new BufferedReader(new InputStreamReader(in, StandardCharsets.UTF_8))) {
            bufferedReader.readLine();
            quantityCities = Integer.parseInt(bufferedReader.readLine());
            townSigns = new LinkedHashMap<>(quantityCities);
            fillCity(bufferedReader);
            fillConnection();
            quantityProblems = Integer.parseInt(bufferedReader.readLine());
            fillProblem(bufferedReader);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    private void fillCity(BufferedReader bufferedReader) {
        int quantityNeighbors;
        String name;

        for (int i = 1; i <= quantityCities; i++) {
            try (UploadDataFileDB UploadDataFileDB = new UploadDataFileDB()) {
                Map<Integer, Integer> neighborsCost = new HashMap<>();
                name = bufferedReader.readLine().trim();
                quantityNeighbors = Integer.parseInt(bufferedReader.readLine());
                for (int j = 0; j < quantityNeighbors; j++) {
                    String[] citiesCost = bufferedReader.readLine().split(" ");
                    neighborsCost.put(Integer.parseInt(citiesCost[0]), Integer.parseInt(citiesCost[1]));
                }
                UploadDataFileDB.uploadCity(name);
                townSigns.put(i, new TownSigns(name, neighborsCost));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void fillConnection() {
        try (UploadDataFileDB UploadDataFileDB = new UploadDataFileDB()) {
            for (Map.Entry<Integer, TownSigns> entry : townSigns.entrySet()) {
                Integer numbCity = entry.getKey();
                TownSigns sign = entry.getValue();
                for (Map.Entry<Integer, Integer> e : sign.getNeighbors().entrySet()) {
                    Integer town = e.getKey();
                    Integer cost = e.getValue();
                    UploadDataFileDB.uploadConnection(townSigns.get(numbCity).getCity(), townSigns.get(town).getCity(), cost);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void fillProblem(BufferedReader bufferedReader) {
        for (int i = 0; i < quantityProblems; i++) {
            try (UploadDataFileDB UploadDataFileDB = new UploadDataFileDB()) {
                var pathFind = bufferedReader.readLine().split(" ");
                UploadDataFileDB.uploadProblem(pathFind[0], pathFind[1]);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


}
