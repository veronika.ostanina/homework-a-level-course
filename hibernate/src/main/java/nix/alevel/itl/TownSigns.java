package nix.alevel.itl;

import java.util.Map;

public class TownSigns {
    private String city;
    private Map<Integer, Integer> neighbors;

    public TownSigns(String city, Map<Integer, Integer> neighbors) {

        this.city = city;
        this.neighbors = neighbors;
    }


    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Map<Integer, Integer> getNeighbors() {
        return neighbors;
    }

    public void setNeighbors(Map<Integer, Integer> neighbors) {
        this.neighbors = neighbors;
    }
}
