package nix.alevel.mincost.workdb;

import nix.alevel.homework.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class DataDBTest {


    @Test
    void fillDB() {
        Configuration configuration = new Configuration().configure();
        try (SessionFactory sessionFactory = configuration.buildSessionFactory();
             Session session = sessionFactory.openSession();
             DataDB dataDB = new DataDB()) {
            dataDB.fillDB();
            Query<City> cities = session.createQuery("from City", City.class);
            assertEquals(6, cities.list().size());
            Query<Connection> connection = session.createQuery("from Connection ", Connection.class);
            assertEquals(12, connection.list().size());
            Query<Problem> problems = session.createQuery("from Problem ", Problem.class);
            assertEquals(3, problems.list().size());

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }


    @Test
    void fillTableResult() {
        Configuration configuration = new Configuration().configure();
        try (SessionFactory sessionFactory = configuration.buildSessionFactory();
             Session session = sessionFactory.openSession();
        ) {
            MinCosts minCosts = new MinCosts();
            minCosts.loadData();
            minCosts.calculateSolution();
            minCosts.writeFileResult();
            Query<FoundRout> foundRouts = session.createQuery("from FoundRout ", FoundRout.class);
            assertEquals(2, foundRouts.list().size());
            Query<ImpossibleRout> impossibleRouts = session.createQuery("from ImpossibleRout ", ImpossibleRout.class);
            assertEquals(1, impossibleRouts.list().size());

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}