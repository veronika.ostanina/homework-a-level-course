package nix.alevel.itl;

import nix.alevel.homework.City;
import nix.alevel.homework.Connection;
import nix.alevel.homework.Problem;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;
import org.junit.jupiter.api.Test;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ITLFileDBTest {
    ITLFileDB testClass = new ITLFileDB();
    ;

    @Test
    void loadDataFileBD() {
        Configuration configuration = new Configuration().configure();
        try (SessionFactory sessionFactory = configuration.buildSessionFactory();
             Session session = sessionFactory.openSession();) {
            List<String> cities = new ArrayList<>(List.of("gdansk", "bydgoszcz", "torun", "warszawa"));
            checkBD("C:/Users/Nica_/IdeaProjects/homework-a-level-course/inputOutputFilesTask3/input.txt", session, 4, cities, 10, 2);
            cities.clear();
            cities = List.of("Harkiv", "Dnipro", "Poltava", "Odesa", "Voznesensk");
            checkBD("C:/Users/Nica_/IdeaProjects/homework-a-level-course/inputOutputFilesTask3/input2.txt", session, 5, cities, 8, 2);
        }


    }

    private void checkBD(String path, Session session, int quantityCities, List<String> names, int sizeConnection, int sizeProblem) {
        deleteDB(session);
        testClass.loadDataFileBD(path);
        Query<City> cities = session.createQuery("from City", City.class);
        int size = cities.getResultList().size();
        assertEquals(quantityCities, size);
        List<City> citiesTest = cities.getResultList();
        for (int i = 0; i < size; i++) {
            assertEquals(names.get(i), citiesTest.get(i).getName());
        }
        Query<Connection> connection = session.createQuery("from Connection ", Connection.class);
        int conect = connection.getResultList().size();
        assertEquals(sizeConnection, conect);

        Query<Problem> problem = session.createQuery("from Problem ", Problem.class);
        int problems = problem.getResultList().size();
        assertEquals(sizeProblem, problems);

    }

    private void deleteDB(Session session) {
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaDelete<City> city = cb.createCriteriaDelete(City.class);
        city.from(City.class);

        CriteriaDelete<Connection> connection = cb.createCriteriaDelete(Connection.class);
        connection.from(Connection.class);

        CriteriaDelete<Problem> problem = cb.createCriteriaDelete(Problem.class);
        problem.from(Problem.class);


        Transaction transaction = session.beginTransaction();

        session.createQuery(connection).executeUpdate();
        session.createQuery(problem).executeUpdate();
        session.createQuery(city).executeUpdate();

        transaction.commit();

    }


}