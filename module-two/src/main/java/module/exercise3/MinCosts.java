package module.exercise3;

import java.io.*;
import java.nio.charset.StandardCharsets;

public class MinCosts {

    private ProfitableWays profitableWaysOneFile;

    public void loadData(String path) {
        File file = new File(path);
        if (file.isDirectory() || !file.exists()) {
            throw new RuntimeException("File is directory or not exist!");
        }
        try (InputStream in = new FileInputStream(file);
             BufferedReader bufferedReader =
                     new BufferedReader(new InputStreamReader(in, StandardCharsets.UTF_8))) {
            bufferedReader.readLine();
            profitableWaysOneFile = new ProfitableWays();
            profitableWaysOneFile.setTownsSize(Integer.parseInt(bufferedReader.readLine()))
                    .fullTowns(bufferedReader)
                    .setTownCalculateMinCostSize(Integer.parseInt(bufferedReader.readLine()))
                    .fullTownCalculateMinCost(bufferedReader)
                    .build();

        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    public boolean calculateSolution() {
        if (profitableWaysOneFile == null) {
            return false;
        }
        profitableWaysOneFile.calculateSolution();
        return true;
    }

    public void writeFileResult(String path) {
        try (FileWriter writer = new FileWriter(path, false)) {
            writer.write("Вывод:" + System.lineSeparator());
            for (ResultCost i : profitableWaysOneFile.getTownCalculateMinCost()) {
                writer.write(i.getFinalCost() + System.lineSeparator());
            }
        } catch (IOException e) {
            throw new UncheckedIOException(e);

        }
    }

}
