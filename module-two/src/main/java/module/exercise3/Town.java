package module.exercise3;


import java.io.BufferedReader;
import java.io.IOException;
import java.util.LinkedHashMap;

public class Town {
    private LinkedHashMap<Integer, Integer> neighbors = new LinkedHashMap<>();
    private int countNeighbors;
    private String nameTown;
    private int numberTown;

    public Town(String nameTown, int countNeighbors, int number) {
        if (countNeighbors == 0 || number == 0) {
            throw new IllegalArgumentException("Number town or count of neighbors is 0");
        }
        this.nameTown = nameTown;
        this.countNeighbors = countNeighbors;
        this.numberTown = number;
    }

    public void setTownNeighbors(BufferedReader bufferedReader) throws IOException {
        int i = 1;
        while (i <= countNeighbors) {
            String[] temp = bufferedReader.readLine().split(" ");
            neighbors.put(Integer.parseInt(temp[0]), Integer.parseInt(temp[1]));
            i++;
        }
    }

    public LinkedHashMap<Integer, Integer> getNeighbors() {
        return neighbors;
    }

    public String getNameTown() {
        return nameTown;
    }

    public int getNumberTown() {
        return numberTown;
    }
}
