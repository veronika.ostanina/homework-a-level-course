package module.exercise3;

public class ResultCost {

    private String townFrom;
    private String townTo;
    private int finalCost;

    public ResultCost(String townFrom, String townTo) {
        if (townFrom.equals(townTo)) {
            throw new IllegalArgumentException("Towns are the same");
        }
        this.townFrom = townFrom;
        this.townTo = townTo;
    }

    public String getTownFrom() {
        return townFrom;
    }

    public String getTownTo() {
        return townTo;
    }

    public int getFinalCost() {
        return finalCost;
    }

    public void setFinalCost(int finalCost) {
        this.finalCost = finalCost;
    }
}