package module.exercise3;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ProfitableWays {
    private final List<Town> towns = new ArrayList<>();
    private final List<ResultCost> townCalculateMinCost = new ArrayList<>();
    private int townCalculateMinCostSize;
    private int townsSize;

    public ProfitableWays setTownCalculateMinCostSize(int townCalculateMinCostSize) {
        if (townCalculateMinCostSize > 100) {
            throw new IllegalArgumentException("Calculated min cost quantity  is more 100");
        }
        this.townCalculateMinCostSize = townCalculateMinCostSize;
        return this;
    }

    public ProfitableWays setTownsSize(int townsSize) {
        if (townsSize > 10_000) {
            throw new IllegalArgumentException(" Town`s quantity  is more 10 000");
        }
        this.townsSize = townsSize;
        return this;
    }

    public ProfitableWays fullTownCalculateMinCost(BufferedReader bufferedReader) throws IOException {
        for (int i = 0; i < townCalculateMinCostSize; i++) {
            String[] temp = bufferedReader.readLine().split(" ");
            townCalculateMinCost.add(new ResultCost(temp[0], temp[1]));
        }
        return this;
    }

    public ProfitableWays fullTowns(BufferedReader bufferedReader) throws IOException {

        for (int j = 0; j < townsSize; j++) {
            Town town = new Town(bufferedReader.readLine(), Integer.parseInt(bufferedReader.readLine()), j + 1);
            town.setTownNeighbors(bufferedReader);
            towns.add(town);
        }

        return this;
    }

    public void build() {
        if (towns.isEmpty() || townCalculateMinCost.isEmpty()) {
            throw new IllegalArgumentException("There are town`s quantity is 0 or ways to calculate is 0");
        }
    }

    public void calculateSolution() {
        for (ResultCost resultCost : townCalculateMinCost) {
            CalculateMinCost calculateMinCost = new CalculateMinCost();
            resultCost.setFinalCost(calculateMinCost.findMinCost(resultCost.getTownFrom(), resultCost.getTownTo(), towns));
        }

    }

    public List<ResultCost> getTownCalculateMinCost() {
        return townCalculateMinCost;
    }
}




