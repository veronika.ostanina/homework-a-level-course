package module.exercise2;

import java.util.*;

public class FindFirstUniqueName {

    public static Optional<String> firstUniqueName(List<String> names) {
        if (names.isEmpty()) {
            return Optional.empty();
        }
        if (names.size() == 1) {
            return Optional.ofNullable(names.get(0));
        }
        Set<String> checkedNames = new HashSet<>();
        Set<String> uniqueNames = new LinkedHashSet<>();
        for (String name : names) {
            if (checkedNames.add(name)) {
                uniqueNames.add(name);
            } else {
                uniqueNames.remove(name);
            }
        }
        return uniqueNames.stream().findFirst();
    }
}
