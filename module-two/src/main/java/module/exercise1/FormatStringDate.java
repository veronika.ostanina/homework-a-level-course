package module.exercise1;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class FormatStringDate {
    private final DateTimeFormatter[] PATTERN_FROM = {
            DateTimeFormatter.ofPattern("MM-dd-yyyy"),
            DateTimeFormatter.ofPattern("dd/MM/yyyy"),
            DateTimeFormatter.ofPattern("yyyy/MM/dd")
    };
    private final DateTimeFormatter PATTERN_TO = DateTimeFormatter.ofPattern("yyyyMMdd");


    public Optional<List<String>> convertDateToString(List<String> listDate) {
        if (listDate.isEmpty()) {
            return Optional.empty();
        }
        List<String> arrayDate = new ArrayList<>(listDate.size());
        for (String s : listDate) {
            LocalDate parsed;
            try {
                switch (s.charAt(2)) {
                    case '-':
                        parsed = LocalDate.parse(s, PATTERN_FROM[0]);
                        break;
                    case '/':
                        parsed = LocalDate.parse(s, PATTERN_FROM[1]);
                        break;
                    default:
                        parsed = LocalDate.parse(s, PATTERN_FROM[2]);
                }
                arrayDate.add(parsed.format(PATTERN_TO));
            } catch (DateTimeParseException e) {
            }

        }
        return Optional.ofNullable(arrayDate);
    }
}
