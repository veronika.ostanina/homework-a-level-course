package module.exercise3;

import org.junit.jupiter.api.Test;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class MinCostsTest {
    MinCosts minCosts = new MinCosts();

    @Test
    void loadData() {
        assertDoesNotThrow(() -> minCosts.loadData("D:/aleveljavanix/inputOutputFilesTask3/input.txt"));
        assertThrows(IllegalArgumentException.class, () -> minCosts.loadData("D:/aleveljavanix/inputOutputFilesTask3/input3.txt"));
        assertThrows(IllegalArgumentException.class, () -> minCosts.loadData("D:/aleveljavanix/inputOutputFilesTask3/input4.txt"));

    }

    @Test
    void calculateSolution() {
        assertFalse(minCosts.calculateSolution());

        minCosts.loadData("D:/aleveljavanix/inputOutputFilesTask3/input5.txt");
        assertThrows(IllegalArgumentException.class, () -> minCosts.calculateSolution());


        minCosts.loadData("D:/aleveljavanix/inputOutputFilesTask3/input2.txt");
        minCosts.calculateSolution();
        minCosts.writeFileResult("D:/aleveljavanix/inputOutputFilesTask3/output1.txt");
        List<String> fileValue = new ArrayList<>();
        fileValue.add("110");
        fileValue.add("70");
        assertTrue(checkResultFile("D:/aleveljavanix/inputOutputFilesTask3/output1.txt", fileValue));


        minCosts.loadData("D:/aleveljavanix/inputOutputFilesTask3/input7.txt");
        minCosts.calculateSolution();
        minCosts.writeFileResult("D:/aleveljavanix/inputOutputFilesTask3/output2.txt");
        fileValue.clear();
        fileValue.add("20");
        assertTrue(checkResultFile("D:/aleveljavanix/inputOutputFilesTask3/output2.txt", fileValue));

        minCosts.loadData("D:/aleveljavanix/inputOutputFilesTask3/input8.txt");
        minCosts.calculateSolution();
        minCosts.writeFileResult("D:/aleveljavanix/inputOutputFilesTask3/output3.txt");
        fileValue.clear();
        fileValue.add("110");
        fileValue.add("60");
        fileValue.add("50");
        fileValue.add("70");
        fileValue.add("40");
        fileValue.add("30");
        fileValue.add("20");
        fileValue.add("60");
        fileValue.add("10");
        fileValue.add("70");
        assertTrue(checkResultFile("D:/aleveljavanix/inputOutputFilesTask3/output3.txt", fileValue));

        minCosts.loadData("D:/aleveljavanix/inputOutputFilesTask3/input.txt");
        minCosts.calculateSolution();
        minCosts.writeFileResult("D:/aleveljavanix/inputOutputFilesTask3/output4.txt");
        fileValue.clear();
        fileValue.add("3");
        fileValue.add("2");
        assertTrue(checkResultFile("D:/aleveljavanix/inputOutputFilesTask3/output4.txt", fileValue));
    }

    private boolean checkResultFile(String path, List<String> checkList) {
        File file = new File(path);
        List<String> fileValue = new ArrayList<>();
        try (InputStream in = new FileInputStream(file);
             BufferedReader bufferedReader =
                     new BufferedReader(new InputStreamReader(in, StandardCharsets.UTF_8))) {
            bufferedReader.readLine();
            bufferedReader.lines().
                    forEach(fileValue::add);

        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
        return fileValue.equals(checkList);
    }
}