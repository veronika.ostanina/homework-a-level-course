package module.exercise3;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

class ResultCostTest {

    @Test
    void resultCostTest() {
        assertThrows(RuntimeException.class, () -> new ResultCost("Town", "Town"));
        assertDoesNotThrow(() -> new ResultCost("Town1", "Town2"));

    }

}