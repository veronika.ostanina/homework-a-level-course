package module.exercise1;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

class FormatStringDateTest {

    @Test
    void convertDateToString() {
        List<String> list = new ArrayList<>();
        FormatStringDate formatStringDate = new FormatStringDate();
        assertEquals(Optional.empty(), formatStringDate.convertDateToString(list));

        list.add("04-05-20420");
        assertEquals("[]", formatStringDate.convertDateToString(list).get().toString());

        list.clear();
        list.add("2020/04/05");
        list.add("05/04/2020");
        list.add("04-05-2020");
        assertEquals("[20200405, 20200405, 20200405]", formatStringDate.convertDateToString(list).get().toString());

        list.clear();
        list.add("02/03/2020");
        list.add("02-03-2020");
        list.add("2020/03/20");
        list.add("20209/03/20");
        list.add("aaaa/03/20");
        list.add("20200328");
        assertEquals("[20200302, 20200203, 20200320]", formatStringDate.convertDateToString(list).get().toString());
    }
}