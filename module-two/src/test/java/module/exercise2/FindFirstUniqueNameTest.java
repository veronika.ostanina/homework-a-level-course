package module.exercise2;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class FindFirstUniqueNameTest {

    @Test
    void findFirstUniqueName() {
        List<String> strings = new ArrayList<>();

        assertEquals(Optional.empty(), FindFirstUniqueName.firstUniqueName(strings));

        strings.add("Peter");
        strings.add("Peter");
        strings.add("Josh");
        strings.add("Josh");
        assertTrue(FindFirstUniqueName.firstUniqueName(strings).isEmpty());

        strings.clear();
        strings.add("Peter");
        assertEquals("Peter", FindFirstUniqueName.firstUniqueName(strings).get());

        strings.clear();
        strings.add("Peter");
        strings.add("Peter");
        strings.add("Josh");
        strings.add("Peter");
        strings.add("Tom");
        strings.add("Peter");
        strings.add("Josh");
        strings.add("Pol");
        strings.add("Sam");
        strings.add("Pol");
        assertEquals("Tom", FindFirstUniqueName.firstUniqueName(strings).get());
    }
}