package nix.alevel.homework.controller;


import nix.alevel.homework.entity.SaveToDoList;
import nix.alevel.homework.entity.ToDoList;
import nix.alevel.homework.service.ToDoListService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/todolist")
public class ToDoListController {

    private final ToDoListService toDoListService;

    public ToDoListController(ToDoListService toDoListService) {
        this.toDoListService = toDoListService;
    }

    @GetMapping("/tasks")
    public List<ToDoList> getList() {
        return toDoListService.getToDoList();
    }

    @GetMapping("/{id}")
    public ToDoList getTask(@PathVariable Long id) {
        return toDoListService.getById(id).get();
    }

    @GetMapping("/notdonetasks")
    public List<ToDoList> getNotDoneTasks() {
        return toDoListService.getNotDoneTasks();
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping
    public ToDoList createTask(@RequestBody SaveToDoList task) {

        return toDoListService.createTask(task).get();
    }

    @PutMapping("/{id}")
    public ToDoList update(@PathVariable Long id, @RequestBody SaveToDoList task) {
        return toDoListService.updateTask(id, task).get();
    }

    @DeleteMapping("/{tasks}")
    public void deleteTasks(@PathVariable List<Long> tasks) {
        toDoListService.deleteTasks(tasks);
    }

}
