package nix.alevel.homework.repository;

import nix.alevel.homework.entity.SaveToDoList;
import nix.alevel.homework.entity.ToDoList;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface ToDoListRepository extends JpaRepository<ToDoList, Long> {
    List<ToDoList> getToDoListByDoneIsFalse ();

    void deleteAllByIdIn(List<Long> id);
}
