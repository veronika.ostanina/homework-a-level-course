package nix.alevel.homework.service;

import nix.alevel.homework.entity.SaveToDoList;
import nix.alevel.homework.entity.ToDoList;
import nix.alevel.homework.repository.ToDoListRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class ToDoListService {
    private final ToDoListRepository toDoListRepository;


    public ToDoListService(ToDoListRepository toDoListRepository) {
        this.toDoListRepository = toDoListRepository;
    }

    public List<ToDoList>  getToDoList (){
     return toDoListRepository.findAll();
    }

    public Optional<ToDoList> getById (Long id){
        return toDoListRepository.findById(id);
    }

    public List<ToDoList> getNotDoneTasks (){
        return toDoListRepository.getToDoListByDoneIsFalse();
    }
    public Optional<ToDoList> createTask (SaveToDoList saveToDoList){
        ToDoList toDoList = new ToDoList(saveToDoList.getText(), saveToDoList.isDone());
        return Optional.of(toDoListRepository.save(toDoList));
    }
    public  Optional<ToDoList>  updateTask (Long id, SaveToDoList saveToDoList){
        ToDoList toDoList = toDoListRepository.findById(id).orElseThrow();
        toDoList.setText(saveToDoList.getText());
        toDoList.setDone(true);
        return Optional.of(toDoListRepository.save(toDoList));
    }
    public void deleteTasks (List<Long> tasks){
            toDoListRepository.deleteAllByIdIn(tasks);

    }


}
