package nix.alevel.homework.entity;
import javax.persistence.*;

@Entity
@Table(name = "todolists")
public class ToDoList {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(nullable = false)
    private String text;

    @Column(nullable = false)
    private boolean done;

    public ToDoList() {
    }

    public ToDoList( String text, boolean done) {
        this.text = text;
        this.done = done;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    @Override
    public String toString() {
        return String.valueOf(id) + String.valueOf(text)+String.valueOf(done);
    }
}
