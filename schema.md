# Path finder data

### City

1. id (PK_
2. name (NOT NULL, unique)

### Connection

1. cost
2. from (fk to city_id)
3. to   (fk to city_id)

PK - (1, 2, 3)

### Problems

1. id (PK)
2. from (fk to city_id)
3. to   (fk to city_id)

### Found Routes

1. problem_id (PK, FK TO problem_id) - 1-1 relationship
2. min_cost (NOT NULL) : int

### Impossible Routes

1. problem_id (PK, FK TO problem_id) - 1-1 relationship