package nix.alevel.website;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@WebServlet(name = "user-info", urlPatterns = "/")
public class ShowInfoUserServlet extends HttpServlet {
    public static final Logger log = LoggerFactory.getLogger(ShowInfoUserServlet.class);
    private final Map<String, String> users = new ConcurrentHashMap<>();

    @Override
    public void init() {
        log.info("Start");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setContentType("text/html");
        PrintWriter response = resp.getWriter();
        response.print("<h1 align=\"center\">Users</h1>");
        users.putIfAbsent(req.getRemoteHost(), req.getHeader("User-Agent"));
        users.forEach((ipUser, infoUser) -> {
            if (ipUser.equals(req.getRemoteHost()))
                response.println("<p><b>User: " + ipUser + " User-Agent: " + infoUser + "</b></p>");
            else
                response.println("<p>User: " + ipUser + " User-Agent" + infoUser + "</p>");
        });


    }

    @Override
    public void destroy() {
        log.info("Exit");
    }
}
