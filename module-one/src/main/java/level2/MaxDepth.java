package level2;

public class MaxDepth {

    private int maxDepthRecursion(TreeNode treeNode, int quantity) {
        if (treeNode.left == null && treeNode.right == null) {
            return quantity;
        }
        if (treeNode.right == null) {
            return maxDepthRecursion(treeNode.left, quantity + 1);
        }
        if (treeNode.left == null) {
            return maxDepthRecursion(treeNode.right, quantity + 1);
        }
        return Math.max(maxDepthRecursion(treeNode.right, quantity + 1), maxDepthRecursion(treeNode.left, quantity + 1));

    }

    public int maxDepth(TreeNode treeNode) {
        if (treeNode == null) {
            return 0;
        }
        int quantity;
        quantity = maxDepthRecursion(treeNode, 1);
        return quantity;
    }

}
