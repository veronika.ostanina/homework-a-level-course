package level2;

import java.util.ArrayDeque;

public class StringSymbol {
    private String stringSymbol;

    public StringSymbol(String stringSymbol) {
        this.stringSymbol = stringSymbol;
    }

    public void setStringSymbol(String stringSymbol) {
        this.stringSymbol = stringSymbol;
    }

    public boolean checkStringSymbol() {
        if (stringSymbol.isEmpty()) {
            return true;
        }
        if (stringSymbol.length() % 2 != 0) {
            return false;
        }
        ArrayDeque<Character> tempOpenScopes = new ArrayDeque();
        for (int i = 0, len = stringSymbol.length(); i < len; i++) {
            char tempSymbol = stringSymbol.charAt(i);
            if (tempSymbol == '[' || tempSymbol == '{' || tempSymbol == '(') {
                tempOpenScopes.add(tempSymbol);
            } else {
                if (tempSymbol == ']' && tempOpenScopes.getLast() == '[') {
                    tempOpenScopes.removeLast();
                }
                if (tempSymbol == '}' && tempOpenScopes.getLast() == '{') {
                    tempOpenScopes.removeLast();
                }
                if (tempSymbol == ')' && tempOpenScopes.getLast() == '(') {
                    tempOpenScopes.removeLast();
                }
            }

        }
        if (tempOpenScopes.isEmpty()) {
            return true;
        }
        return false;
    }
}
