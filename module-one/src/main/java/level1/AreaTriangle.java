package level1;

public class AreaTriangle {
    private final int[][] matrixArea = new int[2][2];


    public AreaTriangle(int a, int a1, int b, int b1, int c, int c1) {
        matrixArea[0][0] = a - c;
        matrixArea[0][1] = a1 - c1;
        matrixArea[1][0] = b - c;
        matrixArea[1][1] = b1 - c1;
    }

    public void setAreaTriangle(int a, int a1, int b, int b1, int c, int c1) {
        matrixArea[0][0] = a - c;
        matrixArea[0][1] = a1 - c1;
        matrixArea[1][0] = b - c;
        matrixArea[1][1] = b1 - c1;
    }

    public double findAreaTriangle() {
        double areaTriangle = ((matrixArea[0][0] * matrixArea[1][1]) - (matrixArea[0][1] * matrixArea[1][0])) / 2.0;
        if (areaTriangle < 0) {
            return -areaTriangle;
        }
        return areaTriangle;
    }

}
