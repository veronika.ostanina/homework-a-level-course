package level1;

public class HorseRiding {
    private int horizontal;
    private int vertical;

    public HorseRiding(int horizontal, int vertical) {
        this.horizontal = horizontal;
        this.vertical = vertical;
    }

    public void setHorizontal(int horizontal) {
        this.horizontal = horizontal;
    }

    public void setVertical(int vertical) {
        this.vertical = vertical;
    }

    public boolean checkMove(int horizontal, int vertical) {
        int rowMove = Math.abs(this.horizontal - horizontal);
        int columnMove = Math.abs(this.vertical - vertical);
        return (rowMove == 1 && columnMove == 2) || (rowMove == 2 && columnMove == 1);
    }
}
