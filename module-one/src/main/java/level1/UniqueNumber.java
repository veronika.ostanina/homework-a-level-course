package level1;

import java.util.HashSet;

public class UniqueNumber {

    private int[] arrayFindNumber;
    private int len;

    public UniqueNumber(int... arrayFindNumber) {
        this.arrayFindNumber = arrayFindNumber;
        len = arrayFindNumber.length;
    }

    public void setArrayFindNumber(int... arrayFindNumber) {
        this.arrayFindNumber = arrayFindNumber;
        len = arrayFindNumber.length;
    }

    public int findUniqueNumber() {
        int quantity = 1;
        if (len == 0) {
            return 0;
        }
        if (len == 1) {
            return 1;
        }
        HashSet<Integer> uniqueValue = new HashSet<>();
        uniqueValue.add(arrayFindNumber[0]);
        for (int i = 1; i < len; i++) {
            if (!uniqueValue.contains(arrayFindNumber[i])) {
                quantity++;
                uniqueValue.add(arrayFindNumber[i]);
            }
        }


        return quantity;
    }
}
