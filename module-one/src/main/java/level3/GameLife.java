package level3;

public class GameLife {
    boolean[][] livePlace;
    int lenRow;
    int lenCol;

    public GameLife(boolean[][] livePlace) {
        this.livePlace = new boolean[livePlace.length + 2][livePlace[0].length + 2];
        lenRow = this.livePlace.length;
        lenCol = this.livePlace[0].length;
        for (int i = 1; i < lenRow - 1; i++) {
            for (int j = 1; j < lenCol - 1; j++) {
                this.livePlace[i][j] = livePlace[i - 1][j - 1];
            }

        }
    }


    public void setLivePlace(boolean[][] livePlace) {

        this.livePlace = new boolean[livePlace.length + 2][livePlace[0].length + 2];
        lenRow = this.livePlace.length;
        lenCol = this.livePlace[0].length;
        for (int i = 1; i < lenRow - 1; i++) {
            for (int j = 1; j < lenCol - 1; j++) {
                this.livePlace[i][j] = livePlace[i - 1][j - 1];
            }

        }

    }

    public boolean[][] makeTurn() {
        boolean[][] nextStatus = new boolean[lenRow - 2][lenCol - 2];
        int countLiveТeighbors;

        for (int i = 1; i < lenRow - 1; i++) {
            for (int j = 1; j < lenCol - 1; j++) {
                countLiveТeighbors = 0;
                for (int k = i - 1; k <= i + 1; k++) {
                    for (int l = j - 1; l <= j + 1; l++) {
                        if (k == i && l == j) continue;
                        if (livePlace[k][l]) {
                            countLiveТeighbors += 1;
                        }
                    }
                }
                if (countLiveТeighbors == 3 || (countLiveТeighbors == 2 && livePlace[i][j])) {
                    nextStatus[i - 1][j - 1] = true;
                }

            }

        }
        return nextStatus;
    }


}
