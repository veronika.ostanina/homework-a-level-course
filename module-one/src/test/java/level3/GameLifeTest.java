package level3;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

class GameLifeTest {
    boolean[][] arrayTest = {{true, true, true},
            {true, true, true},
            {true, true, true}};
    GameLife gameLifeTest = new GameLife(arrayTest);

    @Test
    void makeTurn() {
        boolean[][] checkTest = {{true, false, true}, {false, false, false}, {true, false, true}};
        for (int i = 0, len = checkTest.length; i < len; i++) {
            assertArrayEquals(checkTest[i], gameLifeTest.makeTurn()[i]);
        }
        boolean[][] arrayTestOne = {
                {true, true, false, false},
                {false, true, true, false},
                {false, true, false, false},
                {false, true, false, true}};
        gameLifeTest.setLivePlace(arrayTestOne);
        boolean[][] checkTestOne = {
                {true, true, true, false},
                {false, false, true, false},
                {true, true, false, false},
                {false, false, true, false}};
        for (int i = 0, len = checkTestOne.length; i < len; i++) {
            assertArrayEquals(checkTestOne[i], gameLifeTest.makeTurn()[i]);
        }
        boolean[][] arrayTestTwo = {
                {false, false, false},
                {false, false, false},
                {false, false, false}};
        gameLifeTest.setLivePlace(arrayTestTwo);
        boolean[][] checkTestTwo = {
                {false, false, false},
                {false, false, false},
                {false, false, false}};
        for (int i = 0, len = checkTestTwo.length; i < len; i++) {
            assertArrayEquals(checkTestTwo[i], gameLifeTest.makeTurn()[i]);
        }


    }
}
