package level1;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class UniqueNumberTest {
    UniqueNumber uniqueNumberTest = new UniqueNumber();

    @Test
    void findUniqueNumber() {
        assertEquals(0, uniqueNumberTest.findUniqueNumber());
        uniqueNumberTest.setArrayFindNumber(1);
        assertEquals(1, uniqueNumberTest.findUniqueNumber());
        uniqueNumberTest.setArrayFindNumber(1, 4, 5, 1, 1, 3);
        assertEquals(4, uniqueNumberTest.findUniqueNumber());
        uniqueNumberTest.setArrayFindNumber(10, 4, 5, 1, 1, 3, 9, 12, 8);
        assertEquals(8, uniqueNumberTest.findUniqueNumber());
        uniqueNumberTest.setArrayFindNumber(10, 4, 5, 1, 67, 3, 9, 12, 8);
        assertEquals(9, uniqueNumberTest.findUniqueNumber());
        uniqueNumberTest.setArrayFindNumber(13, 13, 13, 13, 13, 13);
        assertEquals(1, uniqueNumberTest.findUniqueNumber());


    }
}