package level1;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class AreaTriangleTest {
    AreaTriangle areaTriangleTest = new AreaTriangle(0, 0, 3, 2, 7, 5);

    @Test
    void findAreaTriangle() {
        assertEquals(0.5, areaTriangleTest.findAreaTriangle(), 0.001);
        areaTriangleTest.setAreaTriangle(0, 0, 0, 0, 0, 0);
        assertEquals(0, areaTriangleTest.findAreaTriangle(), 0.001);
        areaTriangleTest.setAreaTriangle(4, 5, 1, 7, 7, 23);
        assertEquals(30, areaTriangleTest.findAreaTriangle(), 0.001);

    }
}
