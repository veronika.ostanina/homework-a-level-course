package level1;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class HorseRidingTest {
    HorseRiding horseRidingTest = new HorseRiding(10, 10);

    @Test
    void checkMove() {
        assertFalse(horseRidingTest.checkMove(10, 10));
        assertFalse(horseRidingTest.checkMove(10, 11));
        assertFalse(horseRidingTest.checkMove(11, 11));
        assertTrue(horseRidingTest.checkMove(11, 12));
        assertTrue(horseRidingTest.checkMove(11, 12));
        assertTrue(horseRidingTest.checkMove(12, 11));
        assertTrue(horseRidingTest.checkMove(12, 9));
        assertTrue(horseRidingTest.checkMove(11, 8));
        assertTrue(horseRidingTest.checkMove(9, 12));
        assertTrue(horseRidingTest.checkMove(12, 9));
        assertTrue(horseRidingTest.checkMove(9, 8));
        assertTrue(horseRidingTest.checkMove(8, 9));


    }
}