package level2;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class StringSymbolTest {
    StringSymbol stringSymbolTemp = new StringSymbol("[(){}[]]");

    @Test
    void checkStringSymbol() {
        assertTrue(stringSymbolTemp.checkStringSymbol());
        stringSymbolTemp.setStringSymbol("[)]");
        assertFalse(stringSymbolTemp.checkStringSymbol());
        stringSymbolTemp.setStringSymbol("([)])");
        assertFalse(stringSymbolTemp.checkStringSymbol());
        stringSymbolTemp.setStringSymbol("");
        assertTrue(stringSymbolTemp.checkStringSymbol());
        stringSymbolTemp.setStringSymbol("[]{}()");
        assertTrue(stringSymbolTemp.checkStringSymbol());
        stringSymbolTemp.setStringSymbol("[({[()]})]");
        assertTrue(stringSymbolTemp.checkStringSymbol());
        stringSymbolTemp.setStringSymbol("[[[[({({{}[[[]((()))]]})})]]]]");
        assertTrue(stringSymbolTemp.checkStringSymbol());


    }
}