package level2;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class MaxDepthTest {
    TreeNode treeNodeTest = new TreeNode(1);
    MaxDepth maxDepthTest = new MaxDepth();

    @Test
    void maxDepth() {
        TreeNode treeNode = new TreeNode(1);

        treeNode.right = new TreeNode(2);
        treeNode.left = new TreeNode(2);

        treeNode.right.right = new TreeNode(3);
        assertEquals(3, maxDepthTest.maxDepth(treeNode));

        treeNode.right.right.left = new TreeNode(4);
        treeNode.right.right.right = new TreeNode(4);
        assertEquals(4, maxDepthTest.maxDepth(treeNode));

        treeNode.right.right.right.right = new TreeNode(5);
        assertEquals(5, maxDepthTest.maxDepth(treeNode));

        treeNode.right.right.right.right.left = new TreeNode(6);
        assertEquals(6, maxDepthTest.maxDepth(treeNode));

        assertEquals(0, maxDepthTest.maxDepth(null));


    }
}