package homeworkSQL;

import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertTrue;

class MinCostsTest extends ConnectionDB {
    MinCosts minCostsTest = new MinCosts();

    @Test
    void testCreateFillDB() {

        List<String> names = new ArrayList<>(List.of("gdansk", "bydgoszcz", "vozn", "torun", "warszawa", "kiev"));
        List<TownsCost> neighborsCost = new ArrayList<>(List.of(
                new TownsCost("gdansk", "bydgoszcz", 1),
                new TownsCost("gdansk", "torun", 3),
                new TownsCost("bydgoszcz", "gdansk", 1),
                new TownsCost("bydgoszcz", "torun", 1),
                new TownsCost("bydgoszcz", "warszawa", 4),
                new TownsCost("torun", "gdansk", 3),
                new TownsCost("torun", "bydgoszcz", 1),
                new TownsCost("torun", "warszawa", 1),
                new TownsCost("kiev", "vozn", 1),
                new TownsCost("vozn", "kiev", 1),
                new TownsCost("warszawa", "bydgoszcz", 4),
                new TownsCost("warszawa", "torun", 1)));
        List<TownsCost> calculateCost = new ArrayList<>(List.of(
                new TownsCost("gdansk", "warszawa"),
                new TownsCost("kiev", "warszawa"),
                new TownsCost("bydgoszcz", "warszawa")));

        minCostsTest.createFillDB(names, neighborsCost, calculateCost);
        assertTrue(isExistValue("\"PathFinderData\".Problems"));
        assertTrue(isExistValue("\"PathFinderData\".City"));
        assertTrue(isExistValue("\"PathFinderData\".Connection"));

    }

    @Test
    void loadData() {
        assertDoesNotThrow(() -> minCostsTest.loadData());
    }

    @Test
    void calculateSolution() throws IOException {
        minCostsTest.loadData();
        assertDoesNotThrow(() -> minCostsTest.calculateSolution());

    }

    @Test
    void writeFileResult() throws IOException {
        minCostsTest.loadData();
        minCostsTest.calculateSolution();
        assertDoesNotThrow(() -> minCostsTest.writeFileResult());
        assertTrue(isExistValue("\"PathFinderData\".Found_routs"));
        assertTrue(isExistValue("\"PathFinderData\".Impossible_routs"));
        assertTrue(getValueSelectPossibleRoots(3, 2));
        assertTrue(getValueSelectImpossibleRoots(2));
    }

    private boolean getValueSelectPossibleRoots(int valueFirst, int valueSecond) {
        try (Connection connection = DriverManager.getConnection(url, props)) {
            try (PreparedStatement getCost = connection.prepareStatement("Select cost from \"PathFinderData\".Found_routs")) {
                ResultSet resSet = getCost.executeQuery();
                if (resSet.next() && resSet.getInt(1) != valueFirst) {
                    return false;
                }
                if (resSet.next() && resSet.getInt(1) != valueSecond) {
                    return false;
                }
                if (resSet.next()) {
                    return false;
                }
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return true;
    }

    private boolean getValueSelectImpossibleRoots(int valueFirst) {
        try (Connection connection = DriverManager.getConnection(url, props)) {
            try (PreparedStatement getCost = connection.prepareStatement("Select id_problem from \"PathFinderData\".Impossible_routs")) {
                ResultSet resSet = getCost.executeQuery();
                if (resSet.next() && resSet.getInt(1) != valueFirst) {
                    return false;
                }
                if (resSet.next()) {
                    return false;
                }
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return true;
    }

    private boolean isExistValue(String name) {
        try (Connection connection = DriverManager.getConnection(url, props)) {
            try (PreparedStatement checkTables = connection.prepareStatement("Select  * FROM " + name + ";")) {
                ResultSet resSet = checkTables.executeQuery();
                if (!resSet.next()) {
                    return false;
                }
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return true;
    }
}