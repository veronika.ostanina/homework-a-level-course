package homeworkSQL;

import org.junit.jupiter.api.Test;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.LinkedHashMap;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class TownTest {

    @Test
    void getNeighbors() {
        assertThrows(RuntimeException.class, () -> new Town("Town1", 0, 0));
        assertThrows(RuntimeException.class, () -> new Town("Town1", 3, 0));
        assertThrows(RuntimeException.class, () -> new Town("Town1", 0, 3));

        LinkedHashMap<Integer, Integer> neighborsCheck = new LinkedHashMap<>();
        neighborsCheck.put(2, 10);
        neighborsCheck.put(5, 50);
        neighborsCheck.put(4, 20);
        assertEquals(neighborsCheck, getLinkedList("D:/aleveljavanix/inputOutputFilesTask3/inputTown.txt"));
        assertThrows(Exception.class, () -> getLinkedList("D:/aleveljavanix/inputOutputFilesTask3/inputTown2.txt"));

    }

    private LinkedHashMap<Integer, Integer> getLinkedList(String path) {
        File file = new File(path);
        LinkedHashMap<Integer, Integer> neighbors = new LinkedHashMap<>();
        try (InputStream in = new FileInputStream(file);
             BufferedReader bufferedReader =
                     new BufferedReader(new InputStreamReader(in, StandardCharsets.UTF_8))) {
            bufferedReader.lines().
                    forEach(e -> {
                        String[] temp = e.split(" ");
                        neighbors.put(Integer.parseInt(temp[0]), Integer.parseInt(temp[1]));
                    });
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
        return neighbors;
    }
}