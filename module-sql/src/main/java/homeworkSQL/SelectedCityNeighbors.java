package homeworkSQL;

import java.util.LinkedHashMap;

public class SelectedCityNeighbors {
    private int id;
    private String name;
    private LinkedHashMap<Integer, Integer> neighbors = new LinkedHashMap<>();


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSizeNeighbors() {
        return neighbors.size();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LinkedHashMap<Integer, Integer> getNeighbors() {
        return neighbors;
    }

    public void setNeighbors(int to, int cost) {
        neighbors.put(to, cost);
    }
}
