package homeworkSQL;

import java.io.IOException;
import java.io.InputStream;
import java.io.UncheckedIOException;
import java.util.Properties;

public class ConnectionDB {
    protected final Properties props = loadProperties("JdbcBox.properties");
    protected final String url = props.getProperty("url");

    protected Properties loadProperties(String FileProperties) {
        Properties props = new Properties();
        try (InputStream input = ConnectionDB.class.getResourceAsStream(FileProperties)) {
            props.load(input);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
        return props;
    }
}
