package homeworkSQL;

import java.io.IOException;
import java.util.List;

public class MinCosts {

    private ProfitableWays profitableWaysDB;

    public void createFillDB(List<String> names, List<TownsCost> neighborsCost, List<TownsCost> townsFindCost) {
        CreateFillDB newFilledDB = new CreateFillDB();
        newFilledDB.createDB();
        newFilledDB.fillDB(names, neighborsCost, townsFindCost);
    }

    public void createFillDB() {
        CreateFillDB newFilledDB = new CreateFillDB();
        newFilledDB.createDB();
        newFilledDB.fillDB();
    }

    public void loadData() throws IOException {
        GetDataDB getDataDB = new GetDataDB();
        int townSize = getDataDB.getCountCity();
        if (townSize == 0) {
            throw new RuntimeException("Table is not exist!");
        }
        profitableWaysDB = new ProfitableWays();
        profitableWaysDB.setTownsSize(townSize)
                .fullTowns(getDataDB.fillSelectedCityNeighbors())
                .setTownCalculateMinCostSize(getDataDB.getCountCalculateCost())
                .fullTownCalculateMinCost(getDataDB.fillTownCalculateMinCost())
                .build();

    }

    public boolean calculateSolution() {
        if (profitableWaysDB == null) {
            return false;
        }
        profitableWaysDB.calculateSolution();
        return true;
    }

    public void writeFileResult() {
        CreateFillDB fillResultTable = new CreateFillDB();
        fillResultTable.fillTableResult(profitableWaysDB.getTownCalculateMinCost());
    }

}
