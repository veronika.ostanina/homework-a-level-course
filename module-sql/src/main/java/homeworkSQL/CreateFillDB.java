package homeworkSQL;

import java.sql.*;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class CreateFillDB extends ConnectionDB {
    private List<String> names = new ArrayList<>(List.of("gdansk", "bydgoszcz", "vozn", "torun", "warszawa", "kiev"));
    private List<TownsCost> neighborsCost = new ArrayList<>(List.of(
            new TownsCost("gdansk", "bydgoszcz", 1),
            new TownsCost("gdansk", "torun", 3),
            new TownsCost("bydgoszcz", "gdansk", 1),
            new TownsCost("bydgoszcz", "torun", 1),
            new TownsCost("bydgoszcz", "warszawa", 4),
            new TownsCost("torun", "gdansk", 3),
            new TownsCost("torun", "bydgoszcz", 1),
            new TownsCost("torun", "warszawa", 1),
            new TownsCost("kiev", "vozn", 1),
            new TownsCost("vozn", "kiev", 1),
            new TownsCost("warszawa", "bydgoszcz", 4),
            new TownsCost("warszawa", "torun", 1)));
    private List<TownsCost> calculateCost = new ArrayList<>(List.of(
            new TownsCost("gdansk", "warszawa"),
            new TownsCost("kiev", "warszawa"),
            new TownsCost("bydgoszcz", "warszawa")));


    public void createDB() {
        try (Connection connection = DriverManager.getConnection(url, props)) {
            try (Statement createTables = connection.createStatement()) {
                createTables.addBatch(
                        "CREATE TABLE \"PathFinderData\".City ( " +
                                "id_city SERIAL NOT NULL UNIQUE," +
                                "name VARCHAR NOT NULL UNIQUE, " +
                                "CONSTRAINT PK_City PRIMARY KEY (id_city));");
                createTables.addBatch("CREATE TABLE \"PathFinderData\".Connection ( " +
                        "cost INT NOT NULL ," +
                        "from_City INT NOT NULL , " +
                        "to_City INT NOT NUll," +
                        "CONSTRAINT PK_Connection PRIMARY KEY (cost, from_City, to_City)," +
                        "CONSTRAINT FK1_Connection_City FOREIGN KEY (from_City) REFERENCES \"PathFinderData\".City (id_city)," +
                        "CONSTRAINT FK2_Connection_City FOREIGN KEY (to_City) REFERENCES \"PathFinderData\".City (id_city));");
                createTables.addBatch("CREATE TABLE \"PathFinderData\".Problems ( " +
                        "id_Problem SERIAL NOT NULL ," +
                        "from_City INT NOT NULL , " +
                        "to_City INT NOT NUll," +
                        "CONSTRAINT PK_Problems PRIMARY KEY (id_Problem)," +
                        "CONSTRAINT FK1_Problems_City FOREIGN KEY (from_City) REFERENCES \"PathFinderData\".City (id_city)," +
                        "CONSTRAINT FK2_Problems_City FOREIGN KEY (to_City) REFERENCES \"PathFinderData\".City (id_city));");
                createTables.addBatch("CREATE TABLE \"PathFinderData\".Found_Routs ( " +
                        "id_Problem INT NOT NULL UNIQUE ," +
                        "cost INT NOT NULL , " +
                        "CONSTRAINT PK_Found_Routs PRIMARY KEY (id_Problem)," +
                        "CONSTRAINT FK_Found_Routs_Problems FOREIGN KEY (id_Problem) REFERENCES \"PathFinderData\".Problems (id_Problem));");
                createTables.addBatch("CREATE TABLE \"PathFinderData\".Impossible_Routs ( " +
                        "id_Problem INT NOT NULL UNIQUE ," +
                        "CONSTRAINT PK_Impossible_Routs PRIMARY KEY (id_Problem)," +
                        "CONSTRAINT FK_Impossible_Routs_Problems FOREIGN KEY (id_Problem) REFERENCES \"PathFinderData\".Problems (id_Problem));");
                createTables.executeBatch();
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void fillDB() {
        fillDBData();
    }

    public void fillDB(List<String> names, List<TownsCost> neighborsCost, List<TownsCost> townsFindCost) {
        this.names = names;
        this.neighborsCost = neighborsCost;
        this.calculateCost = townsFindCost;
        fillDBData();
    }

    private void fillDBData() {
        Map<String, Integer> nameId = new LinkedHashMap<>();
        try (Connection connection = DriverManager.getConnection(url, props)) {
            nameId = fillNamesTown(connection);
            fillConnection(nameId, connection);
            fillProblems(nameId, connection);
        } catch (SQLException throwables) {
            throw new RuntimeException();
        }
    }

    private Map<String, Integer> fillNamesTown(Connection connection) throws SQLException {
        Map<String, Integer> nameId = new LinkedHashMap<>();
        try (PreparedStatement fillTables = connection.prepareStatement("INSERT INTO \"PathFinderData\".City (name) VALUES (?) ON CONFLICT DO NOTHING;",
                PreparedStatement.RETURN_GENERATED_KEYS)) {
            for (String name : names) {
                fillTables.setString(1, name);
                fillTables.addBatch();
            }
            fillTables.executeBatch();
            ResultSet generatedKeys = fillTables.getGeneratedKeys();
            int i = 0;
            while (generatedKeys.next()) {
                nameId.put(names.get(i), generatedKeys.getInt("id_city"));
                i++;
            }
        }
        return nameId;
    }

    private void fillConnection(Map<String, Integer> nameId, Connection connection) throws SQLException {
        try (PreparedStatement fillTables = connection.prepareStatement("INSERT INTO \"PathFinderData\".Connection  VALUES (?,?,?) ON CONFLICT DO NOTHING;")) {
            for (TownsCost townsCost : neighborsCost) {
                fillTables.setInt(1, townsCost.getCost());
                fillTables.setInt(2, nameId.get(townsCost.getTownFrom()));
                fillTables.setInt(3, nameId.get(townsCost.getTownTo()));
                fillTables.addBatch();
            }
            fillTables.executeBatch();
        }
    }

    private void fillProblems(Map<String, Integer> nameId, Connection connection) throws SQLException {
        try (PreparedStatement fillTables = connection.prepareStatement("INSERT INTO \"PathFinderData\".Problems (from_City, to_City)  VALUES (?,?) ON CONFLICT DO NOTHING;")) {
            for (TownsCost townsCost : calculateCost) {
                fillTables.setInt(1, nameId.get(townsCost.getTownFrom()));
                fillTables.setInt(2, nameId.get(townsCost.getTownTo()));
                fillTables.addBatch();
            }
            fillTables.executeBatch();
        }
    }

    public void fillTableResult(List<TownsCost> result) {
        try (Connection connection = DriverManager.getConnection(url, props)) {
            try (PreparedStatement fillTables = connection.prepareStatement("INSERT INTO \"PathFinderData\".found_routs   VALUES (?,?) ON CONFLICT DO NOTHING")) {
                for (TownsCost townsCost : result) {
                    if (townsCost.getCost() == Integer.MAX_VALUE) {
                        fillImpossibleRouts(townsCost, connection);
                    } else {
                        fillTables.setInt(1, townsCost.getProblems());
                        fillTables.setInt(2, townsCost.getCost());
                    }
                    fillTables.addBatch();
                }
                fillTables.executeBatch();
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    private void fillImpossibleRouts(TownsCost towmCost, Connection connection) throws SQLException {
        try (PreparedStatement fillTables = connection.prepareStatement("INSERT INTO \"PathFinderData\".impossible_routs   VALUES (?) ON CONFLICT DO NOTHING")) {
            fillTables.setInt(1, towmCost.getProblems());
            fillTables.execute();
        }


    }
}


