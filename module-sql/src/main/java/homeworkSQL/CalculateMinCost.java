package homeworkSQL;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class CalculateMinCost {

    private List<Town> towns = new ArrayList<>();

    public int findMinCost(String from, String to, List<Town> towns) {
        this.towns = towns;
        int numberFrom = 0;
        int numberTo = 0;

        for (Town e : towns) {
            if (e.getNameTown().equals(from)) {
                numberFrom = e.getNumberTown();
            } else if (e.getNameTown().equals(to)) {
                numberTo = e.getNumberTown();
            }
        }
        if (numberFrom == 0 || numberTo == 0) {
            throw new IllegalArgumentException("Array Towns don`t have needed town to calculate");
        }
        Set<Integer> checkedTowns = new HashSet<>();
        return minCost(numberFrom, numberTo, 0, checkedTowns);

    }

    private int minCost(int neighbors, int endNeighbors, int value, Set<Integer> checkedTowns) {
        if (value > 200_000) {
            return Integer.MAX_VALUE;
        }
        if (neighbors == endNeighbors) {
            return value;
        }
        List<Integer> tempCost = new ArrayList<>();
        var currentTown = towns.get(neighbors - 1);
        currentTown.getNeighbors().forEach((neighbor, cost) -> {
            if (!checkedTowns.contains(neighbor) && neighbor != neighbors) {
                checkedTowns.add(neighbors);
                tempCost.add(minCost(neighbor, endNeighbors, value + cost, checkedTowns));
                checkedTowns.remove(neighbors);
            }
        });

        return tempCost.isEmpty() ? Integer.MAX_VALUE : tempCost.stream().min(Integer::compareTo).get();
    }


}
