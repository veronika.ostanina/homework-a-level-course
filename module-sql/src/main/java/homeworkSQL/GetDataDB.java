package homeworkSQL;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class GetDataDB extends ConnectionDB {

    public int getCountCity() {
        int resultCount = 0;
        try (Connection connection = DriverManager.getConnection(url, props)) {
            try (Statement getCountCity = connection.createStatement()) {
                ResultSet countCity = getCountCity.executeQuery("SELECT COUNT (*) FROM \"PathFinderData\".City;");
                countCity.next();
                resultCount = countCity.getInt(1);
            }

        } catch (SQLException throwables) {
            throw new RuntimeException(throwables);
        }
        return resultCount;
    }

    public List<SelectedCityNeighbors> fillSelectedCityNeighbors() {
        List<SelectedCityNeighbors> listDataCity = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(url, props)) {
            try (Statement getCountCity = connection.createStatement()) {
                ResultSet city = getCountCity.executeQuery("SELECT * FROM \"PathFinderData\".City;");
                while (city.next()) {
                    SelectedCityNeighbors selectedCityNeighbors = new SelectedCityNeighbors();
                    selectedCityNeighbors.setId(city.getInt("id_city"));
                    selectedCityNeighbors.setName(city.getString("name"));
                    try (Statement getCountN = connection.createStatement()) {
                        ResultSet neighbors = getCountN.executeQuery("SELECT to_city, cost FROM \"PathFinderData\".Connection WHERE from_city = " + city.getInt("id_city") + ";");
                        while (neighbors.next()) {
                            selectedCityNeighbors.setNeighbors(neighbors.getInt("to_city"), neighbors.getInt("cost"));
                        }
                    }
                    listDataCity.add(selectedCityNeighbors);
                }
            }

        } catch (SQLException throwables) {
            throw new RuntimeException(throwables);
        }
        return listDataCity;
    }

    public int getCountCalculateCost() {
        int resultCount = 0;
        try (Connection connection = DriverManager.getConnection(url, props)) {
            try (Statement getCountCalculateCost = connection.createStatement()) {
                ResultSet countCity = getCountCalculateCost.executeQuery("SELECT COUNT (*) FROM \"PathFinderData\".Problems;");
                countCity.next();
                resultCount = countCity.getInt(1);
            }

        } catch (SQLException throwables) {
            throw new RuntimeException();
        }
        return resultCount;
    }

    public List<TownsCost> fillTownCalculateMinCost() {
        List<TownsCost> townCalculateMinCost = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(url, props)) {
            try (Statement getCountCity = connection.createStatement()) {
                ResultSet townsCalculate = getCountCity.executeQuery("SELECT \"PathFinderData\".problems.id_problem, \"PathFinderData\".city.name\n" +
                        "FROM \"PathFinderData\".city\n" +
                        "INNER JOIN  \"PathFinderData\".problems ON " +
                        "problems.from_city = \"PathFinderData\".city.id_city " +
                        "OR problems.to_city = \"PathFinderData\".city.id_city;");
                while (townsCalculate.next()) {
                    TownsCost townsCost = new TownsCost();
                    townsCost.setProblems(townsCalculate.getInt("id_problem"));
                    townsCost.setTownFrom(townsCalculate.getString("name"));
                    townsCalculate.next();
                    townsCost.setTownTo(townsCalculate.getString("name"));

                    townCalculateMinCost.add(townsCost);
                }
            }

        } catch (SQLException throwables) {
            throw new RuntimeException();
        }
        return townCalculateMinCost;
    }
}
    

