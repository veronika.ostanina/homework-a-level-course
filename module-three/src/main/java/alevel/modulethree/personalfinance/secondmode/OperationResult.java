package alevel.modulethree.personalfinance.secondmode;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

public class OperationResult {
    private static int saldo = 0;
    private static int sumIncome = 0;
    private int idOperation;
    private int amountMoney;
    private Instant date;
    private int accountFinance;

    public OperationResult(int idOperation, int amountMoney, Instant date, int accountFinance) {
        this.idOperation = idOperation;
        this.amountMoney = amountMoney;
        this.date = date;
        this.accountFinance = accountFinance;
        if (amountMoney > 0) {
            sumIncome += amountMoney;
        }
        saldo += amountMoney;
    }


    public String[] getArrayValue() {
        String[] data = new String[6];
        data[0] = String.valueOf(idOperation);
        data[1] = String.valueOf(amountMoney);
        DateTimeFormatter isoLocaDateTime = DateTimeFormatter.ISO_LOCAL_DATE_TIME;
        data[2] = LocalDateTime.ofInstant(date, ZoneOffset.UTC).format(isoLocaDateTime);
        data[3] = String.valueOf(accountFinance);
        data[4] = String.valueOf(sumIncome);
        data[5] = String.valueOf(saldo);
        return data;
    }
}
