package alevel.modulethree.personalfinance.secondmode;

import alevel.modulethree.personalfinance.app.InitialData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class AccountStatement {
    private static final Logger LOG = LoggerFactory.getLogger(AccountStatement.class);
    private final FileWriterResult fileResult;
    private final String url;
    private final String password;
    private final String username;
    private final int idUser;
    private int idAccount;

    public AccountStatement(String fileURL, InitialData initialData, String resultWritePath) {
        url = fileURL;
        password = initialData.getPassword();
        username = initialData.getUsername();
        idUser = initialData.getId().intValue();
        fileResult = new FileWriterResult(resultWritePath);
        LOG.info("User choose to save data to file");

    }

    public List<Map<Integer, Integer>> getAccount() {
        List<Map<Integer, Integer>> result = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(url, username, password)) {
            try (PreparedStatement getCountCity = connection.prepareStatement("SELECT idaccount, balance from account where  userfinance = ? ;")) {
                getCountCity.setInt(1, idUser);
                ResultSet account = getCountCity.executeQuery();
                while (account.next()) {
                    result.add(Map.of(account.getInt(1), account.getInt(2)));
                }
            }
        } catch (SQLException throwables) {
            throw new RuntimeException(throwables);
        }
        return result;
    }

    public void setAccount(Map<Integer, Integer> account) {
        idAccount = account.keySet().stream().findFirst().orElseThrow();
        LOG.info("User account {}", idAccount);
    }

    public void makeStatement(Date from, Date to) {
        try (Connection connection = DriverManager.getConnection(url, username, password)) {
            try (PreparedStatement getCountCity = connection.prepareStatement("SELECT * from operation where accountfinance = ? AND time_operation BETWEEN ? AND ? ;")) {
                if (to.before(from)) {
                    Date temp = to;
                    to = from;
                    from = temp;
                }
                getCountCity.setInt(1, idAccount);
                getCountCity.setTimestamp(2, new Timestamp(from.getTime()));
                getCountCity.setTimestamp(3, new Timestamp(to.getTime()));
                ResultSet operations = getCountCity.executeQuery();
                List<String[]> resultWrite = new ArrayList<>();
                while (operations.next()) {
                    int id = operations.getInt(1);
                    int amountMoney = operations.getInt(2);
                    Instant time = operations.getTimestamp(3).toInstant();
                    int account = operations.getInt(4);
                    OperationResult operRes = new OperationResult(id, amountMoney, time, account);
                    resultWrite.add(operRes.getArrayValue());
                }
                fileResult.createFile(resultWrite);
                LOG.info("File was created");
            }

        } catch (SQLException throwables) {
            LOG.info("File wasn`t created");
            throw new RuntimeException(throwables);
        }
    }

}
