package alevel.modulethree.personalfinance.secondmode;

import au.com.bytecode.opencsv.CSVWriter;

import java.io.FileWriter;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.List;

public class FileWriterResult {
    private String path;


    public FileWriterResult(String path) {
        this.path = path;
    }

    public void createFile(List<String[]> operResult) {

        try (CSVWriter writer = new CSVWriter(new FileWriter(path), ',', '\u0000')) {
            String[] headers = {"IdOperation", "AmountMany", "Data", "Account", "SumIncome", "Saldo"};
            writer.writeNext(headers);
            writer.writeAll(operResult);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

}
