package alevel.modulethree.personalfinance.firstmode;

import alevel.modulethree.personalfinance.app.InitialData;
import alevel.modulethree.personalfinance.entity.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Instant;
import java.util.List;

public class MakeOperation implements AutoCloseable {
    private static final Logger LOG = LoggerFactory.getLogger(MakeOperation.class);
    private final Configuration configuration;
    private final SessionFactory sessionFactory;
    private final Session session;
    private final UserFinance userFinance;
    private OperationFinance operationFinance;

    public MakeOperation(InitialData initData) {
        LOG.info("User chose operation");
        configuration = new Configuration()
                .setProperty("hibernate.connection.username", initData.getUsername())
                .setProperty("hibernate.connection.password", initData.getPassword())
                .configure();
        sessionFactory = configuration.buildSessionFactory();
        session = sessionFactory.openSession();


        userFinance = session.load(UserFinance.class, initData.getId());

        if (userFinance == null) {
            LOG.error("User isn`t exist");
            throw new IllegalArgumentException("User isn`t exist");
        }
        LOG.info("User log in");
    }

    public List<AccountFinance> getAccountUser() {
        return userFinance.getAccountFinances();
    }

    public void fillAccount(AccountFinance accountFinance) {
        operationFinance = new OperationFinance();
        operationFinance.setAccountFinance(accountFinance);
        LOG.info("User chose account {}", accountFinance.getIdAccount());
    }

    public List<?> startOperation(Long sum) {
        session.beginTransaction();
        operationFinance.setAmountMoney(sum);
        LOG.info("User put sum {}", sum);
        operationFinance.setTime(Instant.now());
        LOG.info("Time {}", Instant.now());
        operationFinance.getAccountFinance().setBalance(operationFinance.getAccountFinance().getBalance() + sum);
        session.getTransaction().commit();
        if (sum > 0) {
            Query<Income> query = session.createQuery("from Income", Income.class);
            return query.list();
        } else {
            Query<Consumption> query = session.createQuery("from Consumption", Consumption.class);
            return query.list();
        }
    }

    public <E> void fillCategory(List<E> category) {
        session.beginTransaction();
        if (operationFinance.getAmountMoney() > 0) {
            List<Income> incomes = (List<Income>) category;
            incomes.forEach(operationFinance::addIncome);
        } else {
            List<Consumption> consumptions = (List<Consumption>) category;
            consumptions.forEach(operationFinance::addConsumption);
        }
        LOG.info("Operation was filled successfully");
        session.saveOrUpdate(operationFinance);
        session.getTransaction().commit();
    }

    @Override
    public void close() throws Exception {
        session.close();
        sessionFactory.close();
    }
}
