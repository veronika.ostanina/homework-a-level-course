package alevel.modulethree.personalfinance.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "account")
public class AccountFinance {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idAccount;
    @Column(name = "balance", nullable = false)
    private Long balance;
    @ManyToOne()
    @JoinColumn(name = "userFinance")
    private UserFinance userFinance;
    @OneToMany(mappedBy = "accountFinance", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<OperationFinance> operationFinances = new ArrayList<>();

    public AccountFinance() {
    }

    public AccountFinance(Long balance, UserFinance userFinance) {

        this.balance = balance;
        this.userFinance = userFinance;
        userFinance.addAccount(this);
    }

    public List<OperationFinance> getOperationFinances() {
        return operationFinances;
    }

    public void setOperationFinances(List<OperationFinance> operationFinances) {
        this.operationFinances = operationFinances;
    }

    public Long getIdAccount() {
        return idAccount;
    }

    public void setIdAccount(Long idAccount) {
        this.idAccount = idAccount;
    }

    public Long getBalance() {
        return balance;
    }

    public void setBalance(Long balance) {
        this.balance = balance;
    }

    public UserFinance getUserFinance() {
        return userFinance;
    }

    public void setUserFinance(UserFinance userFinance) {
        this.userFinance = userFinance;
    }

    public void addOperation(OperationFinance operationFinance) {
        operationFinances.add(operationFinance);
    }

    @Override
    public String toString() {
        return String.valueOf(balance);
    }
}
