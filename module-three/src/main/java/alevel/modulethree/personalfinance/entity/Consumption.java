package alevel.modulethree.personalfinance.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "consumption")
public class Consumption {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idConsumption;
    @Column(name = "name", nullable = false)
    private String name;
    @ManyToMany(mappedBy = "consumptions")
    private List<OperationFinance> operationFinances = new ArrayList<>();

    public Consumption() {
    }

    public Consumption(String name) {
        this.name = name;
    }

    public List<OperationFinance> getOperationFinances() {
        return operationFinances;
    }

    public void setOperationFinances(List<OperationFinance> operationFinances) {
        this.operationFinances = operationFinances;
    }

    public Long getIdConsumption() {
        return idConsumption;
    }

    public void setIdConsumption(Long idConsumption) {
        this.idConsumption = idConsumption;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void addOperation(OperationFinance operationFinance) {
        operationFinances.add(operationFinance);
        operationFinance.addConsumption(this);
    }

    @Override
    public String toString() {
        return name;
    }
}
