package alevel.modulethree.personalfinance.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "user_finance")
public class UserFinance {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idUser;
    @OneToMany(mappedBy = "userFinance", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<AccountFinance> accountFinances = new ArrayList<>();

    public UserFinance() {
    }

    public UserFinance(Long idUser) {
        this.idUser = idUser;
    }

    public List<AccountFinance> getAccountFinances() {
        return accountFinances;
    }

    public void setAccountFinances(List<AccountFinance> accountFinances) {
        this.accountFinances = accountFinances;
    }

    public void addAccount(AccountFinance accountFinance) {
        accountFinances.add(accountFinance);
    }

    public Long getIdUser() {
        return idUser;
    }

    public void setIdUser(Long idUser) {
        this.idUser = idUser;
    }

    @Override
    public String toString() {
        return idUser.toString();
    }
}
