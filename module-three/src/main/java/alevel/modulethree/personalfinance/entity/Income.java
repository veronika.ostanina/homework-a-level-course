package alevel.modulethree.personalfinance.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "income")
public class Income {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idIncome;
    @Column(name = "name", nullable = false)
    private String name;
    @ManyToMany(mappedBy = "incomes")
    private List<OperationFinance> operationFinances = new ArrayList<>();

    public Income(String name) {
        this.name = name;
    }

    public Income() {
    }

    public List<OperationFinance> getOperationFinances() {
        return operationFinances;
    }

    public void setOperationFinances(List<OperationFinance> operationFinances) {
        this.operationFinances = operationFinances;
    }

    public Long getIdIncome() {
        return idIncome;
    }

    public void setIdIncome(Long idIncome) {
        this.idIncome = idIncome;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void addOperation(OperationFinance operationFinance) {
        operationFinances.add(operationFinance);
        operationFinance.addIncome(this);
    }

    @Override
    public String toString() {
        return name;
    }
}
