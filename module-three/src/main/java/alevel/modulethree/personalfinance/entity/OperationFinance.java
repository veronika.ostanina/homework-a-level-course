package alevel.modulethree.personalfinance.entity;


import javax.persistence.*;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "operation")
public class OperationFinance {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idOperation;
    @Column(name = "amountMoney", nullable = false)
    private Long amountMoney;
    @Column(name = "time_operation", nullable = false)
    private Instant time;


    @ManyToOne()
    @JoinColumn(name = "accountFinance")
    private AccountFinance accountFinance;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(
            name = "Operation_Income",
            joinColumns = {@JoinColumn(name = "operation_id")},
            inverseJoinColumns = {@JoinColumn(name = "income_id")}

    )
    private List<Income> incomes = new ArrayList<>();

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(
            name = "Operation_Consumption",
            joinColumns = {@JoinColumn(name = "operation_id")},
            inverseJoinColumns = {@JoinColumn(name = "consumption_id")}

    )
    private List<Consumption> consumptions = new ArrayList<>();

    public OperationFinance() {
    }

    public Instant getTime() {
        return time;
    }

    public void setTime(Instant time) {
        this.time = time;
    }

    public Long getIdOperation() {
        return idOperation;
    }

    public void setIdOperation(Long idOperation) {
        this.idOperation = idOperation;
    }

    public Long getAmountMoney() {
        return amountMoney;
    }

    public void setAmountMoney(Long amountMoney) {
        if (amountMoney == 0) {
            throw new IllegalArgumentException("Amount many of operation is 0!");
        }
        this.amountMoney = amountMoney;
    }

    public AccountFinance getAccountFinance() {
        return accountFinance;
    }

    public void setAccountFinance(AccountFinance accountFinance) {
        this.accountFinance = accountFinance;
    }

    public List<Income> getIncomes() {
        return incomes;
    }

    public void setIncomes(List<Income> incomes) {
        isEmpty(consumptions);
        this.incomes = incomes;
    }


    public List<Consumption> getConsumptions() {
        return consumptions;
    }

    public void setConsumptions(List<Consumption> consumptions) {
        isEmpty(incomes);
        this.consumptions = consumptions;
    }

    public void addIncome(Income income) {
        isEmpty(consumptions);
        incomes.add(income);

    }

    public void addConsumption(Consumption consumption) {
        isEmpty(incomes);
        consumptions.add(consumption);
    }

    private <E> void isEmpty(List<E> checkList) {
        if (!checkList.isEmpty()) {

            throw new IllegalArgumentException("Consumption is exist! " + checkList.size());
        }
    }

}
