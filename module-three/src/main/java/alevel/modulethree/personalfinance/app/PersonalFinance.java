package alevel.modulethree.personalfinance.app;

import alevel.modulethree.personalfinance.firstmode.MakeOperation;
import alevel.modulethree.personalfinance.secondmode.AccountStatement;

public class PersonalFinance {
    private final InputConsole getUserData = new InputConsole();

    public PersonalFinance(String[] initData) {
        InitialData initialData = new InitialData();
        initialData.setUsername(initData[0]);
        initialData.setPassword(initData[1]);
        initialData.setId(Long.parseLong(initData[2]));
        if (getUserData.takeMode() == 1) {

            makeOperation(initialData);
        } else {
            accountStatement(initialData);
        }
    }

    public static void main(String[] args) {

        PersonalFinance pf = new PersonalFinance(args);
    }

    private void makeOperation(InitialData initD) {
        MakeOperation operation = new MakeOperation(initD);
        operation.fillAccount(getUserData.chooseAccount(operation.getAccountUser()));
        var list = operation.startOperation(getUserData.takeOperation());
        operation.fillCategory(getUserData.takeIncomeConsumption(list));

    }

    private void accountStatement(InitialData initD) {
        AccountStatement accountStatement = new AccountStatement("jdbc:postgresql://localhost:5432/PersonalFinance", initD, getUserData.takePathResult());
        accountStatement.setAccount(getUserData.chooseAccountID(accountStatement.getAccount()));
        accountStatement.makeStatement(getUserData.getDate("from"), getUserData.getDate("to"));
    }
}
