package alevel.modulethree.personalfinance.app;

import alevel.modulethree.personalfinance.entity.AccountFinance;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UncheckedIOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class InputConsole {

    public int takeMode() {
        int mode = 0;
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Please, enter make operation[1] or export statement[2]:");
        try {
            mode = Integer.parseInt(reader.readLine());
            if (mode != 1 && mode != 2) {
                throw new IOException();
            }
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
        return mode;

    }

    public AccountFinance chooseAccount(List<AccountFinance> accountFinances) {
        int account = 0;
        accountFinances.forEach(System.out::println);
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Please, chose account to work [1-" + accountFinances.size() + "] : ");
        try {
            account = Integer.parseInt(reader.readLine());
            accountFinances.get(account - 1);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return accountFinances.get(account - 1);
    }

    public Long takeOperation() {
        Long sumOperation = 0l;
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Please, enter sum: ");
        try {
            sumOperation = Long.parseLong(reader.readLine());
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
        return sumOperation;
    }

    public <E> List<E> takeIncomeConsumption(List<E> category) {
        List<E> chosenCategory = new ArrayList<>();
        category.forEach(System.out::println);
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Please choose category from list [1-" + category.size() + "] or enter -1 to end");
        try {
            int tempCtg = Integer.parseInt(reader.readLine());
            while (tempCtg > 0) {
                chosenCategory.add(category.get(tempCtg - 1));
                tempCtg = Integer.parseInt(reader.readLine());
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return chosenCategory;
    }

    public Map<Integer, Integer> chooseAccountID(List<Map<Integer, Integer>> accountFinances) {
        int account = 0;
        accountFinances.forEach(System.out::println);
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Please, chose account to work [1-" + accountFinances.size() + "] : ");
        try {
            account = Integer.parseInt(reader.readLine());
            accountFinances.get(account - 1);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return accountFinances.get(account - 1);
    }

    public Date getDate(String period) {
        Date time = null;
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Please enter " + period + " data in format YYYY-MM-DD HH:mm ");
        try {
            SimpleDateFormat parser = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            time = parser.parse(reader.readLine());


        } catch (IOException | ParseException e) {
            throw new RuntimeException(e);
        }
        return time;
    }

    public String takePathResult() {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Please, enter path to save file: ");
        try {
            return reader.readLine().trim();
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }
}
