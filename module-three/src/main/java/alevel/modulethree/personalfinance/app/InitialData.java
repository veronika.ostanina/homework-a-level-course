package alevel.modulethree.personalfinance.app;

public class InitialData {
    private String username;
    private String password;
    private Long id;

    public InitialData() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        if (username.isEmpty()) {
            throw new RuntimeException();
        }
        this.username = username;
    }

    public String getPassword() {

        return password;
    }

    public void setPassword(String password) {
        if (password.isEmpty()) {
            throw new RuntimeException();
        }
        this.password = password;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
