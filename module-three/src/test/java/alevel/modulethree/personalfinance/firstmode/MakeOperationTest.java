package alevel.modulethree.personalfinance.firstmode;

import alevel.modulethree.personalfinance.app.InitialData;
import alevel.modulethree.personalfinance.entity.Consumption;
import alevel.modulethree.personalfinance.entity.Income;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;
import org.junit.jupiter.api.Test;

import java.util.Comparator;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class MakeOperationTest {
    InitialData initD = new InitialData();
    MakeOperation operation;

    @Test
    void fillCategory() {
        initD.setPassword("12345");
        initD.setUsername("postgres");
        initD.setId(3l);
        assertDoesNotThrow(() -> operation = new MakeOperation(initD));
        initD.setPassword("908");
        assertThrows(Exception.class, () -> operation = new MakeOperation(initD));
        operation.fillAccount(operation.getAccountUser().get(0));
        List<Income> list = (List<Income>) operation.startOperation(800l);
        operation.fillCategory(list);


        initD.setPassword("12345");
        initD.setUsername("postgres");
        initD.setId(4l);
        assertDoesNotThrow(() -> operation = new MakeOperation(initD));
        assertDoesNotThrow(() -> operation = new MakeOperation(initD));
        operation.fillAccount(operation.getAccountUser().get(0));
        List<Consumption> list2 = (List<Consumption>) operation.startOperation(-20l);
        operation.fillCategory(list2);
        checkValue(list, list2);


    }

    private void checkValue(List<Income> income, List<Consumption> cons) {
        Configuration configuration = new Configuration()
                .setProperty("hibernate.connection.username", "postgres")
                .setProperty("hibernate.connection.password", "12345")
                .configure();
        try (SessionFactory sessionFactory = configuration.buildSessionFactory();
             Session session = sessionFactory.openSession()) {
            Query<Income> oper = session.createQuery("from Income ", Income.class);
            List<Income> operationFinances = oper.getResultList();
            income.sort(Comparator.comparing(Income::getIdIncome));
            operationFinances.sort(Comparator.comparing(Income::getIdIncome));
            for (int i = 0; i < income.size(); i++) {
                assertEquals(income.get(i).getName(), operationFinances.get(i).getName());
            }

            Query<Consumption> consam = session.createQuery("from Consumption ", Consumption.class);
            List<Consumption> consumptions = consam.getResultList();
            consumptions.sort(Comparator.comparing(Consumption::getIdConsumption));
            cons.sort(Comparator.comparing(Consumption::getIdConsumption));
            for (int i = 0; i < cons.size(); i++) {
                assertEquals(cons.get(i).getName(), consumptions.get(i).getName());
            }
        }
    }
}