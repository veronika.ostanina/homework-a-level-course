package alevel.modulethree.personalfinance.secondmode;

import alevel.modulethree.personalfinance.app.InitialData;
import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.FileReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;

class AccountStatementTest {

    @Test
    void makeStatement() throws ParseException {
        InitialData initD = new InitialData();
        initD.setPassword("12345");
        initD.setUsername("postgres");
        initD.setId(2l);
        AccountStatement accountStatement = new AccountStatement("jdbc:postgresql://localhost:5432/PersonalFinance", initD, "test.csv");
        Map<Integer, Integer> account = Map.of(3, 1821);
        accountStatement.setAccount(account);
        SimpleDateFormat parser = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        accountStatement.makeStatement(parser.parse("2020-01-12 12:12"), parser.parse("2020-12-12 13:18"));
        checkFile();

    }

    void checkFile() {
        assertDoesNotThrow(() ->
        {
            try (BufferedReader reader = new BufferedReader(new FileReader("test.csv"))) {
                reader.readLine();
                String[] arrayCheck = {
                        "1,89,2020-05-08T07:24:11.396957,3,89,89",
                        "4,345,2020-05-08T13:04:34.064016,3,434,434",
                        "5,-100,2020-05-08T13:19:40.302075,3,434,334",
                        "6,987,2020-05-08T15:35:08.15275,3,1421,1321"};

                for (String s : arrayCheck) {
                    assertEquals(s, reader.readLine());
                }
            }
        });
    }
}